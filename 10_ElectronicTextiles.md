# Working with electronic textiles, or textile computing

## Working with electricity

Before we get to the topic at hand, I need to scare you all a bit.

**NEVER** **NEVER** **NEVER** **NEVER** **NEVER** **NEVER** **NEVER** **NEVER** **NEVER** **NEVER** **NEVER** **NEVER** **NEVER** **NEVER** **NEVER** 

**MESS WITH THE INSIDES OF AN ELECTRONIC DEVICE THAT PLUGS INTO A WALL** 

**YOU ARE LIABLE TO BE ELECTROCUTED AND DIE**

I write this to remind you that electricity is very powerful. It's not something to be *feared*, per se, but *respected*. And since we're going to be working with electricity in the coming weeks, we need to understand what kinds of electricity we *can* work with, and what kinds we should stay away from.

The power that comes out of the wall is alternating current at anywhere from 110-120V 60Hz (Hertz, or cycles per second). What this means is that the voltage varies 60 times a second in a sine wave between around -120V to 120V. (In Europe the voltage and frequency is different: around 220V at 50Hz. Depending on who colonised whom, the voltages in other parts of the world are either the US standard or the European standard.) Most wall outlets are designed to output at least 10-15 amps of sustained current before the circuit breaker will be tripped.

Now, the details of all of this aren't entirely important. We'll get into what some of these terms mean in a bit. But the following is very important:

**what kills you when you get electrocuted is not voltage but current**.

You all probably have experienced *static electricity shocks* in the winter months. While annoying, these shocks don't really hurt you. But you might be surprised to know that the voltages in those shocks are hundreds if not thousands of volts! So why don't you get killed by these shocks? It's because the *current* or amount of *electric charge* that passes through your body is quite minimal.

Luckily your skin acts as a great resistor to the movement of electric charge. But within your body is a lot of liquid with salts in it, which acts not as a resistor, but rather helps current/charge flow. And your heart functions based on regular electric pulses from your nerves that tell the muscles to contract. So it turns out that only a few *milliamps* (thousandths of an amp) are necessary to disrupt the normal beating of your heart and cause cardiac arrest. But given the normal resistances of the skin, it's highly unlikely that small amounts of current/charge are going to reach your heart.

That's not the case when you're working with power directly from the wall. The voltage, which not as high as most static electricity, is enough sometimes to break down the resistance of the skin, especially if you're sweaty or it's humid outside. As well, the outlet can pump out a lot more current/charge than a static shock. And that's why it's **ABSOLUTELY EXTREMELY TERRIBLY HORRIBLY DANGEROUS** to work with **ANY** electric device that plugs directly into the wall, unless you know what you're doing.

**IF**, however, you find yourself in a situation where you're concerned about being shocked by something that might be powered directly from the wall, here are a few tips:

* **ALWAYS** touch with your right hand
* **ALWAYS** have your left hand behind your back

Why is this? Since your heart is on the left side of your body, you want to touch with the *right* side of your body so that the current/charge has a further distance to travel. And, you hold your left hand behind your back so that the electric charge doesn't cause your left arm/hand muscles to contract and grab onto the electric device, or, worse, grab on to your right arm/hand, thus completing a circuit of charge flowing through your heart and leading to nearly certain death.

If you see somebody in this kind of a situation, **DO NOT TOUCH THEM**, but rather try and use a wooden pole, or your feet (if you're wearing rubber soled shoes) to try and push/kick them away. You may have to be violent, but doing so will likely save their life.

I was shocked in college, very briefly, by 120V from the wall. It was due to my own stupidity. But it was enough to make me respect electricity greatly.

## But! Not to be feared!

It's important to take these concerns to heart. But let me say again: electricity is not be feared, but respected. And in the cases we're working with, with devices plugged into battery packs or USB ports on your computer, you're going to be safe. The amount of current and voltage that we're dealing with her (usually around 3.3 to 9 volts) is not enough to cause you any problems.

So, until you really know what you're doing, only work with electric devices that use small amounts of batteries (like 1-8 AA, AAA, C, or D batteries), or devices that can plug into your USB ports.

Again: electricity is not something to be feared, but respected.

## With that out of the way...

Let's talk about some terminology. This isn't a physics course, so we're not going to be able to go into what exactly an *electrical charge* is, how it functions, and so on. We're rather going to get a basic idea of the main terms involved, a few of the basic relationships, and some of the symbols that represent these ideas in electronics diagrams or schematics.

Now, in the past I've said that I don't like metaphors. But when it comes to electricity, a metaphor relating it to water is actually quite useful. So let's go through some parts of this.

Imagine that we've got some water that's flowing in a pipe. This stands in for electricity flowing down a wire. In that case then, we can define the following terms, written as "term name (unit): description":

* Electric charge (coulombs): amount of water in the pipe
* Voltage (volts): water pressure in the pipe. Think of a pipe with one end on the top of the hill and the other end at the bottom. And say there's a separate pipe that begins on a *higher* hill but meets at the end of the first pipe. The pipe on the second hill will have a higher water pressure than the first pipe. In our metaphor, that would be a higher *voltage*.
* Current (amps): amount of water flow, how fast it moves through the pipe. In electricity this is defined as "coulombs per second".
* Resistance (ohms): something that blocks or impedes the flow of water in the pipe.
* Capacitance (farads): the capacity to release water. Consider a reservoir at the top of the pipe. The reservoir acts as a *capacitor* that holds water until it is needed to flow through the pipe.

So in electrical circuits we have a *voltage source* (in our case, batteries or your USB port) which provides the *charge* that flows as *current* through *resistors* and *capacitors* and other kinds of electronic components like LEDs, motors, or microcontrollers.

One of the most important relationships in electronics is Ohm's law, which relates voltage in volts (V), current in amps (I), and resistance in Ohms (R): `V = I * R`. This equation is absolutely essential when you're designing your own circuits. For this course, you don't need to know how to use it. But it's good to know that it exists.

For more info on the basics of electricity see https://learn.sparkfun.com/tutorials/voltage-current-resistance-and-ohms-law/electricity-basics .

## Adafruit and your kits

We're going to be working with materials developed by a company called Adafruit, started by "Lady Ada" Limor Fried. Fried went to MIT and started Adafruit by selling parts and kits for some of the circuits she was designing as a student. A consummate hacker, Fried has also developed a number of circuit designs that help people get into electronics. These products also come with extensive tutorials and documentation, and pretty much all of the firmware, libraries, and sample code is available as open source. More information about her and the company is available here: https://www.adafruit.com/about .

The parts in your kit are the following:

* Adafruit Gemma M0: https://learn.adafruit.com/adafruit-gemma-m0
* Flora RGB Smart NeoPixel: https://learn.adafruit.com/flora-rgb-smart-pixels
* Flora Lux Sensor (https://learn.adafruit.com/flora-lux-sensor/overview ) OR Flora Accelerometer/Compass Sensor (https://learn.adafruit.com/flora-accelerometer/overview )
* Vibrating Mini Motor Disc: https://www.adafruit.com/product/1201
* Conductive Thread Bobbin - 12m (consumable) (for your kit, this was purchased from another electronics company, SparkFun: https://www.sparkfun.com/products/13814 ; however, I usually prefer to buy Adafruit's version because it's easier to sew with: https://www.adafruit.com/product/640 )
* Small Alligator Clip Test Leads: https://www.adafruit.com/product/1008
* 3 x AA Battery Holder: https://www.adafruit.com/product/3287
* 3 x AA Batteries (consumable)
* Needle set: https://www.adafruit.com/product/615
* USB Cable: https://www.adafruit.com/product/592

We'll be going over these components in more depth below. But just a bit of foreshadowing. Your Gemma M0 has what's called a *microcontroller* on it, which you can think of as a small computer. You can program it in similar ways to your computer, but it is *decidedly* not as powerful as your laptop or even your phone! These are limitations that we'll have to deal with below. The Gemma board exposes access to specific *pins* on the microcontroller that you can use to interface with the outside world, like sensors, LEDs, or motors. We'll be writing the code that allows the microcontroller to access and do things with those pins.

To do the programming with your Gemma, you'll also need to download the program "Mu": https://learn.adafruit.com/welcome-to-circuitpython/installing-mu-editor . Like with PyCharm, Mu will be our IDE for programming the Gemma boards. Because yes, we can actually program the chip on those boards using Python!

## Looking at your Gemma

![Adafruit Gemma M0](media/AdafruitGemmaM0.jpg){ width=60% #fig:GemmaM0}

Let's go into a bit more detail about the Gemma (see Figure @fig:GemmaM0). Assume that you've oriented your board like the one in the figure. There are six pins that we'll be able to interact with, three on one side, and three on the other. The pins have large holes to allow for easy sewing or clipping onto using the alligator clips. The silver connector at the top is for your USB cable, and the black connector on the bottom is for your battery pack. There is also a "reset" switch on the left, and an on-off power switch on the right. It might not be apparent right now, but there are also a couple of on-board LEDs, including one that allows for full RGB color.

There are three pins for powering other components that we'll be connecting to the Gemma. The ones you'll be using are GND for ground, and Vout, for powering your external components. The pin labeled 3Vo we won't be using.

Then there are three other pins that allow us to interact with the world. Each of them can be used either as an *analog* pin or a *digital* pin, but not analog or digital at the same time. The details of how this works are beyond the scope of this class, but let's go into things a bit.

First, the idea of *digital* pins. Recall Boolean logic: things are either true or false. How might we represent that *physically*? By convention, we do that using voltages! So we say that 0 volts, or *ground*, represents "false", and some other value higher than that as "true". Most *digital* logic circuits run on either 5V or 3.3V; the latter is more common with more recent devices that are designed to use less power, and is what the Gemma runs on. So, for the Gemma, a voltage of 3.3V represents a logical "true", and a voltage of 0V represents a logical "false"^[Things are a bit more complicated, depending on whether we're using TTL or CMOS logic, and sometimes anything above a certain threshold less than 3.3V or 5V counts as "true", and and and...suffice to say things get complex. We'll leave those complexities to the side here, but if you go into depth in electrical engineering in the future you'll learn more about the details.]. True, or 3.3V, is also called HIGH, and False, or 0V, is also called LOW.

When you start programming the microcontroller you have to tell it whether you're going to use a pin as an *output* or an *input* pin; you can't have it work as both at the same time. So, if you wanted to output whether something is true or false, you could setup pin D0 (on the left hand side of the board) to be an output, and change it's value to be either 3.3V or 0V. Conversely, if you wanted to receive a true or false value, you could set pin D2 (top left of the board) to be a digital *input*, and wait for some component to output to this pin a voltage of 0V (false) or 3.3V (true). (Note: you always connect the *output* of one thing to the *input* of another. Inputs going to inputs, or outputs going to outputs doesn't make much sense^[Except for when you actually *want* to do that in order to make interesting art or sound pieces, which some artists do.]!)

Let's say, on the other hand, that you want to receive an *analog* signal, or output an *analog* signal, i.e., something that *varies* between two values, and can take any value in-between those two values. Well, this presents a conundrum. Your microcontroller only works in the world of the *digital*. So we have to find a way to take an analog signal and *quantize* it into something that can be understandable by a digital system. We do this by using either an *analog-to-digital converter* (ADC) or *digital-to-analog converter* (DAC), depending on the direction of our conversion. So let's say that we want our microcontroller to take in some analog data from the world, say from a knob that we can turn left or right (this is known as a *potentiometer*). We'd set a pin on the Gemma, say A2, as an *analog input*, which then will use the ADC built-in to the microcontroller to digitize and *quantize* the signal into a value that we can work with. The ADC will have a certain *precision* that it can represent the incoming signal as; this is usually given in terms of the *bits* of the ADC, and for the Gemma it's a 12-bit ADC. (While you'd think that means the values would range from 0 to 4096, internally the values go from 0 to 65535 for ease of use; the scaling happens internally.)

Outputting an analog signal is a bit more challenging. On the Gemma there is one pin, labeled A0 (on the right of the Gemma), that is a "true" analog out using a 10-bit DAC. The other two pins, A1 and A2, can be used for analog output using *pulse-width modulation*, which we'll learn about briefly later.

## Bit more about circuits, and series and parallel notions

![Basic circuit in series.](media/BasicCircuit_schem.pdf){ width=60% #fig:BasicCircuit}

Before we go into using the Gemma, it's best to spend a bit of time about some basic notions about circuits. We're not going to be going into the details of components like resistors, capacitors, and batteries, but some knowledge of these things will be useful for understanding why we connect things in the way we do below.

In order to have a *complete* electrical circuit we need a way to have current go from one place to another in a complete loop. Electrical charge always needs to return to where it began in order for the circuit to be complete. If you don't do that, you have an *open circuit*, and it's unlikely that your circuit is going to work.

So to begin with we always have to have a *power source*. In our case it's going to be provided by the USB port on your computer, or your battery pack. In either case there is both a *positive* and an *negative* end. In the basic schematic seen in Figure @fig:BasicCircuit, the battery is seen on the left hand side, labeled "VCC1". You can think of current as flowing from the top of the battery, through a wire and clockwise to the right through the resistor, labeled R1, through a wire that then connectors to the capacitor, labeled C1, and through another wire back to the battery again on the left. This is a complete circuit, where current always returns to where it began.

Now, the current is flowing through the resistor and the capacitor *in series*. That means that the current first flows through the resistor, and next through the capacitor. Technically there is what's called a *voltage drop* from the resistor to the capacitor. Sometimes that's what you want, but often in what we're doing in our later examples you want to wire things a bit differently.


![Basic circuit in parallel.](media/BasicCircuitParallel_schem.pdf){ width=60% #fig:BasicCircuitParallel}

Consider the slightly different schematic seen in Figure @fig:BasicCircuitParallel. Here we have R1 and C1 wired *in parallel*, meaning that voltage and current flow *across* both components *simultaneously*. This relates to two laws called *Kirchoff's current law* and *Kirchoff's voltage law*. We don't need to go into either in much detail, but the basic idea is that we want the same voltage and the same current to go across our components so that they are powered properly. This happens when we wire our components in parallel. So, below, you'll see us connect the positive lead of one component to the positive lead of the next component, and the same for the negative (ground) leads. This ensures that we are wiring the components together in parallel, and ensures that things will work like we want them to. (This is different from what I said earlier about only connecting outputs to inputs. When it comes to power and ground, it's best not to think of them as either an input or an output. They're something entirely different altogether, and it's very common to connect all of the powers together, and all of the grounds together, so that everything is wired in parallel.)

This might seem like a lot to take in, and in a way it is. We unfortunately don't have a lot of time to spend on some of the finer points of electronics. For more details, see the SparkFun tutorials on [What is a Circuit?](https://learn.sparkfun.com/tutorials/what-is-a-circuit), [Capacitors](https://learn.sparkfun.com/tutorials/capacitors), [Resistors](https://learn.sparkfun.com/tutorials/resistors), and [Series and Parallel Circuits](https://learn.sparkfun.com/tutorials/series-and-parallel-circuits).

<!-- Kirchoff's current law: sum of currents into a node is equal to the sum of currents flowing out of a node

Kirchoff's voltage law: sum of all voltages around a loop is equal to zero

What does this mean: for things wired in *series*, the voltage *drops* for each additional element in the series, while the current stays the same. For things wires in *parallel*, the voltage is the *same* going into each branch of the circuit, while the current is spread *across* each branch. -->


## HARDWARE IS HARD

Repeat after me:

**HARDWARE IS HARD**

**HARDWARE IS HARD**

**HARDWARE IS HARD**

It's true: working with hardware and electronic circuits is quite challenging. You're dealing with the physical world, and sometimes things are unpredictable, are affected by other things going on in the world, or are just plain *fussy*. So I'll repeat what I said at the beginning about programming. If you find yourself frustrated, about ready to throw your computer or your Gemma in the trash, *breathe*, step away for a bit, and take a break. Trust me, you'll thank yourself. Have someone else look at your circuit. Come back to it later. And you'll feel rather accomplished once you get it to work!

## A few more important things...

While we looked earlier at how to think about analog and digital inputs and outputs with your Gemma, for the most part we'll be using higher-level ways of working with the pins. That's because most of your components require a bit more configuration to work, and a lot of that is hidden behind the scenes in libraries that we'll be using that were written by Adafruit developers/employees. The one difference is with the motor, which we'll work with in a bit.

We're going to be using something called CircuitPython to program our Gemma. This allows us to build upon what we were just learning about Python. More info about CircuitPython can be found at https://learn.adafruit.com/adafruit-gemma-m0/the-next-step . Using CircuitPython means that it builds upon your knowledge of Python, but it also means that you have to learn a new API, which is a useful skill. But because of the lack of power of our Gemma as compared to your computer, it also means that CircuitPython is a bit limited compared to the Python that you use on your computer. You'll see some examples of this in a bit.

It's also possible to program your Gemma using it as an Arduino, which uses a language that is very conceptually similar to Processing. Programming your Gemma in this way is beyond the scope of this class, but you can find more info here: https://learn.adafruit.com/adafruit-gemma-m0/arduino-ide-setup .

By convention, power leads and wires are colored red, and ground leads and wires are colored black. Of course the colors that you use, either of alligator clips or wire or conductive thread, doesn't affect how the circuit functions! But it's best to keep to these conventions as much as possible.

Finally, remember to download the Mu software before going through the examples below!

## Sewing with conductive thread

At first you'll be wiring up your projects using the alligator clips, as it allows for easy prototyping. When you've got a circuit that you'd like to make more permanent, you can replace the clips with sewn conductive thread on the fabric. The thread is usually some combination of spun steel, and depending on the number of plys, how the thread is spun, etc., your thread will be more or less easy to work with. It's also best to keep the thread lengths on the shorter side, as longer amounts of conductive thread can lead to higher resistances, which might affect your circuit. The easiest way to sew with the conductive thread is to hold the fabric within an embroidery hoop, as that keeps the fabric taught. Tie a knot on the end of your thread so that it doesn't pass through the fabric as you're sewing. Sew three or four times around the pin/pad on the Gemma or the LEDs or the sensor to ensure a tight connection, as this is where you will often have the most problems, as the connections might not be snug and thus your circuit might act somewhat flaky. Then, use a running stitch to sew towards the next element in your circuit, and repeat the sewing through the pads as before. When you're done sewing one leg of the components together, tie off the thread in a knot on the underside of the piece, and cut off the excess thread.

A couple of tips:

* Of course, don't cross conductive thread traces that carry different types of signals! Given that the threads aren't insulated from each other, threads of different types of signals that touch will short each other out, causing your circuit not to work. So you have to plan where you'll be sewing before you actually do it.
* Same thing goes for the underside of your piece; ensure that the cut ends of traces don't touch thus causing a short.
* Protip! Once you've cut off the ends of your threads, they might start fraying. Use clear nail polish topcoat to stiffen up those ends and ensure that they won't continue fraying and thus potentially cause something to short out in your piece. If something *does* short out, it's unlikely to cause any damage or a fire, but rather just cause your circuit to behave strangely.

## Wiring up the NeoPixel

Okay, let's get to making some circuits! The Flora NeoPixels (https://www.adafruit.com/product/1260) are a simple circuit board that allows you to easily control an RGB LED using only power, ground, and a single data line. Usually working with an RGB LED would require a fair bit of circuitry and components. But the NeoPixel board allows you to dispense with all of that, as well as interfacing with it in a straight-forward way in your code.

The way to wire up your Gemma M0 and one of your NeoPixels is shown in Figure @fig:NeoPixel .

![Flora NeoPixel wiring](media/NeoPixelWiring_bb.pdf){ width=100% #fig:NeoPixel }

When wiring up the NeoPixel for testing, it's best to use the alligator clips to allow you to easily remove and readjust thing if needed. One thing to note: ensure that the metal ends of the alligator clips do not touch any of the small silver "pads" on your circuit boards. The silvery metal on these pads is called *solder*, and is what is used to permanently bind electronic components to *circuit traces* on the *circuit board*. Solder is made up of a metal alloy that melts at low temperature, and a core called *flux* that allows the metal alloy to flow. Solder is used to make permanent joints, whereas here we'll be making *temporary* connections using either the alligator clips, or conductive thread (which is definitely more permanent than the alligator clips, but can still easily be removed or reworked if necessary).

Once you've gotten the NeoPixel wired up, we need to write the code for it. There's a bit of housekeeping needed to ensure that we have the correct libraries imported that will allow us to communicate with the board:

```python
## Adafruit CircuitPython imports
import microcontroller
import board
import neopixel

## Standard library imports
import time
```

These `import`s allow us to reference pins or pads on the board, as well as work with the NeoPixel itself.

We then need to setup the NeoPixel:

```python
## Setup the neopixel
## We're going to use pin A0, we only have 1 neopixel attached to this pin, and we want the brightness to be somewhat faint
pixels = neopixel.NeoPixel(board.A0, 1, brightness=0.2)
```

If you've noticed, the NeoPixels have four pads: one for power, one for ground, one for an "input", and one for an "output". The idea of the NeoPixel is that each one can be address individually based on their position in a chain. So, you can connect the output of one NeoPixel to the input of the following on, and the software will allow you to easily address each pixel individually. We'll see how to do this in a bit. For now, let's stick to the individual NeoPixel.

So this code for setting up the NeoPixel says that we're going to use pad `A0` on the board as our output, we have only one NeoPixel, and we want the brightness to be not-too-bright.

Then, let's look at how we might cycle through some colors in time. But let's return to some of our discussion earlier in the semester about *infinite loops*. We said then that infinite loops were a terrible, no good thing. But we throw that advice out of the window when we're programming for *hardware*. Because we often want a set of code to run continuously, from the time we turn on the device (by plugging in the battery, for example, or flipping a switch), to the time that we turn it off (by unplugging the battery or flipping the switch again). So in this case, it's often desirable to go into an infinite loop that causes a set of code to run over and over again. The idiom for doing that in CircuitPython is what you'll see below:

```python
while True:
    # Fill the neopixels with a white color
    # Note that we are passing a `tuple`, one of the types of lists that we didn't talk about.
    # Basically a `tuple` is an *immutable* list, meaning that it can't be changed once it is created.
    # Sometimes functions or classes expect `tuple`s to be passed, or return them as return values.
    # A `tuple` works a lot like a list, except elements can't be added once it has been created (i.e., it's immutable!)
    # You enclose your values between pairs of *parentheses*, rather than square brackets like with a `list`.
    pixels.fill((255, 255, 255))
    # Show the pixels (i.e., send the commands to change the color to be white)
    pixels.show()
    # Sleep for half a second
    time.sleep(0.5)
    
    pixels.fill((255, 0, 0))
    pixels.show()
    time.sleep(0.5)
    
    pixels.fill((0, 255, 0))
    pixels.show()
    time.sleep(0.5)
    
    pixels.fill((0, 0, 255))
    pixels.show()
    time.sleep(0.5)
```

A couple of things

* We actually have to use a `tuple` here, so be sure to read the comments in the code to see what a `tuple` is and how to use it.
* We can fill and set all of the pixels with the same color by using the `fil()` method.
* Once we set the color, we have to tell the NeoPixels to display the new color using `show()`.
* We can use a library from the Python standard library, `time`, to allow us to `sleep()` (or do nothing) for a certain number of seconds. This allows us to sequence a series of color changes with the pixels.

Now let's look at how to work with multiple NeoPixels. See the wiring diagram in Figure @fig:NeoPixelMultiple .

![Multiple NeoPixel wiring](media/NeoPixelMultipleWiring_bb.pdf){ width=80% #fig:NeoPixelMultiple }

We don't have to change much in our code. When we setup our NeoPixels, we now say that we have two pixels connected to each other:

```python
## Setup the neopixel
## We're going to use pin A0, and now we have two pixels connected to this pin, and we want the brightness to be a bit faint
pixels = neopixel.NeoPixel(board.A0, 2, brightness=0.2)
```

`pixels` is a `list`, where each element in the `list` is a particular NeoPixel object, referencing the NeoPixels in the real world. To set the color of the first NeoPixel to white, we can just write `pixel[0] = (255, 255, 255)`, and to set the color of the second NeoPixel to red, we'd write `pixel[1] = (255, 0, 0)`. Thus, the following revision of our infinite loop allows us to cycle through *different* colors for each NeoPixel:

```python
while True:
    # We're going to cycle through different colors for each NeoPixel.
    # `pixels` is a `list` whose length is the same as the number of NeoPixels defined above
    # We can set the color of each NeoPixel by assigning a `tuple` of RGB values to the appropriate element in the list
    pixels[0] = (0, 0, 0)
    pixels[1] = (255, 255, 255)
    # Show the pixels (i.e., send the commands to change the color)
    pixels.show()
    # Sleep for half a second
    time.sleep(0.5)
    
    pixels[0] = (255, 0, 0)
    pixels[1] = (0, 0, 255)
    pixels.show()
    time.sleep(0.5)
    
    pixels[0] = (0, 255, 255)
    pixels[1] = (0, 255, 0)
    pixels.show()
    time.sleep(0.5)
```

Theoretically the Gemma M0 could address *thousands* of NeoPixels chained together. However, after about 10 pixels chained together you'll start to run into *electrical* problems, as there won't be enough current to power later NeoPixels in the chain (given that the current is being *sourced* by the pins on the Gemma M0 itself), and, if you're using conductive thread, there might be significant resistances at work due to the length of the conductive thread. So if you want to address more than 10 NeoPixels, you probably will have to split off the extra NeoPixels to a different pin on the Gemma M0.

## I2C bus

Many of the sensors that you can purchase from Adafruit work over what's called *I2C*, or "inter-integrated circuit" bus. (Note: I2C is pronounced "I-squared-C.) Let's unpack a few things here.

In computers various devices (like the microprocessor, the memory, your hard drive, etc.), communicate with each other over what is called a *bus*, which you can think of as a "superhighway" down which data travels. Each device on a bus has a particular *address* by which it can be found. To send data from one device to another, the sending device needs to send the address of the receiving device to the bus, and then the data to be transferred. The receiving device only listens for messages that are prefixed by the receiving device's address. If the receiving device sees a message with its address, then it reads the corresponding data off of the bus.

To synchronize all of these different devices on the bus (some of which might be processing data faster or slower than others), there needs to be a *clock* which pulses at a regular rate. And then there needs to be the actual address-data combination to be sent. Thus, two wires (at least) need to be used: one for the clock, and one for the data. In I2C the two lines are called SCL (Serial Clock Line, for the clock) and SDA (Serial Data Line, for the data). Within the I2C system, one device, called the *master* sends out the clock signal, and all of the other devices, called *slaves*, listen for the clock and then can receive/send data on the bus.

## Aside: Master-Slave terminology

Wait, whut? *Master-slave*??? Yes, indeed, those are the terms that are used to describe the "behaviors" of devices. Yes, it's pretty darn inappropriate. Yes, it ought to be changed. But unfortunately we're stuck with that terminology for right now, and when you're looking for documentation you're going to come across these terms, so it's better that you know the terminology and the specific way it's used in computing^[See also: <https://motherboard.vice.com/en_us/article/8x7akv/masterslave-terminology-was-removed-from-python-programming-language>. Another unfortunate use of the "master-slave" terminology comes from Western philosophy and the work of the German philosopher Georg Wilhelm Friedrich Hegel. Known as the "master-slave" dialectic (or "lordship and bondage"), Hegel was attempting to explain how one kind of "self-consciousness" can only come to know itself through recognizing another "self-consciousness". Thus, the "master" can only recognize himself (because of course for Hegel it was definitely a *him*) by recognizing the consciousness of the "slave". Importantly, Hegel was influenced by the successful Haitian revolution (1791--1804), which was the only slave uprising that actually led to the founding of a republic.].

Another set of unfortunate terms in computing? "Male" and "female" ports--the definition is left up to the imagination of the reader.

## Working with the Lux Sensor

Now that we know a bit about I2C, we can talk about wiring up our sensors, the Lux sensor or the Compass/Accelerometer sensor. We'll start with the Lux sensor, as the Compass/Accelerometer is slightly more complicated. But we have to talk about one thing first, and that's *space*. Not space out there in space, but storage space on the Gemma M0. While it's pretty impressive that the Gemma M0 can run Python, it still has a very small amount of storage space to work with. And that means that you have to be especially cognizant of what you put on it. By "small", I mean *small*. On my Gemma M0 I have a total of about 48KB to work with. That's *kilobytes*. A photo that you take with your camera phone can be around 1.5-2MB, or *megabytes*. So we're talking about an amount of space that's about 30 times *smaller* than the size of your average photograph! 

Because of that, we have to be especially careful of what we put on the Gemma's drive. By default things should be setup so that OS X, in particular, doesn't add extraneous files to the Gemma. If something goes wrong, you can look at the topic ["Running Out of File Space on Non-Express Boards"](https://learn.adafruit.com/adafruit-gemma-m0?view=all#running-out-of-file-space-on-non-express-boards) on the Adafruit website to see how to get things back to normal.

All of this info is needed because we have to download a specific *library* to use our Lux sensor. Given the small size of the Gemma's storage, the library needed to interface with the sensor is not installed by default. We have to go to the ["Adafruit CircuitPython Library Bundle"](https://github.com/adafruit/Adafruit_CircuitPython_Bundle) page on Github to download the complete set of libraries specifically configured for our Gemma. To actually download the library, go to the [releases](https://github.com/adafruit/Adafruit_CircuitPython_Bundle/releases) page and download the file that says something like "adafruit-circuitpython-bundle-2.2.3-mpy-20180504.zip" (the sequence of numbers at the end is a date, and will likely be different depending on when you download the file). You want the file that has a sequence of numbers beginning with "2.", and includes "mpy" in the filename. This means that we're download the libraries that work with CircuitPython version 2, and that the files are in a compressed "mpy" format to take up less space on our Gemma.

Once you've done that, you can extract the zip file. Then, navigate to the `lib` folder in the extracted folder. There, we'll see all the possible libraries that we could copy over to our Gemma. We only need to copy two things. First, copy `adafruit_bus_device` to your `lib` folder on your `CIRCUITPY` drive. Then, copy `adafruit_tsl2561.mpy` to your `lib` folder on your `CIRCUITPY` drive. (Be sure to copy right right file, **not** `adafruit_tsl2591.mpy`, otherwise your code won't work!) Also copy over `simpleio.mpy` as that'll help us with some calculations later.

NB: what the heck does `tsl2561` mean in the name of that library? Well, `tsl2561` is the name for the specific *integrated circuit* chip that is used in the Lux sensor. This is a chip sold by a particular hardware manufacturer for a variety of uses. You can look at the technical datasheet for this chip by just typing `tsl2561` into Google. It's going to be a highly technical document, so don't worry about the details! But the idea is is that anyone can then go and look at how to interface with this chip in their own projects. Adafruit has taken care of all of the details of working with the chip by creating the library, `adafruit_tsl2561`. The name of the chip is used in the name of the library, rather than `LuxSensor`, because it often happens that certain chips stop being produced and companies like Adafruit have to move to new, different chips. Thus, the end-user programmer (that's you!) can know which library to use for which light sensing chip.

After doing that your folder structure in your `lib` directory should look something like what you see in Figure @fig:drive_tsl2561 .

![`CIRCUITPY` `lib` directory setup for connecting to the Lux sensor.](media/drive_tsl2561.png){ #fig:drive_tsl2561 }

Once that is setup, we can now start connecting the wires up to the Lux sensor. We're also going to wire up a NeoPixel so that we can control the color of the light based on how bright the sensor thinks things are.

The Lux sensor works over I2C, so we have to connect the SDA and SCL lines to the correct pads on the Gemma. On the Gemma, A2 can double as SDA, and A1 as SCL. We'll connect the data line for our NeoPixel to A0. See Figure @fig:LuxWiring for the details of how to connect things up. Remember: the *color* of your wires doesn't affect the *electrical* properties at all, but keeping ground as black and power as red as much as possible does help you in tracing problems with the wiring.

![Flora Lux Sensor wiring](media/Lux_TSL2561_Wiring_bb.pdf){ width=100% #fig:LuxWiring }

Now that you have everything setup, we can turn to the code. Here is our list of `import`s; note the additions for working with the Lux sensor and for doing some calculations later, namely `busio`, `adafruit_tsl2561`, and `simpleio`:

```python
## Adafruit CircuitPython imports
from digitalio import DigitalInOut, Direction, Pull
from analogio import AnalogIn, AnalogOut
import microcontroller
import board
import neopixel
import busio
import adafruit_tsl2561
import neopixel
import simpleio

## Standard library imports
import time
```

We setup the NeoPixel as before:

```python
## Setup the neopixel
## We're going to use pin A0, we only have 1 neopixel attached to this pin, and we want the brightness to be somewhat faint
pixels = neopixel.NeoPixel(board.A0, 1, brightness=0.2)
```

We now need to setup our I2C system. Luckily, `busio` provides a constructor for that, where we just have to pass the references to the board's SCL and SDA lines. We then pass that new `i2c` object to the constructor for the Lux sensor:

```python
## A2 (pad #0) is SDA
## A1 (pad #2) is SCL
i2c = busio.I2C(board.SCL, board.SDA)
tsl = adafruit_tsl2561.TSL2561(i2c)
```

Now let's just print out the values of the sensor to the serial console:

```python
while True:
    lux = tsl.lux
    print("Lux value is: %f" % lux)
```

When you're first working with a sensor it's an *excellent* idea to print out the values to your console so that you get an idea of the *range* of the sensor given your particular conditions. Obviously the range of a light sensor is going to be different if you're indoors vs outdoors. This gets to the question of *calibration*, which is beyond what we can get to in this class. Suffice it to say that when working with hardware calibrating sensors for particular conditions is a very important part of what you do.

When I was using this sensor at home, it appeared to go from around 6 fully covered by my hand, to about 45 uncovered. So I'd like to map that range of values to 0 to 255. This is where the `simpleio` library comes in handy, as it has a `map_range()` method that works just like `map` does in Processing. We can then color our NeoPixel using the following code. Note that the pixel values have to be an integer, so I cast the result of `map_range()` to an `int()`. Our infinite loop now looks like the following:

```python
while True:
    lux = tsl.lux
    print("Lux value is: %f" % tsl.lux)
    
    # Map our lux range to a range more suited to RGB values
    rValue = int(simpleio.map_range(lux, 6, 45, 0, 255))
    pixels.fill((rValue, 255, 255))
    pixels.show()
```

Write the complete code to `main.py` on your Gemma and you should see your NeoPixel change color based on the sensor readings of the Lux sensor!

## Working with the Accelerometer/Compass Sensor

Wiring up the Accel/Compass sensor is exactly like the Lux sensor, as it too uses I2C to communicate with the Gemma. See Figure @fig:CompassAccel for details.

![Flora Accelerometer Compass Sensor wiring](media/CompassAccel_LSM303_Wiring_bb.png){ width=100% #fig:CompassAccel }

We need to copy over a separate library for the sensor. So go back to the instructions for copying over the `adafruit_tsl2561` library, and copy over the `adafruit_lsm303.mpy` library, which interfaces with the `lsm303` chip.

Our code is substantially similar to the Lux sensor code, just using the correct libraries for our Accel/Compass sensor:

```python
## Adafruit CircuitPython imports
from digitalio import DigitalInOut, Direction, Pull
from analogio import AnalogIn, AnalogOut
import microcontroller
import board
import neopixel
import busio
import adafruit_lsm303
import neopixel
import simpleio

## Standard library imports
import time

## Setup the neopixel
## We're going to use pin A0, we only have 1 neopixel attached to this pin, and we want the brightness to be somewhat faint
pixels = neopixel.NeoPixel(board.A0, 1, brightness=0.2)

i2c = busio.I2C(board.SCL, board.SDA)
sensor = adafruit_lsm303.LSM303(i2c)
```

There are a couple of differences in the code for reading from the sensor, and here we discover another useful quirk of Python. The `sensor` object has a couple of fields of data that we can use, namely the `acceleration` and `magnetic` variables. Each of these variables in turn has three components to it, namely the respective `x`, `y`, and `z` values. Python allows us to quickly assign each of those components to *separate* varaibles by writing a list of three variables on the *left hand side* of the equation. Thus, to get the corresponding `x`, `y`, and `z` values of the `acceleration` and `magnetic` variables, we can write:

```python
while True:
    accel_x, accel_y, accel_z = sensor.acceleration
    mag_x, mag_y, mag_z = sensor.magnetic
    print("%f %f %f" % (accel_x, accel_y, accel_z))
```

See how we do that? What's going on is that `sensor.acceleration` returns a `tuple` of three values, and instead of assigning it directly to a `tuple` and then using the components of that `tuple` later, we simply assign each of the three components to three separate variables. This helps improve the readability of our code.

When I printed out the values I saw that the acceleration values went between around -7 and 8.5. So I'm going to map that range onto the range of 0 to 255 for each component of RGB. The complete infinite loop is below:

```python
while True:
    accel_x, accel_y, accel_z = sensor.acceleration
    mag_x, mag_y, mag_z = sensor.magnetic
    print("%f %f %f" % (accel_x, accel_y, accel_z))
    
    # Map our accel range to a range more suited to RGB values
    rValue = int(simpleio.map_range(accel_x, -7, 8.5, 0, 255))
    gValue = int(simpleio.map_range(accel_y, -7, 8.5, 0, 255))
    bValue = int(simpleio.map_range(accel_z, -7, 8.5, 0, 255))
    
    pixels.fill((rValue, gValue, bValue))
    pixels.show()
    
    time.sleep(0.5)
```

Thus our NeoPixel changes color based on the orientation of our sensor. Neat!

## Capacitive Sensing

Given that we're electric animals, we all have some level of *capacitance*, or stored electrical charge. This means that with the right circuit we're able to *sense* this charge, this capacitance. Such a technology is how many of your touchscreen devices work.

Usually doing this requires a fair bit of electronics knowledge and external components, but our Gemma has capacitive sensing built in for each of the pins `A0`, `A1`, and `A2`. We're going to use `A2` for this example.

Setting up the capacitive sensing is very easy:

```python
from touchio import TouchIn
## ...
## Capacitive touch on A2
touch2 = TouchIn(board.A2)
```

Then to determine whether or not a touch was sensed, we simply test whether `touch2.value` is `True` or `False`:

```python
if touch2.value is True:
    print("A2 touched!")
```

Now, the capacitive sensor is somewhat "touchy"; that is, because of the speed of the infinite loop, your Gemma's pin will probably trigger multiple times even if you feel like you just "tapped" on the pin. To take care of this, we use the `time` module to only register touches that have occurred *outside* a given threshold. Like in Processing with `frameCount`, the `time` module provides information about how long the device has been on when you call `time.monotonic()`. This returns the number of seconds (as a `float()`) that the device has been on. So, we can keep track of the last time the pin was touched, and only register a touch event if the new touch event occurs at a time that is the old time plus some small threshold (like `0.2` seconds).

So here's a way to cycle through colors each time the pin is touched:

```python
while True:
    # Now we look at our capacitive touch value. If the Gemma senses a touch on the pin, then `value` will
    # be True
    if touch2.value:
        # Here's where we check whether or not the current monotonic time is greater than our threshold.
        # This keeps the capacitive sensor from being too "touchy".
        if ((time.monotonic() - currentTime) > threshold):
            # This should look familiar to you. We keep track of a counter, and then do modular division
            # to cycle through the colors.
            counter += 1
            
            if ((counter % NUM_COLORS) == 0):
                currentColor = WHITE
            elif ((counter % NUM_COLORS) == 1):
                currentColor = RED
            elif ((counter % NUM_COLORS) == 2):
                currentColor = BLUE
            elif ((counter % NUM_COLORS) == 3):
                currentColor = GREEN
            else:
                currentColor = (255, 255, 0)
            
            pixels.fill(currentColor)
            pixels.show()
            
            # We set the current time to be the current monotonic value so that the next time through the loop
            # we can easily check whether or not we're past the threshold value.
            currentTime = time.monotonic()
```

Let's look at how to make a capacitive sensor using conductive thread!

## Capacitive Sensor

![Capacitive sensor wiring.](media/CapacitiveSensing_bb.pdf){ width=100% #fig:CapacitiveSensing}

Of course the pin on the Gemma works just fine as a sensor, but it's nice to have something a bit more stylish or easy to use. How could we create a sensor using conductive thread? The idea is to sew a *pad* that is large enough to be a good sensor. And we can do that by sewing a series of *running stitches* back and forth to create a rectangular (or circular or or or) area of thread. A not particularly elegant example of this can be found in Figure @fig:SewnCapacitiveSensor.

![Sewn capacitive sensor on the left.](media/SewnCapacitiveSensor.jpg){ width=100% #fig:SewnCapacitiveSensor}

Your sensors can be any shape you'd like. Just be sure to not use *too* much thread, as when you use more thread the resistance increases, and that could potentially affect the sensing logic.

## Working with the vibration motor


![Wiring up your vibration motor.](media/MotorWiring_bb.pdf){ width=80% #fig:MotorWiring}

Here we're going to just work with the vibration motor. If you look at the motor it has two leads, one red, one black. We're going to connect the black lead to ground, and the red lead to one of your digital outputs, say pin D1 (see Figure @fig:MotorWiring ). To turn the motor on, we just need to pass, say, 3.3V to the positive side of the motor. How can we do that? By using a digital out, of course!

Let's look at how to setup pin D1 as a digital out:

```python
## Pin to connect to the motor
motor = DigitalInOut(board.D1)
motor.direction = Direction.OUTPUT
```

Now we can easily write to pin D1 by setting `motor.value` to either `True` or `False`. Since we don't want the motor to be on all the time (as it might damage the motor, and might, over time, actually damage our Gemma), we decide to just "pulse" the motor for short periods of time. To do that, we simply turn on the motor briefly, and then sleep for a bit, and then turn it off, and sleep for a longer period of time:

```python
while True:
    # You definitely don't want to be powering the motor
    # from the pin continuously. So here we just pulse
    # the motor for a short period of time by setting
    # the output pin, D1, to be HIGH (True, 3.3V)
    # for 0.2 seconds, and then setting it to be LOW
    # (False, 0V) for 1.5 seconds. This is repeated
    # forever.
    motor.value = True
    time.sleep(0.2)
    
    motor.value = False
    time.sleep(1.5)
```

So now you could potentially cause the motor to vibrate based off of inputs on one of the other two Gemma pins.

## Fading your NeoPixel

Let's add some fading in and out to your NeoPixel. The `neopixel` library makes this easy, as we just need to change the `brightness` value over time.

First, let's setup some max and min values for the brightness level. In Python it's a convention to use ALL CAPS for variables that we deem to be *constant*, or unchanging over the course of the program:

```python
## Some values for brightness of the NeoPixel
BRIGHTNESS_MAX = 0.3
BRIGHTNESS_MIN = 0.01
```

We then need to set the current `brightness` value and how much it'll change each time through the loop, our `brightnessStep`. Changing this to larger or smaller values will determine how fast our NeoPixel fades in and out:

```python
## Changing `brightnessStep` will change how slow or fast the LED fades.
brightness = 0.3
brightnessStep = 0.0008
```

Then, within our infinite loop we simply check whether or not we've hit our `BRIGHTNESS_MIN` or `BRIGHTNESS_MAX`, and adjust the direction of our step accordingly. We then increment or decrement or `brightness` value and assign it to our pixels:

```python
while True:

    # For our fading in and out, we determine whether or not our current brightness is within the
    # range of our BRIGHTNESS_MIN and BRIGHTNESS_MAX, and then increment or decrement the 
    # brightness accordingly. The size of `brightnessStep` determines how fast the NeoPixel
    # fades in or out.
    if (brightness >= BRIGHTNESS_MAX):
        brightness = BRIGHTNESS_MAX
        brightnessStep *= -1
    elif (brightness <= BRIGHTNESS_MIN):
        brightness = BRIGHTNESS_MIN
        brightnessStep *= -1
    brightness += brightnessStep
    pixels.brightness = brightness
    pixels.show()
```

Now we have fading LEDs!

### Complete capacitive and fading example

Let's see the complete code that combines capacitive sensing (and cycling of our NeoPixel's color), along with fading of the NeoPixel in and out:

```python
import board
import time
from digitalio import DigitalInOut, Direction, Pull
from analogio import AnalogIn, AnalogOut
from touchio import TouchIn
import neopixel
import pulseio

## Standard Python library imports
import time

## Setup the neopixel
## We're going to use pin A0, we only have 1 neopixel attached to this pin, and we want the brightness to be somewhat faint
pixels = neopixel.NeoPixel(board.A0, 1, brightness=0.2)

## Capacitive touch on A2
touch2 = TouchIn(board.A2)

## Let's setup some default values for things, these are called *constants* in other languages.
## In Python the convention is to define these variables using
## ALL CAPS

## Some colors
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
WHITE = (255, 255, 255)
NUM_COLORS = 4

## Some values for brightness of the NeoPixel
BRIGHTNESS_MAX = 0.3
BRIGHTNESS_MIN = 0.01


## Now let's setup the default values for the variables we're going to use
## in our infinite loop.
counter = 0
currentColor = WHITE

## Changing `brightnessStep` will change how slow or fast the LED fades.
brightness = 0.3
brightnessStep = 0.0008

## These values have to do with the "touchiness" of the capacitive sensor.
## It can be triggered very easily, and if we let it be constantly triggered we'll cycle through
## our colors a lot faster than we'd like. So we need to use some sort of time threshold to determine
## when to move on to the next color. The function `time.monotonic()` works similarly to `frameCount` in
## Processing; it returns a value that is constantly increasing since the Gemma was powered on.
threshold = 0.2
currentTime = time.monotonic()

while True:

    # For our fading in and out, we determine whether or not our current brightness is within the
    # range of our BRIGHTNESS_MIN and BRIGHTNESS_MAX, and then increment or decrement the 
    # brightness accordingly. The size of `brightnessStep` determines how fast the NeoPixel
    # fades in or out.
    if (brightness >= BRIGHTNESS_MAX):
        brightness = BRIGHTNESS_MAX
        brightnessStep *= -1
    elif (brightness <= BRIGHTNESS_MIN):
        brightness = BRIGHTNESS_MIN
        brightnessStep *= -1
    brightness += brightnessStep
    pixels.brightness = brightness
    pixels.show()
    

    # Now we look at our capacitive touch value. If the Gemma senses a touch on the pin, then `value` will
    # be True
    if touch2.value:
        # Here's where we check whether or not the current monotonic time is greater than our threshold.
        # This keeps the capacitive sensor from being too "touchy".
        if ((time.monotonic() - currentTime) > threshold):
            # This should look familiar to you. We keep track of a counter, and then do modular division
            # to cycle through the colors.
            counter += 1
            
            if ((counter % NUM_COLORS) == 0):
                currentColor = WHITE
            elif ((counter % NUM_COLORS) == 1):
                currentColor = RED
            elif ((counter % NUM_COLORS) == 2):
                currentColor = BLUE
            elif ((counter % NUM_COLORS) == 3):
                currentColor = GREEN
            else:
                currentColor = (255, 255, 0)
            
            pixels.fill(currentColor)
            pixels.show()
            
            # We set the current time to be the current monotonic value so that the next time through the loop
            # we can easily check whether or not we're past the threshold value.
            currentTime = time.monotonic()
```


## Pulse-Width Modulation

Let's say that you want to change the brightness of an attached LED, but can only turn a digital pin on and off. It would seem like you'd only be able to output 5V or 0V, correct? In theory, yes, but in practice, you can output a lower voltage signal by regularly *varying* the when the pin is on and off. This leads to the idea of *pulse width modulation* (PWM). PWM is an extremely common technique for turning a digital signal into an analog one, for controlling motors, and for cheaply and easily outputting an audio signal. In PWM, you vary how *often* you turn the pin on and off (the *frequency*), and then how long *within that period* the pin is on versus off (the *duty cycle*).

![Duty cycle example.](media/duty_cycle.png){ width=100% #fig:DutyCycle}

Take a look at the example in Figure @fig:DutyCycle. The exact frequency that the signal is being turned on and off is not important here. In the top example, we have a 25% duty cycle, because the signal is high (on, at 3.3V) for 25% of the period. The middle example has the signal on for 50% of the period, and the bottom for 75%. Thus, over one period, the bottom example will have a higher *average voltage* than the middle example, and the middle example will have a higher average voltage than the top example. As a result, we can vary the voltage of a *digital* pin (which should usually only be between 0 and 3.3 volts) by changing the *duty cycle* of a PWM signal.

Luckily you don't have to control the PWM directly, as Circuit Python offers a library for that. To set it up, you merely have to tell Circuit Python which pin you're going to use for PWM^[Your Gemma board has one pin, A0, that actually is an *analog output* and doesn't require PWM! So if you aren't using A0 already, and you need to output something in an analog fashion, it's best to use A0 rather than do PWM on A1 or A2.]:

```python
import pulseio

led = pulseio.PWMOut(board.A1, frequency=5000, duty_cycle=0)
```

Let's setup some default values for our duty cycle limits. The duty cycle is given as a 16 bit number, so the values range from 0 to 65535:

```python
## Setup some default values
## Changing `duty_cycle_inc` will change how fast the 
## LED fades in and out.
## The duty cycle is given as a 16bit number, so it goes from
## 0 to 65535. (See, knowing binary representation *does* help you!)
MIN_DUTY_CYCLE = 1000
MAX_DUTY_CYCLE = 65535
duty_cycle_inc = 50
current_duty_cycle = MIN_DUTY_CYCLE
```

Now, to actually use PWM to change the intensity of the LED, we need to chnage the `duty_cycle` over time. Let's consider the following infinite loop:

```python
while True:
    # Let's set the duty cycle of the LED to the `current_duty_cycle`
    led.duty_cycle = current_duty_cycle
    # Then update the duty cycle.
    current_duty_cycle += duty_cycle_inc
    # And then, ensure that we stay within our default limits.
    if (current_duty_cycle >= MAX_DUTY_CYCLE):
        duty_cycle_inc *= -1
        current_duty_cycle = MAX_DUTY_CYCLE
    elif (current_duty_cycle <= MIN_DUTY_CYCLE):
        duty_cycle_inc *= -1
        current_duty_cycle = MIN_DUTY_CYCLE
```

![Wiring for PWM'ed LED sequin.](media/PWMSequin_bb.pdf){ width=40% #fig:duty_cycle_circuit}

You can see the basic wiring of this in Figure @fig:duty_cycle_circuit. You don't have an "LED sequin" in your kit, as you see in the Figure, but it gives you an idea of how you'd wire this up. Basically you connect the negative end of the sequin to ground, and the positive end to your PWM pin.

## Going forward

You've gotten a good introduction to how you can work with basic electronic ideas. If you're finding that you're running out of inputs on your Gemma, take a look at the Adafruit Flora (https://www.adafruit.com/product/659 ) or Adafruit Circuit Playground Express (https://www.adafruit.com/product/3333 ). And if you want to work even more with electronics, it's best to go over the tutorials mentioned earlier on the basics of resistors, capacitors, and circuits. I can guarantee that you'll better understand what we've been doing if you spend the time to learn that material.

## Assignment

Do something *interesting* with your kit! Due next Thursday night, including some kind of photo/video documentation of what you did.

