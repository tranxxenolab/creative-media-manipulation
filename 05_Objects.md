# Objects, or learning further levels of abstraction and the limits of metaphors

<!-- class, constructors, overloaded constructors, initialization, instance variables, getter and setter methods, pass by reference, inheritance, Strings -->

## Further levels of abstraction

Our next topic takes us into the world of *object oriented programming*, which is an abstraction of what we've already talked about vis a vis functions. So if you have questions about functions, be sure to go back and review those notes before you go further. And if you still have questions, be sure to come and speak with me.

Let's take stock of where we are. We've talked about variables, conditionals, loops, and functions. We've talked about global and local variables, and a bit about program organization and how to program in a way that allows for the *conceptual* side of the code to stand out, rather than just the functional. We're going to further expand upon this idea as we turn to *classes*, *objects*, and *object-oriented programming*. But first, an example.

Consider our favorite moving ellipses. Maybe instead of moving the ellipses around on the screen with a parametric equation, we'd instead like them to move around randomly, like we were doing last week[^randomwalk]. And let's say that we'd like to have three of them moving around at a time. How would we do that given what we know so far? Well, we'd need something like the following:

[^randomwalk]: This type of movement is what's known in mathematics as a *random walk*, and it underlies many different branches of mathematical inquiry. Random walks can be used to model things as varied as the diffusion of molecules to the stock market. For more information, see https://en.wikipedia.org/wiki/Random_walk .

```java
float x1 = 0;
float x2 = 0;
float x3 = 0;
float y1 = 0;
float y2 = 0;
float y3 = 0;

void setup() {
  size(600, 600);
  background(0);
  smooth();
  frameRate(30);

  x1 = width/2;
  x2 = width/2;
  x3 = width/2;

  y1 = height/2;
  y2 = height/2;
  y3 = height/2;
}

void draw() {
  background(0);

  updateEllipses();

  fill(255, 0, 0);
  stroke(255, 0, 0);
  ellipse(x1, y1, 20, 20);

  fill(0, 255, 0);
  stroke(0, 255, 0);
  ellipse(x2, y2, 20, 20);

  fill(0, 0, 255);
  stroke(0, 0, 255);
  ellipse(x3, y3, 20, 20);

}

void updateEllipses() {
  float x1offset = 0;
  float x2offset = 0;
  float x3offset = 0;
  float y1offset = 0;
  float y2offset = 0;
  float y3offset = 0;

  x1offset = random(-7, 7);
  if ((x1 + x1offset) < 0) {
    x1 = 0;
  } else if ((x1 + x1offset) > width) {
    x1 = width;
  } else {
    x1 += x1offset;
  }

  x2offset = random(-7, 7);
  if ((x2 + x2offset) < 0) {
    x2 = 0;
  } else if ((x2 + x2offset) > width) {
    x2 = width;
  } else {
    x2 += x2offset;
  }

  x3offset = random(-7, 7);
  if ((x3 + x3offset) < 0) {
    x3 = 0;
  } else if ((x3 + x3offset) > width) {
    x3 = width;
  } else {
    x3 += x3offset;
  }

  y1offset = random(-7, 7);
  if ((y1 + y1offset) < 0) {
    y1 = 0;
  } else if ((y1 + y1offset) > height) {
    y1 = height;
  } else {
    y1 += y1offset;
  }

  y2offset = random(-7, 7);
  if ((y2 + y2offset) < 0) {
    y2 = 0;
  } else if ((y2 + y2offset) > height) {
    y2 = height;
  } else {
    y2 += y2offset;
  }

  y3offset = random(-7, 7);
  if ((y3 + y3offset) < 0) {
    y3 = 0;
  } else if ((y3 + y3offset) > height) {
    y3 = height;
  } else {
    y3 += y3offset;
  }
}
```

Whoa!!! That's a lot of code! And it's necessary, because for each of our ellipses we have to keep track of the `x` and `y` coordinates. We have to add different offsets to each ellipse too (and for each `x` and `y` coordinate as well). And we have to check whether or not each coordinate for each ellipse has gone past the edge of the screen. Imagine what we'd need to do if we had 30 of these ellipses. How long would our code be then? How brittle would it be? Is there an easier way?

As you can probably guess, indeed there is. We can create an abstract description of our ellipses, called a *class*, and *instantiate* particular *instances* of this class, called *objects*. That's a lot of new terminology, so let's go through it slowly.

## Classes, or abstract definitions of the world

There are a lot of metaphors that are often used to talk about *object oriented programming*, which is what we're beginning to learn. But as usual I also think these metaphors get in the way of understanding, so let's cut to the chase.

class

: An abstract encapsulation of a set of data and methods.

Okay, that's probably not terribly helpful. But let's break it down. A class contains *data* and *methods*. So what's data? Well, we can think about data as *variables*. And *methods*? Well, we also know them as *functions*. And thus a class is an *encapsulation* of variables and functions. So basically with a class we're wanting to create a way to *group* together related variables and functions that describe something in our code.

Let's go back to our previous example. If we were to go to a pretty high level, what would we want to know about each of our ellipses?

* `x` and `y` coordinates
* `x` and `y` offsets
* whether or not the ellipse was past the edge of the screen
* the size of the ellipse
* the stroke and fill of the ellipse

With a class, we can write an abstract definition of this that we can then *reuse* in our code. And the point about *reuse* is key, as we're moving towards this object-oriented programming to think about how to create reusable *components* in our code.

There's no better way to explore this than to jump in, so let's do that. We're going to start writing the class to describe a `Particle` that can move about the screen.

**NB**: Class names, by convention, begin with an uppercase letter, to differentiate them from the types used for variables (like `int`s, `float`s, and so on). This will become very important in a moment, so it's best to follow this convention.

### Class variables

!["New Tab" menu option in Processing](media/newtab.png)

Classes are often defined in separate files from our main sketch. Remember how I said a number of weeks ago that it's important you upload your code with the folder structure intact? You're going to see why now. Create a new sketch and call it `particles_classes`. Within the Processing window there is a down arrow next to the name of your current sketch. If you click on that you'll see the option for a "New Tab". If you select that menu item, you'll get another window where you'll be asked to create a new file. Name this new file "Particle". Then, if you go to your file manager and look in your sketch folder, you'll see a new file called, appropriately enough, `Particle.pde`. Class definitions *do not* have to go in separate files, but they often do as a way of creating components that can be reused.

Now we've got two files in our sketch: our main sketch, and a new file called `Particle`. In `Particle` is where we're going to start writing our class. We want to start with the variables that we're going to be using to describe our abstract particle.

```java
class Particle {
  // Define our class variables
  float x = 0;
  float y = 0;
  float xoffset = 0;
  float yoffset = 0;
}
```

Classes begin, appropriately enough, with the word `class` and then the name of the class, in this case `Particle`. Then you have, within a set of open and closed curly brackets, your *class definition*. So what we're doing here is writing our code in a rather large block. And remember our discussion of local and global variables from a couple of weeks ago? Variables defined within a block are available to *all* interior blocks too. That'll be really important in a bit.

So we define our variables like we normally would in a function. We write them in a general fashion since the class defines an *abstract* particle, not any particular one. So we don't have to write `x1` and `y1` like we did before. And we declare our offsets.

### Constructors

Okay, now let's add to the class definition.

```java
class Particle {
  // Define our class variables
  float x = 0;
  float y = 0;
  float xoffset = 0;
  float yoffset = 0;

  // Define our constructor
  Particle(float xinit, float yinit) {
    x = xinit;
    y = yinit;
  }
}
```

The syntax on line 9 might look a bit wonky to you. But here we're defining our *constructor*, which allows us to setup different variables when we *instantiate* our class as an *object*. We'll get into the details of this in a bit, but for now, let's just say that the constructor is a special function that allows us to define particular variables, set things up, and so on when we create our `Particle`s in our code. The name of the constructor is also the name of the class, so we use `Particle`. Here we're saying in our constructor, let's pass two floats when we create a new `Particle` and then store those values as the `Particle`s `x` and `y` variables. Again, hold off on wondering how this works for a moment.

### Class methods

Now let's think about drawing the `Particle` on the screen. How might we do that? Within classes we can also write *functions*, just like we did in our sketch. But within the class definition we can write a more *general* version of the function.

```java
class Particle {
  // Define our class variables
  float x = 0;
  float y = 0;
  float xoffset = 0;
  float yoffset = 0;

  // Define our constructor
  Particle(float xinit, float yinit) {
    x = xinit;
    y = yinit;
  }

  // Draw the particle to the screen
  void display() {
    stroke(255, 0, 0);
    fill(255, 0, 0);
    ellipse(x, y, 20, 20);
  }
}
```

Our new code begins on line 15 with a function we've created called `display()`. This function draws the ellipse to the screen with a given fill, stroke, and size[^getter]. In classes you can write as many functions as you'd like, and all of what you know about functions (`void` and return values, passing arguments, etc.) applies.

[^getter]: You might be asking: doesn't that code mean all of our `Particle`s will have the same color and size? And indeed they will; we'll be dealing with that later.

So let's think about dealing with our offsets and updating the position of the Particle over time. Think back to our first example and how we did that, and think about how one could write a function that generalizes that logic.

```java
class Particle {
  // Define our class variables
  float x = 0;
  float y = 0;
  float xoffset = 0;
  float yoffset = 0;

  // Define our constructor
  Particle(float xinit, float yinit) {
    x = xinit;
    y = yinit;
  }

  // Draw the particle to the screen
  void display() {
    stroke(255, 0, 0);
    fill(255, 0, 0);
    ellipse(x, y, 20, 20);
  }

  // Update the particle's position
  void update() {
    xoffset = random(-7, 7);
    yoffset = random(-7, 7);

    // Check on the x coordinate
    if ((x + xoffset) < 0) {
      x = 0;
    } else if ((x + xoffset) > width) {
      x = width;
    } else {
      x += xoffset;
    }

    // Check on the y coordinate
    if ((y + yoffset) < 0) {
      y = 0;
    } else if ((y + yoffset) > height) {
      y = height;
    } else {
      y += yoffset;
    }
  }
}
```

Look at what we're doing starting on line 22. We've written a new function, `update()`, that generalizes the logic of what we were doing before. We create a random `x` and `y` offset. Then we check and ensure that this new offset doesn't push the particle off of the screen. And this new function is written in an abstract way that doesn't make any reference to *particular* `Particle`s.

To review. In classes we can define particular variables that we can use within any function within the class. We can define a special function, the constructor, that sets some variables to particular values. And we can define arbitrary other functions that help us draw something on the screen, or update variables within the class.

We've now written a complete, functional `Particle` class, and are ready to see how to implement it in our code.

**NB**: There is nothing special about the names of our functions `display()` or `update()`; they've just often used as conventions. The only special kind of function within our class is the constructor, which has to be named the same as the name of the class.

## You won't believe how this one little thing can simplify your life! Or the power of objects.

To reiterate: the class is merely an *abstract* definition of a combination of variables and functions. To actually *use* the class we have to *instantiate* it as an *object*.

object

: A concrete instance of a class.

Okay, another almost useless definition without seeing it action. So let's jump in. We're going to rewrite our sketch from the beginning of this chapter to use the new `Particle` class. And we're going to do it step by step.

Go back to our blank `particles_classes` sketch by clicking on that tab in the window. Now we can start writing the body of this sketch from the top down. We're going to create three `Particle`s so as to match what we started out with.

```java
Particle p1;
Particle p2;
Particle p3;
```

At the top of the code we're declaring new variables `p1`, `p2`, and `p3`. And they're of type `Particle`. 

Wait a minute. Type `Particle`???

Yes. When you define classes you are actually defining new data types. So far we've used *primitive* data types like `int`s, `float`s, and `boolean`s. By defining our own classes we're creating more complex data types that involve, remember, a combination of variables and functions. See how we're making things ever more abstract? This new datatype, `Particle`, that we just wrote ourselves, has the ability to save and manipulate its own data. And that gives it a power and flexibility that primitive datatypes like `int` and `float` don't have[^primitive].

[^primitive]: Yes, it's important to note here that "primitive" is a technical term in Processing, even if it might have pernicious connotations outside of the world of programming. This is another place where the language of programming conflicts without everyday language.

```java
Particle p1;
Particle p2;
Particle p3;

void setup() {
  size(600, 600);
  background(0);
  smooth();
  frameRate(30);

  // Here we create three instances of type Particle
  // And we use the Particle constructor to define
  // the width and height of each particle.
  p1 = new Particle(width/2, height/2);
  p2 = new Particle(width/2, height/2);
  p3 = new Particle(width/2, height/2);
}
```

Look closely at what we're doing beginning on line 14. We've already told Processing, at the beginning of our code, that we'd like to have three `Particle`s named `p1`, `p2`, and `p3`. We've done this to let Processing *allocate* the proper amount of memory for these objects. But like variables that are declared but not initialized to any value, our `Particle`s are undefined right now. So we use the *constructor* that we wrote to set the initial `x` and `y` variables for *each* particle.

Let's step back a bit. Our class definition of `Particle` is abstract. We create an *instance* of `Particle`, which we call an *object*. And each of these objects basically lives within its own world, with its own *instance variables*; that is, each has its own `x`, `y`, `xoffset`, `yoffset`, etc. This is the power of objects: to *encapsulate* variables and methods and enforce clean *separation* within our code.

If you're a bit confused, think back to our discussion of local and global variables, and how the variables within a function definition are *different* than the variables outside of that definition. The idea of classes merely abstracts that idea further. The `x` for `p1` is completely different than the `x` for `p2` is completely different than the `x` for `p3`. They can't see each other. They don't know what each other's values are. They each can be manipulated *independently*. And they can be manipulated in a general fashion.

Let's turn now to the `draw()` method, which might surprise you in its simplicity.

```java
Particle p1;
Particle p2;
Particle p3;

void setup() {
  size(600, 600);
  background(0);
  smooth();
  frameRate(30);

  // Here we create three instances of type Particle
  // And we use the Particle constructor to define
  // the width and height of each particle.
  p1 = new Particle(width/2, height/2);
  p2 = new Particle(width/2, height/2);
  p3 = new Particle(width/2, height/2);
}

void draw() {
  background(0);

  // Update each particle's position
  p1.update(); // This is called "dot notation"
  p2.update();
  p3.update();

  // Draw each particle on the screen
  p1.display();
  p2.display();
  p3.display();
}
```

Look at what we've done starting on line 23. For each of our particles, we call the `update()` method. How do we do this? We use what's called *dot notation*. We can call the methods we wrote in our classes using this dot notation. So think of it working like `objectName.functionName`. (We can also use this to access the variables of the class, which we'll go into in a bit.)

Think back to the class definition, and recall that `update()` randomly selects an offset, checks whether or not the particle would exceed the bounds of the window, and then updates the particle's position accordingly. And then the `display()` method draws the particle on the screen for each particle.

Now we've got a complete program. Run it and see how it works!

To review. Classes allow us to write a general definition of how variables and methods should work together. We create concrete instances of classes, which we call objects, that exist independently of one another. And we can then call the methods of the class, which in turn work on the concrete instance variables that are particular to each object.

Whew! If you're finding this confusing, that's okay. Object oriented programming is not an easy concept to get at first. Be sure to review this material before moving on to the next section if you find yourself with questions.

### Accessing instance variables

Remember how I mentioned that we can access the variables of the class using the dot notation? Let me show you how, which will also further prove to you that the `x` variables are different for each of our `Particle` objects. Here's our new `draw()` method:

```java
void draw() {
  background(0);

  // Update each particle's position
  p1.update();
  p2.update();
  p3.update();

  // Draw each particle on the screen
  p1.display();
  p2.display();
  p3.display();

  // Print out our instance variables
  if ((frameCount % 30) == 0) {
    println("p1.x ", p1.x);
    println("p2.x ", p2.x);
    println("p3.x ", p3.x);
  }
}
```

Here we're using the dot notation to access the value of `x` for each of `p1`, `p2`, and `p3`. See how they're all different?

We usually don't want to access the instance variables this way. Instead, we want to access these variables through an *interface* that we provide through writing more functions. So let's go back to the definition of our class to write getter and setter functions.

## Getters and setters

Good object oriented programming practice would say that one should not access the instance variables of your class directly, but only through specially designed functions called *getter* and *setter* functions. (Yes, those are technical terms.) Let's take a look at how we'd do that in our class definition. Return to that tab in your Processing window. At the end of the class definition, we can add the following:

```java
// Getter functions
  float getx() {
    return x;
  }

  float gety() {
    return y;
  }
```

Now, these methods might seem silly. We're writing a function simply to return a value? Yes, indeed we are. And we're doing that to enforce the idea that all access to the instance variables of the class comes through functions alone, and not through accessing the variables directly, like we just did. There's a reason for this, and it again has to do with the idea of encapsulation. We don't want code that uses our class to really know anything about how the class is written. We shouldn't have to know what `x` is actually called. Maybe in the class definition we use a complicated name for the variable because it makes the class more readable. The code that uses the class shouldn't need to know that. It should just know that if it wants to access the current value of the `x` coordinate of the `Particle`, it can call `getx()`. And the same for the `y` coordinate.

This is a good habit to get into. And it lets us update our class definition so that each particle can have a different color.

### Changing the colors of our particle

What would we need to add if we wanted to be able to save and set the stroke and fill for each particle independently?

```java
class Particle {
  // Define our class variables
  float x = 0;
  float y = 0;
  float xoffset = 0;
  float yoffset = 0;
  float strokeR = 0;
  float strokeG = 0;
  float strokeB = 0;
  float fillR = 0;
  float fillG = 0;
  float fillB = 0;

  // Define our constructor
  Particle(float xinit, float yinit) {
    x = xinit;
    y = yinit;

    // We define our default color to be red
    strokeR = 255;
    fillR = 255;
  }

  // Draw the particle to the screen
  void display() {
    // Here we use our instance variables to define
    // the stroke and fill of our ellipse
    stroke(strokeR, strokeG, strokeB);
    fill(fillR, fillG, fillB);
    ellipse(x, y, 20, 20);
  }

  // Update the particle's position
  void update() {
    xoffset = random(-7, 7);
    yoffset = random(-7, 7);

    // Check on the x coordinate
    if ((x + xoffset) < 0) {
      x = 0;
    } else if ((x + xoffset) > width) {
      x = width;
    } else {
      x += xoffset;
    }

    // Check on the y coordinate
    if ((y + yoffset) < 0) {
      y = 0;
    } else if ((y + yoffset) > height) {
      y = height;
    } else {
      y += yoffset;
    }
  }

  // Getter functions
  float getx() {
    return x;
  }

  float gety() {
    return y;
  }

  // Setter functions
  void setStroke(float r, float g, float b) {
    strokeR = r;
    strokeG = g;
    strokeB = b;
  }

  void setFill(float r, float g, float b) {
    fillR = r;
    fillG = g;
    fillB = b;
  }
}
```

Here's our full class definition. Look at what we've done between lines 7 and 12 , where we've defined new variables for the stroke and the fill; between line 20 and 21, where we've defined the default color to be red; lines 28 and 29, where we use these instance variables to determine the stroke and fill of the ellipse; and also between lines 67 and 77, where we've defined new *setter* functions to allow us to change the stroke and fill values.

**NB**: You could also change the stroke and fill values by accessing the variables `strokeR`, etc., directly through dot notation. But we don't want to do this, remember? We want to enforce a particular interface to the instance variables of the class, and we don't want our other code to care about what we name our internal variables. We just want to allow access to them through the setter functions.

Now go back to your sketch. We need to define our colors for each `Particle` after we instantiate the objects. The only change required is in `setup()`:

```java
void setup() {
  size(600, 600);
  background(0);
  smooth();
  frameRate(30);

  // Here we create three instances of type Particle
  // And we use the Particle constructor to define
  // the width and height of each particle.
  p1 = new Particle(width/2, height/2);
  p2 = new Particle(width/2, height/2);
  p3 = new Particle(width/2, height/2);

  // We set our colors accordingly
  p1.setStroke(255, 0, 0);
  p1.setFill(255, 0, 0);
  p2.setStroke(0, 255, 0);
  p2.setFill(0, 255, 0);
  p3.setStroke(0, 0, 255);
  p3.setFill(0, 0, 255);
}
```

Here we use the setter functions to set a different stroke/fill for each `Particle`.

How might we change things if we also wanted to change the width and the height of each `Particle`? And if we wanted to have the size of each particle be randomly set after we've created instances of each `Particle`? See the uploaded code for details.

**NB**: If you're thinking that it's getting a bit tiring to keep writing `p1`, `p2`, and `p3`, and if you're thinking to yourself, there's got to be a better way of doing this, well, there is, and it uses something called *arrays*, which we'll deal with next week.

## Aside: Bounding boxes


![Bounding box diagram.](media/bounding_box.png)

<!-- Of course we could also change the colors over time in the `draw()` method using our favorite idea of parametric equations. If you thought you were rid of them, sorry! -->

One of the things that one often has to do with graphics programming is determine whether or not the mouse pointer is hovering over some graphic drawn on screen. This is how buttons work, how your web browser knows that you want to follow a link, how your toolbar knows that you've clicked on an icon. It's also how games determine whether or not you've hit your target with the mouse.

The general idea here is to determine whether or not the mouse is within the *bounding box* of whatever graphics object you're interested in. So, for example, the bounding box for a circle would be the square whose sides are *tangent* to the circle. That means that, in the case of the circle, it's entirely possible to not actually be clicking on the circle, but just nearby. This is what we consider to be a *corner case*, and is something that we can take care of, but let's not worry about it for the moment.

So what's the logic here? We need to determine whether, when a mouse button is clicked, the mouse pointer is within the bounding box for each `Particle`. How might we write a new method of our `Particle` class to check this? This is probably going to involve a very complicated `if` statement....

```java
boolean hit = false;

void checkBoundingBox(float mx, float my) {
  if (((mx < (x + particleW/2)) && (mx > (x - particleW/2))) && 
    ((my < (y + particleH/2)) && (my > (y - particleH/2)))) {
    hit = !hit;  
  }
}
```

```java
// Draw the particle to the screen
  void display() {
    // Here we use our instance variables to define
    // the stroke and fill of our ellipse

    if (hit) {
      stroke(255);
      fill(255);
    } else {
      stroke(strokeR, strokeG, strokeB);
      fill(fillR, fillG, fillB);
    }

    ellipse(x, y, particleW, particleH);
  }
```

We've previously defined a `boolean`, initialized to `false`, that determines whether or not we've "hit" the Particle. If we have, we set it to be its opposite, `true`. Then, in our `display()` method, we draw the `Particle` in white if `hit` is `true`. And if not, we draw it in the normal color.

You can use the idea of bounding boxes in many, many places in your Processing projects. Can you think about how it might be useful when working with sprites?

## Overloading the constructor

If a constructor is just a function, can it be overloaded too? Indeed it can, and it is a common thing to do, especially if you want to give users of your class flexibility in initializing the class. The syntax for constructor overloading is just like that of function overloading, as seen below.

```java
  // Define a constructor with no input arguments
  // Set the default values of x and y to be the middle of the window
  Particle() {
    x = width/2;
    y = height/2;

    // We define our default color to be red
    strokeR = 255;
    fillR = 255;

  }

  // Define our constructor
  Particle(float xinit, float yinit) {
    x = xinit;
    y = yinit;

    // We define our default color to be red
    strokeR = 255;
    fillR = 255;
  }
```

So here we define a constructor with no input arguments, which sets the default values for `x` and `y` to be the middle of the screen[^middle]. But there is our previous constructor, which also allows us to define the default `x` and `y` values when we initialize the class.

[^middle]: Note that this means you have to initialize your class *after* you've set the size of the screen, which is good programming practice anyway.

As with constructors, so too with functions. Everything that you learned about overloaded functions applies to classes, so you can overloaded functions (e.g., with different input types) in classes too.

## Creating an interface

Here's a bit of useful terminology. If you've read about computing developments in the past few years, you might have heard of something called an "Application Programming Interface", or API. An API is a set of actions that different programming environments, operating systems, or companies offer to allow end-user programmers access to various capabilities. For example, Amazon might allow people to write programs for Alexa, or Apple allows one to write programs that use the camera on the iPhone. These APIs define the allowed actions for the end-user programmer.

In *many* cases, but not all, the "interface" in API is actually a set of class functions, just like we have been defining. In other programming languages it's possible to precisely control access to these functions by other code. So one of the things you're doing when creating a class is defining the *interface* to the class and defining the boundaries of what other code can do. We'll be seeing this in action in coming weeks, when we start to look at other classes that Processing provides that allow us to manipulate images and strings[^classes].

[^classes]: If you want to look ahead, we're going to soon be using the `String` and `PImage` classes.

<!-- # DANGER DANGER: Passing by reference -->

## Assignment

![Example output of your program.](media/SpritesHomework.png)

This assignment asks you to translate your sprite from last week into a class. There's a method to this madness, which you'll understand once you've read through the assignment.

Please upload this to a folder called `05_Sprites`. It's due, as usual, on Thursday night at 11:59:59PM.

Here is the interface your `Sprite` class needs to have:

* Two constructors: one without arguments that sets the `x` and `y` values of the sprite to be the middle of the window; and the second with two arguments that then set the `x` and `y` coordinate of the sprite.
* An `update()` function that updates the position of the sprite like the `Particle`s in these notes. When you update the `x` and `y` offsets you should use a *moving average*; that is, take the average of the previous offset and the new random offset. That will smooth out the movement a bit.
* A `display()` function that draws the sprites. Each sprite should have three "frames" to it. That is, the sprite should cycle through three different *aspects*. Think of Mario walking in Super Mario Brothers. Your sprite could just change color in each of the three frames. Each frame of your sprite should be on screen for 10 frames of your sketch. It's probably helpful to write each sprite's frame as a separate function, and then call each function in turn.
* A `checkBoundingBox(float x, float y)` function that returns a `boolean` of `true` if the given coordinates are within the bounding box of the sprite, or `false` if they're not.
* A `respawn()` function that resets the `x` and `y` positions of the sprite to random values within the window.
* Two "getter" functions that return the `x` and `y` coordinates of the sprite.

Your sketch should instantiate **three** sprites at random locations on the screen. When the user clicks the mouse, you should check whether or not the mouse pointer has hit a sprite. If it has, you should keep track of a "score", increasing by 10 points for each sprite. Each time the user hits a sprite, print to the console the location of the hit sprite, and the total score.

**Congratulations**, you've just programmed your first game!!!
