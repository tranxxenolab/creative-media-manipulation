# Creative Media Manipulation

This is a [textbook](CreativeMediaManipulation.pdf) I wrote for my course, "Creative Media Manipulation" which I taught for two semesters at Wellesley college between 2018 and 2019. While the content of the book is of standard programming topics--variables, functions, classes, etc--I found that all of the existing textbooks did not treat the topic in the way that I wanted them to. Namely, a particular kind of *reckoning* with programming: the way you need to think, the problematics of programming language, relationships with older histories of artists working with code, the difficulties I myself experienced with learning programming. Thus the text takes both a historical as well as sometimes an irreverent tone at times in an attempt to level with the reader/student and provide a shared understanding of the difficulties at work.

I also wanted a book that allowed one to *learn how to learn how to program*. I think this is one of the most important skills for an artistic/creative programmer to learn. Rarely will someone use the same programming for every single project, thus I found it vital to switch gears near the end of the course into a new programming language so the students could experience, *materially*, how their knowledge transferred to a new domain.

I also wanted students to learn how programming can affect the material world we live in, not just on screens, so the final chapter is a brief introduction to programming electronic textiles using CircuitPython.

The languages used are [Processing](https://processing.org/) and [Python](https://python.org). I specifically chose the standalone version of Processing (not [p5.js](https://p5js.org/)) because I wanted students to learn about strong-ish typing, as well as not worrying about the mechanics of HTML files. If I were to teach the class today I might move it to p5.js.

As well, the book was heavily inspired/influenced by Daniel Shiffman's *Learning Processing*.

In any event, I hope all of this might be helpful to some of you, either pedagogues yourself, or others wanting to learn how to program!

# Sample Code

Sample code exists in the, well, "code" folder. This includes individual Processing/Python projects for each week. All of these code examples are licensed under [GPL v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html).

# Syllabus

An extremely important element of this course was the integration of theory and practice: namely, there was a lot of *reading* and *discussion* in the course, coupled to the practical learning of programming concepts. As a result, I've also included the [syllabus](CreativeMediaManipulation_syllabus.pdf) in this repository.

# Building the text

I wrote the text in pandoc Markdown and use the following command to produce the PDF:

```bash
pandoc 00_CreativeMediaManipulation.md 01_Variables.md 02_Conditionals.md \\ 
03_Loops.md 04_Functions.md 05_Objects.md 06_Arrays.md 07_ImagesVideo.md \\ 
08_BeginningPython.md 09_PythonNLP.md 10_ElectronicTextiles.md \\
-o CreativeMediaManipulation.pdf --template eisvogel --pdf-engine=xelatex \\
--biblio bibliography.bib --csl chicago-note-bibliography-16th-edition.csl \\
--listings --top-level-division="chapter" --table-of-contents --toc-depth=3
```

# License

The text is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/) license.

The eisvogel pandoc-latex-template is licensed under the BSD 3-Clause license.

# Author

Adriana Knouf, PhD

<https://zeitkunst.org>

<asknouf@zeitkunst.org>

@zeitkunst on Twitter and Instagram

<https://tranxxenolab.net>

<asknouf@tranxxenolab.net>

@tranxxenolab on Twitter and Instagram

