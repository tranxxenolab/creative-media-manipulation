# Loops, or how to get your code to do the same thing over and over and over and over and over and over and over and over and over and over again

<!-- for, while, infinite loops, local and global variables, nested loops, using loop indices as variables, Processing reference, debugging -->

<!-- TODO: rewrite the first part of the assignment to make it less confusing. Try and make the second part a bit more manageable -->

<!-- **TODO: put something in about breaking out of for loops using break** -->

## Loops looping

So far we've been writing code that consists of one statement after another. We've learned about conditionals, or control flow, thereby letting our code do different things depending on other variables, or depending on the actions of our user. And we saw, in our example of the animated circle, that we can draw multiple animated circles by copying and pasting our code and changing some of the parameters slightly.

But...

If there's one thing computers are good at it's doing the same thing over and over again.

If there's one thing computers are good at it's doing the same thing over and over again.

If there's one thing computers are good at it's doing the same thing over and over again.

If there's one thing computers are good at it's doing the same thing over and over again.

If there's one thing computers are good at it's doing the same thing over and over again.

If there's one thing computers are good at it's doing the same thing over and over again.

If there's one thing computers are good at it's doing the same thing over and over again.

...you get the point.

So we can do this in Processing, and in many other programming languages, using the concept of a *loop*. Basically we can define a loop as follows:

loop

: A programming construct that allows a block of code to be repeated until an *ending condition* is met

Now we need to know what an *ending condition* is:

ending condition

: A Boolean expression that determines when a loop will stop

Now you know why we had to learn about Booleans before we learned about loops! The ending condition allows us to tell the computer when we want the loop to end, otherwise the loop will run on forever. In fact we already know about one kind of loop that goes on "forever", the `draw()` loop, that we'll look at in a bit more detail later on. But first, let's look at some loops in practice.

## The `while()` loop

What would one way be of programming the following?

> Draw a series of lines that all begin and end at the same x coordinates (100 and 500, respectively), and move from the top to the bottom of the screen in 50 pixel increments.

Consider the following as one way of solving that problem:

```java
void setup() {
  size(600, 600);
  background(0);
  smooth();
}

void draw() {
  fill(255);
  stroke(255);

  // Draw a set of lines at regular intervals from
  // the top of the screen to the bottom
  line(100, 0, 500, 0);
  line(100, 50, 500, 50);
  line(100, 100, 500, 100);
  line(100, 150, 500, 150);
  line(100, 200, 500, 200);
  line(100, 250, 500, 250);
  line(100, 300, 500, 300);
  line(100, 350, 500, 350);
  line(100, 400, 500, 400);
  line(100, 450, 500, 450);
  line(100, 500, 500, 500);
  line(100, 550, 500, 550);
}
```

We've here drawn a series of lines that all start and stop at the same x position, but move from the top to the bottom of the screen by 50 pixels each time. There is nothing *wrong* with this code. It'll work and draw the lines like we want.

But one thing to consider when programming is whether or not you can *simplify* your code. And one way to know whether or not you can simplify the code is to ask yourself: am I repeating code by copying and pasting that could potentially be solved by letting the computer *iterate* over a more general statement of the problem?

Here's another definition:

iteration

: In programming, looping over a general statement of the problem in order to produce varied output

So in the case above, we can ask ourselves, is there a way to use a *loop* to accomplish the same thing, in fewer lines of code, in a more general fashion, and in a way that isn't so brittle[^brittle]?

[^brittle]: Ask yourself: how many numbers would you have to change in the first example if you decided later that you wanted the lines to be at intervals of 40 pixels, rather than 50?

Of course, since we're in a chapter on loops, the answer to that question is yes. Consider the following reworking of that code to use a `while()` loop:

```java
int i = 0;

void setup() {
  size(600, 600);
  background(0);
  smooth();
}

void draw() {
  fill(255);
  stroke(255);
  
  // We draw lines until our condition says that 
  // we've exceeded the height of the window
  while(i < height) {
    line(100, i, 500, i);
    i += 50;
  }
}
```

What's going on here? With a `while()` loop, the code within the block is run *as long as* ("while"), the condition in parentheses is `true`. Each time through the loop, we add 50 to the variable `i`, which we declared at the beginning of our code. And once `i` is no longer less than the `height` of the window, the expression `i < height` evaluates to `false`, the loop quits, and we move on to the next frame.

What if we want to change the number of pixels between each line, from 50 to 40? With our `while()` loop we only have to change one value, rather than many as in our first example. Try changing that increment to different values, like 40 or 30 or 5.

So just in terms of lines of code, we've gone from a program that's 25 lines long to one that's 19. We've simplified the *logic* of the code so that we can easily see how we're *iterating* the drawing of a line. And we can easily change parameters (such as how often the line is drawn) in one place in our code, rather than multiple locations.

### What if our expression never evaluates to `false`?

One thing to note with the `while()` loop is that we can easily get ourselves into trouble. Look at the following code, which is a slight modification of the previous example, but **don't** run it in Processing.

```java
int i = 600;

void setup() {
  size(600, 600);
  background(0);
  smooth();
}

void draw() {
  fill(255);
  stroke(255);
  
  // We draw lines until our condition says that 
  // we've exceeded the height of the window
  while(i < height) {
    line(100, i, 500, i);
    i -= 50;
  }
}
```

What is going on in this code? At first blush it appears to do the same thing as the previous example, just in reverse. We initially set `i` to be 600, and each time through the loop we decrement the value of `i` by 50.

But there's a subtle issue at work. Will `i` never *not* be less than the `height`? Indeed, since we're decrementing `i` by 50, `i`, will continually get smaller, eventually going negative and thus always being less than the `height`.

Congratulations, you've just created an *infinite loop*. 

Since the ending condition never evaluates to `false`, the loop continues forever, you never reach the end of the `draw()` method, nothing appears on the screen, and you remain stuck on the first frame *forever*...or for as long as you're running your sketch! In the best of cases your window will just remain blank. In the worst of cases, you'll have to force quit Processing, or perhaps your computer, as your code continually runs through the `while()` loop without end.

This is the "gotcha" with `while()` loops. You have to be very careful about how you define your ending condition so that you **absolutely ensure** that the ending condition evaluates, at some point, to `false`. If you don't, then you'll be creating an infinite loop. And while a certain computer company exists at 1 Infinite Loop^[That is, Apple Computer.], that's definitely not where you want your code to be.

So if you decide to use `while()` loops in your code, **always, always, always** ensure that you have an ending condition that eventually evaluates to `false`. And if you're using `while()` loops but nothing is being drawn on your screen, check your ending condition.

## *The Lifetime of Variables*...

... might be the title to a rather boring film. But it's something important to understand before we turn to our next type of loop.

So far we've been declaring our variables at the very top of our code. We've done this so that we can use the variables wherever we'd like in our code: in the `setup()` function, in the `draw()` function etc. These types of variables are called *global* variables. The precise definition of a global variable depends on the programming language, but for Processing we can define them this way:

global variable

: A variable that is can be used and modified anywhere in your code.

Thus the *scope* of the global variable is our entire code; we can use and modify said variable anywhere we want in our code.

For the examples we've used so far, that's okay. But sometimes we'd like more granular control over our variables, and that's where *local variables* come into play. First the definition, then we'll look at some examples.

local variable

: A variable whose scope is limited to its enclosing block

That definition probably doesn't mean much right now, so let's look at some examples.

```java
boolean printvar = false;

void setup() {
  int var1 = 100;
  
  size(600, 600);
  background(0);
  smooth();
  
  println("In setup(), var1 is ", var1);
}

void draw() {
  int var1 = 200;
  fill(255);
  stroke(255);

  //println("In draw(), var1 is ", var1);
  if (printvar) {
    println("In draw(), var1 is ", var1);
    println("Setting var1 to 500");
    var1 = 500;
    printvar = !printvar;
  }
}

void keyPressed() {
  int var1 = 300;
  
  if (key == 'k') {
    println("In keyPressed(), var1 is", var1);  
  } else if (key == 'p') {
    printvar = !printvar;  
  }
}
```

Let's go through this code carefully. We define a global variable `printvar`, which we'll be using later to toggle a `println()` in the `draw()` method. We declare our other variables *local* to function. That means that we declare the variables at the top of the function before any of our other statements. We then print out the value of the variable in each function. So in the `setup()` function we see what `var1` is, when we press the `k` key we see what `var1` is in the `keyPressed()` function, and if we press the `p` key, we set a Boolean that then allows the `println()` statement to be called in our `draw()` method.

In each case, `var1` is different. How is that so? It's because the *scope* of the variable is *local* to its enclosing block, in this case, the function. So we have three *different* `var1` variables, each with a different value.

Now let's get to the lifetime question. In the `draw()` method, we set the value of `var1` to be 500, but the next time we press the `p` key, `var1` is still 200. Why is that so? It's because each time the `draw()` method is called, `var1` is reset to 200. The *lifetime* of `var1` is only a single loop through the `draw()` method. At the next frame, at the next time through `draw()`, `var1` is reset.

This is of course different from the ways in which global variables work, where the value is kept from one loop to another. The lifetime of a global variable is however long you are running your sketch.

What would happen if we *also* defined `var1` as a *global* variable? Try it out! If you do, you'll see that the value of the local variable trumps the value of the global one, if they are named identically.


## Aside: the `draw()` loop

Since your very first day in class you've been working with loops. The `draw()` function acts like a loop that is called every frame. What that also means is that any method that draws to the screen in the `draw()` loop is basically "queued" up, and then drawn all at once at the end of the loop. At the same time, Processing is also "listening" for any key or mouse presses, and then at the end of the `draw()` loop any of these interactions are processed[^thread].

[^thread]: Things are a bit more complicated behind the scenes. The drawing to the screen and the waiting for mouse or key presses are done on different *threads*, which allow Processing to do both things "simultaneously". Threading is an advanced topic, which we may not get to in this class. If you want more information about how this works in Processing, you can read this discussion on the Processing forums: 
https://forum.processing.org/two/discussion/13161/how-to-understand-the-execution-order-when-draw-and-mousepressed-are-next-to-each-other .

Just to repeat: **all** of the drawing methods are queued up and then drawn to the screen at the end of the `draw()` method. So what that means is that if you want some sort of animation to appear in order on the screen, you can't simply put these drawing methods in order in the `draw()` method. You have to figure out a way to only draw on, say, every other frame, or every fourth frame, etc. So let's see if we can figure out how to do that.

## Aside: `frameRate()`

Given that the `draw()` method is run every frame, we can ask: how many frames per second does it run? By default, Processing runs the `draw()` method **60 times per second**, or **60 frames per second**^[What we consider to be "cinematic" looking film runs at 24 frames per second, film that looks a bit more "live" is 30 frames per second, and Peter Jackson filmed *The Hobbit* at 48 frames per second to try and give a more "immersive" feel, although that seemed to fall flat with audiences.]. So that means that the `draw()` method updates what's on the screen every 1/60 seconds, or around every `0.01667` seconds. If we are trying to do something on screen that is all dependent on timing, then we need to keep this in mind. As well, if our `draw()` method takes *longer* than `0.01667` seconds to complete, then we'll start "losing" frames, and things might begin to stutter and, frankly, not look terribly good.

Can we change the `frameRate`? Yes, indeed we can! If we want to set the `frameRate` to a lower value, like 30 frames per second, we can simply call in our `setup()` function:

```java
void setup() {
    size(600, 600);
    background(0);
    smooth();
    frameRate(30);
}
```

This will set the `frameRate` for the sketch to be 30 frames per second. There is a corresponding global variable, `frameRate`, that gives you the *instantaneous* frame rate for your sketch as a float^[This is a bit tricky. The frame rate can vary quite a lot during your sketch, sometimes being, say, 31 frames per second, sometimes 29, and so on. Dropping frames occasionally is not necessarily good, but it also is not a show-stopper. So the value that Processing returns when you use `frameRate` is a recent *average* of the `frameRate`, and thus will be a float.].

## The modulus operator, `%`

Let's say that you wanted to divide a whole number by 2. If your whole number is even, then when you divide it by two you'll be left with a remainder of 0, as the even number is evenly divisible by 2. But if your whole number is odd, then you'll have a remainder of 1, as your whole number is not evenly divisible by 2 and there is 1 left-over.

Enter the *modulus operator*, which extends this idea to any divisor. How to calculate the modulus of a number not terribly challenging, but for the most part in computer programming we use it for one purpose only: to do things at a regular rate based off of a given divisor. And to have things happen *only* at that rate, then we check to see when the modulus is 0; that is, when the value is evenly divisible by the divisor. 

In Processing, the modulus operator works in the following way:

```java
int remainder = value % divisor;
```

Here's a question. If we want something to happen every other frame, how would we write a conditional to do that?

```java
// Only run the following code every other frame
if ((frameCount % 2) == 0) {
  println("every other frame");
}
```

And if we want things to run every 13th frame?

```java
// Only run the following code every 13th frame
if ((frameCount % 13) == 0) {
  println("every 13th frame");
}
```

Do you see the pattern here? This is the primary use of the modulus operator in Processing code. You may not need to use it for this week's assignment, but it will likely be useful for you in later weeks.

## The `for` loop

Let's get back to loops in Processing. Recall the issues with the `while()` loop, namely the problem of getting stuck in an infinite loop. What if there were a loop that we *knew* would only run a certain number of times and then stop? There is such a loop, and it's called a `for()` loop. And it also draws upon what we've learned so far about local variables and iteration.

The best way to learn about the `for()` loop is through an example. Take a look at the following code fragment:

```java
for (int i = 0; i < 10; i++) {
    println("We are on loop ", i);
}
```

The syntax here is a bit wonky, so let's go through it carefully. The `for()` loop needs three things: a local variable declaration; an ending condition; and a way to update the variable. In this example, we set `i` to be an `int` and set to 0 initially[^i]. We want this loop to execute until `i` is less than 10. And we want to add 1 to `i` every time we go through the loop. So this will print out the following:

[^i]: Almost all `for()` loops use `i` as their *index* as a convention. But you can name your variable whatever you want if it makes your code more readable.

```java
We are on loop  0
We are on loop  1
We are on loop  2
We are on loop  3
We are on loop  4
We are on loop  5
We are on loop  6
We are on loop  7
We are on loop  8
We are on loop  9
```

Just like in conditionals, we can have arbitrarily complicated expressions for our ending condition. And we can also use the local variable `i` *within* the loop in other expressions. It's best to turn to some examples to see how this works in practice.

### Line drawing

Let's re-write our line drawing example from before, this time using a `for()` loop. How might we do that? What expression would we use for our ending condition? 

```java
void setup() {
  size(600, 600);
  background(0);
  smooth();
}

void draw() {
  fill(255);
  stroke(255);
  
  // We draw lines until our condition says that 
  // we've exceeded the height of the window
  for (int i = 0; i < height; i = i + 50){
    line(100, i, 500, i);
  }
}
```

See what I've done there with the part of the `for()` loop that updates the local variable? I've written an expression that says add 50 to `i` each time through the loop. Of course changing 50 to some other value would determine how many lines are drawn to the screen and at what spacing.

### Drawing a set of circles across the screen

How would we draw a set of circles (diameter 10) across the middle of the screen and spaced every 20 pixels? Here's one way of writing the `for()` loop:

```java
for (int i = 0; i < width; i += 20) {
    ellipse(i, height/2, 10, 10);
}
```

Here's a different way of doing the same thing, focusing on the *number of times* that we want the circle to be drawn on screen:

```java
for (int i = 0; i <= (width/20); i++) {
    ellipse(i*20, height/2, 10, 10);
}
```

In this case, I use `i` as a *factor* to multiply by the desired spacing, 20, and I only want the loop to happen `width/20` times.

This is a slightly more complicated way of doing things, but is sometimes more conceptually related to what you're trying to do.

### Drawing an array of circles on the screen

You can nest `for()` loops within each other. But remember what we said about local variables! If you define a variable `i` in an outer `for()` loop, then that `i` will also be valid in any *inner* `for()` loops. An example will make this clear.

How would you draw a field of circles, starting at the top left corner (0,0) and continuing every 20 pixels in the x and y directions?

```java
for (int i = 0; i <= (width/20); i++) {
  for (int j = 0; j <= (height/20); j++) {
    ellipse(i*20, j*20, 10, 10);
  }
}
```

What would we need to change if we want the circles to be drawn every 40 pixels in the x direction, and every 60 in the y?

## Animation and loops

Let's go back to the example from last week, where we were moving a circle from one side of the screen to another under the influence of a `sin()` function.

```java
int xPos = 0;

void setup() {
  size(500, 500);
  background(0);
  smooth();
}

void draw() {
  stroke(255);
  fill(255);
  background(0);
  
  // This is the new code
  // It uses the parameter frameCount to offset the y position
  // of the circle depending on the sin function
  ellipse(xPos, 30*sin(frameCount*0.02) + height/2, 10, 10);
  
  // This is another way of writing
  // xPos = xPos + 1;
  xPos += 1;
  
  // Check that xPos is not past the edge of the screen
  // If so, reset it to 0
  if (xPos >= (width)) {
    xPos = 0;  
  }
}
```

We also looked at how to draw multiple circles moving at different rates by just having multiple calls to ``ellipse()``. Now that we know about loops, we could greatly simplify our code. So, let's draw 10 circles, moving at different heights:

```java
for (int i = 0; i < 10; i++) {
  ellipse(xPos, 20*(i+1)*sin(frameCount*0.02) + height/2, 10, 10);
}
```

Do you see what we've done there? We are now taking the index, `i`, and using it as a factor to multiply with the scaling factor 20. We're also adding 1 to it, since we don't want the value of the function to be 0.

Can we also perhaps change the period of each circle?

```java
for (int i = 0; i < 10; i++) {
  ellipse(xPos, 20*(i+1)*sin(frameCount*0.009*(i+1)) + height/2, 10, 10);
}
```

I slowed down the period here to show a bit more of what's going on.

And maybe we want the `xPos` of the circle to be offset as well, based on the loop index. We'll also change the amplitude of the `sin()` function to show more of what's going on.

```java
for (int i = 0; i < 10; i++) {
  ellipse(xPos+(10*i), 10*(i+1)*sin(frameCount*0.009*(i+1)) + height/2, 10, 10);
}
```

So, by using the `for()` loop we can create quite complicated animations just by using the index variable(s) as parameters in our functions. This will be very useful to you in the assignment.

## Aside: Processing reference

I haven't pointed you to the page on the Processing website that has a reference to all of the methods, functions, and operators that we've been talking about. Here's the link: https://processing.org/reference/ . There are a lot more things there than we've gotten to yet, but it's a good site to bookmark for when you have questions about syntax, or what kind of value a function returns.

## Aside: Debugging

![Image of the first "bug" in Grace Hopper's logs of the functioning of the Mark II computer at Harvard University in 1947. Image from <https://en.wikipedia.org/wiki/Grace_Hopper#/media/File:H96566k.jpg>.](media/hopper_bug.jpg)

When you're banging your head at your keyboard, and you're frustrated with why you can't get your code to work...it's best to step back a bit, take a deep breath, maybe step away from the keyboard, and perhaps even have someone else look at your code. I've been there too, wondering why something's not working, then coming back and realizing that the solution was actually quite simple.

It's not natural for us to think like a computer. We're not wired like a computer. So sometimes, when we've spent a lot of time in that headspace, we need a bit of a break before we can come back and look at the problem anew.

Here are a few debugging tips:

* If you find that in your attempt to fix your bug you keep on adding complexity...step back, maybe revert some of the changes you've made. Chances are the solution is simpler than you're making it out to be. Now, sometimes the solution *is* complex. But it's better to try the most direct solution first, before diving into something complex.
* Think carefully about what each line of your code is doing. Are you missing a place where you should be clearing the background of the screen? Are you perhaps clearing the screen too often? Do you have identical variable names somewhere? Is your variable initialized correctly?
* Have someone else look at your code. You won't believe it, but just the very fact of having another programmer look at your code is often enough to find and squash the bug. 

Please don't feel "embarrassed" at realizing that you were missing something "silly" or "stupid" in your code. Trust me: I've spent *days* trying to fix code, only to realize that the solution was a variable that was declared incorrectly, or not writing my loops correctly. One line, one character: enough to make a program work, or not work. It happens to all of us.

## Assignment

Beginning with this assignment you will be graded on your programming style. Please be sure to refer to the style guidelines discussed in the notes for last week.

You code should be uploaded to a folder labeled `03_Loops` on Sakai and is due at the usual time, 11:59:59PM on the following Thursday night.

![First example output for Problem 1.](media/parametric_lines.png)

![Second example output for Problem 1.](media/parametric_lines_spaced.png)

1. Return to your code from last week, the code that drew the lines in the style of early computer art. Modify it so that it 

   * First, displays *only* the current and future 10 evaluations (i.e., current and future 10 lines) of the parametric equations. These ten lines should be updated at every frame, i.e., at every time step. **HINT**: you probably want to use a loop :-) **HINT, again**: think about the background. **HINT**: remember our discussion of how Processing draws to the screen, at what times, and how things are "queued up" at the end of the `draw()` loop.
   * Next, displays only 10 lines at a time, but spaced out further than in the previous bullet point. So, for example, rather than simply going 10 time steps into the future, you may want to go 50 time steps in the future, spaced evenly by 5 intervening time steps so as to only have 10 lines on the screen at a time.

   Please name your code `LASTNAME_FIRSTNAME_Loops_parametric_for` and `LASTNAME_FIRSTNAME_Loops_` `parametric_for_spaced`. Please take screenshots for each of these modifications. See the screenshots above for examples of what your output should be.

![Monochrome output for Problem 2.](media/trippy_monochrome.png)

![Color output for Problem 2.](media/trippy_color.png)

2. Trippy color changing.

   **NOTE**: If you have photosensitive epilepsy, or are especially sensitive to flicking images, please do not do this part of the assignment. Come to me and we'll figure out an alternative problem.

   For this part of the assignment you should run your sketch at 30 frames per second. Divide the window of size 600 by 600 into a grid of `i` by `j` squares. Each square should be of size `sizeW` and `sizeH` width and height, respectively. (A good starting value for these variables is 40.) Write code to draw each square with a different color that varies both over time *and* position in the manner of the `sin()` function. You'll probably want to use one of the formulations of the `sin()` function from other examples in this chapter and from last week. This would be something like: `amplitude * sin(t * position * frac) + offset`, where `t` is a continuously increasing parameter. **HINT** Try setting the `frac` part of the function to something in the range of 0.0001 to 0.0007 to reduce flickering. **HINT** If you want full color, you'll probably need to choose *different* `frac` values within those ranges. Allow a toggle that switches between full color and monochrome when the user presses the 'f' key. **HINT**: you'll need to shift the value of the `sin()` function so that it's in the range from 0 to 255, so you'll probably want to think about the amplitude and offset aspects of the equation. **HINT**: you'll want to involve the loop indices in your `sin()` function in some way.

   **Ungraded part**: Once you've made this work, can you change it so that, upon pressing the 's' key, the squares change color all along one color's *saturation*? That is, can you vary the colors so that things change only from dark red to light red, or dark green to light green?

   Please name your code `LASTNAME_FIRSTNAME_Loops_trippy`. Please also take a screenshot of both the color and the monochrome parts of your output.
