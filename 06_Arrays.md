# Arrays, or collecting like things together

<!-- arrays, array initialization, array indexing, use in loops, array.length, nested arrays, array functions (append, etc.), String class, strings as array of characters, escape characters, PFont -->

## Creating a set of like things

Before we get into the topic of this week, *arrays*, I want you to take a moment and realize just how far you've come so far. From starting with little-to-no programming knowledge, your current assignment is to program a simple game from scratch. That's pretty amazing! After this week's topic you'll know all of the basic structures of modern programming (variables, conditionals and boolean logic, loops, functions, objects, and arrays). The rest, they say, are just details...important details, but details and not fundamentally new topics.

As I hinted about before, this week we're going to deal with the idea of *arrays*, or sets of like data that we can work on and manipulate all at once. Let's consider the following idea. We want to draw 10 ellipses on the screen at a time, and we want each ellipse to have a different color. (We're not going to use our `Particle` class at the moment.) We want to be able to change the colors of each ellipse at some point in our program. How would we begin to write the variable declarations for the stroke colors of our ellipses?

```java
float stroke000R = 0;
float stroke001R = 0;
float stroke002R = 0;
float stroke003R = 0;
float stroke004R = 0;
float stroke005R = 0;
float stroke006R = 0;
float stroke007R = 0;
float stroke008R = 0;
float stroke009R = 0;
float stroke000G = 0;
float stroke001G = 0;
float stroke002G = 0;
float stroke003G = 0;
float stroke004G = 0;
float stroke005G = 0;
float stroke006G = 0;
float stroke007G = 0;
float stroke008G = 0;
float stroke009G = 0;
float stroke000B = 0;
float stroke001B = 0;
float stroke002B = 0;
float stroke003B = 0;
float stroke004B = 0;
float stroke005B = 0;
float stroke006B = 0;
float stroke007B = 0;
float stroke008B = 0;
float stroke009B = 0;
// etc..
```

We'd have to declare each variable by itself. We'd have to decide on a naming scheme that allowed us to easily reference the relevant variable. We'd need to do all of this all over again if we want to change the fill colors too. And we'd have to do a fair bit of work if we'd want to control 100 ellipses on the screen at a time^[That certainly is an understatement!].

How can we deal with this in a more efficient way? With an array. Here's a definition:

array

: A sequential collection of like data

There are three important things in this definition. The first, *sequential*, means that an array keeps track of things *in order*, a very important and useful property. Second, *collection* means that the data is kept together in a coherent unit. And the last, *like data*, means that the data that the array collects together in order is of the same *type*. 

So let's see this in action. Some of what you're going to see below should be familiar to you after our previous discussion of classes and objects.

### Declaring and initializing the array

Let's say we want to create a set of arrays for dealing with the fill colors of our ellipses. We need a separate array for each of R, G, B. We'd declare the arrays as follows.

```java
float[] fillR;
float[] fillG;
float[] fillB;
```

This wonky syntax needs a bit of explication. We decide on the type, `float`, and then use open and closed *straight brackets* (**not** curly brackets) to indicate that we're creating an array, and then the name of the variable. The use of the straight brackets will become clear in a bit.

Like with objects, we've merely told Processing here to be ready to store three arrays of floats. We then need to further declare the array in our `setup()` function.

```java
// Declare our arrays
float[] fillR;
float[] fillG;
float[] fillB;

void setup() {
  // And here we do our further array declaration
  fillR = new float[10];
  fillG = new float[10];
  fillB = new float[10];
}
```

Like with objects, we use the `new` keyword with the *type* of data we're wanting to put into the array, and then, in straight brackets, the total size of the array. So these lines say, create a new array of floats of count 10 for each of the `fillR`, `fillG`, and `fillB` variables.

One important thing to note: you **must** decide on a size of an array when you declare it. That size cannot be a variable that is decided upon later in your code. The array **must** have a size when being declared. That size can change later on in your code, but at first it must be a definite value. And, as you could imagine, that value **must** be a positive integer.

With that said, the value *could* be an expression that evaluates to something. So we could do the following:

```java
// Declare our arrays
float[] fillR;
float[] fillG;
float[] fillB;

int numEllipses = 10;

void setup() {
  // And here we do our further array declaration
  fillR = new float[numEllipses];
  fillG = new float[numEllipses];
  fillB = new float[numEllipses];
}
```

Here we've decided to define another variable that determines the number of ellipses we're going to draw on the screen. That then allows us to easily have more or fewer ellipses drawn with a change to one line of code.

At this point in time our array can be used, but the actual values of the array are undefined. Let's look at initialization, which is *similar* to how we needed to *instantiate* objects.

### Initializing the array

Now we can begin to understand the straight bracket syntax. We can *access* individual elements of the array by each element's *index*. So if we wanted to start to define the red fill values, we could do the following:

```java
// Declare our arrays
float[] fillR;
float[] fillG;
float[] fillB;

int numEllipses = 10;

void setup() {
  // And here we do our further array declaration
  fillR = new float[numEllipses];
  fillG = new float[numEllipses];
  fillB = new float[numEllipses];

  // Access each array element by its index
  fillR[0] = 128;
  fillR[1] = 234;
  fillR[2] = 2;
  fillR[3] = 156.28;
  fillR[4] = 200.34;
  fillR[5] = 129.51;
  fillR[6] = 45.90;
  fillR[7] = 10;
  fillR[8] = 0;
  fillR[9] = 254;
}
```

So starting on line 15, we refer to our variable, and then within the straight brackets we access the `float` value that is *stored* at *index* 0. And then in line 16 we access the `float` value that is stored at index 1. And so on.

A couple of things here: 

* Array indexing begins at 0. You may have forgotten it, but I mentioned this in one of our early discussions about variables. Counting in programming often begins at 0, and that's especially the case with arrays in Processing. So to count to 10 we begin with 0 and end with 9. Again this might seem a bit wonky but hopefully it'll make more sense in a bit.
* The syntax on the left hand side of the equals sign might appear strange at first, but it's a shorthand way of referring to each element in the array. You can use that syntax on the left hand side just like you would use any other `float` variable.

Here's another way of initializing our arrays all at once:

```java
// Declare our arrays
// This is a shorthand for initializing the fillR array
float[] fillR = {128, 234, 2, 156.28, 200.34, 129.51, 49.90, 10, 0, 254};
float[] fillG;
float[] fillB;
int numEllipses = 10;

void setup() {
  // And here we do our further array declaration
  // Note that we don't have to use the new keyword for our fillR
  // array since we declared and initialized it in one go
  fillG = new float[numEllipses];
  fillB = new float[numEllipses];
}
```

In this shorthand, you put all of the values of the array, separated by commas, within *curly brackets* on the right hand side of the statement. The number of values in the curly brackets defines the size of the array!

So let's see how we can use these values:

```java
// Declare our arrays
float[] fillR;
float[] fillG;
float[] fillB;

int numEllipses = 10;

void setup() {
  size(600, 600);
  frameRate(30);
  smooth();

  // And here we do our further array declaration
  fillR = new float[numEllipses];
  fillG = new float[numEllipses];
  fillB = new float[numEllipses];

  // Access each array element by its index
  fillR[0] = random(0, 255);
  fillR[1] = random(0, 255);
  fillR[2] = random(0, 255);
  fillR[3] = random(0, 255);
  fillR[4] = random(0, 255);
  fillR[5] = random(0, 255);
  fillR[6] = random(0, 255);
  fillR[7] = random(0, 255);
  fillR[8] = random(0, 255);
  fillR[9] = random(0, 255);
}

void draw() {
  if ((frameCount % 30) == 0) {
    // Print out the value at index 0
    println("fillR[0] is ", fillR[0]);

    // Print out the value at index 7
    println("fillR[7] is ", fillR[7]);
  }
}
```

So in the `draw()` method we merely print out the values of the array at indices 0 and 7.

A diagram of what we've learned so far can be seen in Figure 1.

![A visual representation of how an array works.](media/array_diagram.png)

What we've done so far is somewhat useful, but it's not harnessing the real power of arrays. If an array is a sequential collection of data, and has indices that go from 0 to some number, is there some other way of moving through a sequence (that you know about already) that we can combine with arrays?

### Arrays and loops

Our method for assigning values to the array is rather inefficient. We'd still have to fill all of these values by hand with many, many lines of code.

Enter loops! We can use `for` loops to *iterate* over the contents of an array. This is a very powerful technique and is used nearly everywhere in programming. Let's rewrite our code so that it uses loops instead to initialize the array.

```java
for (int i = 0; i < numEllipses; i++) {
  fillR[i] = random(0, 255);
}
```

Yup, that's it. We use a `for` loop to iterate from 0 to `numEllipses`. We then use that loop index as the index to our array, which allows us to then assign a *different* random number to each element of the array.

Let's see this in action with a slightly more expansive sketch than the one we've been looking at. We're going to draw our `numEllipses` ellipses to the window with different colors and different positions. You'll see the power of loops at work here.

```java
// Declare our arrays
float[] fillR;
float[] fillG;
float[] fillB;
float[] ellipsesX;
float[] ellipsesY;
float[] ellipsesW;
float[] ellipsesH;

int numEllipses = 10;

void setup() {
  size(600, 600);
  frameRate(30);
  smooth();

  // And here we do our further array declaration
  fillR = new float[numEllipses];
  fillG = new float[numEllipses];
  fillB = new float[numEllipses];
  ellipsesX = new float[numEllipses];
  ellipsesY = new float[numEllipses];
  ellipsesW = new float[numEllipses];
  ellipsesH = new float[numEllipses];

  // Here's the loop to iterate over the array
  for (int i = 0; i < numEllipses; i++) {
    // Initialize colors to different random values
    fillR[i] = random(0, 255);
    fillG[i] = random(0, 255);
    fillB[i] = random(0, 255);

    // Initialize position to random values
    ellipsesX[i] = random(0, width);
    ellipsesY[i] = random(0, height);

    // Initialize width and height to be random values
    ellipsesW[i] = random(10, 40);
    ellipsesH[i] = random(10, 40);
  }

}

void draw() {
  background(0);

  // Iterate over our ellipses
  for (int i = 0; i < numEllipses; i++) {
    // Set our fill color
    fill(fillR[i], fillG[i], fillB[i]);

    // Set the stroke to be the same as the fill
    stroke(fillR[i], fillG[i], fillB[i]);

    // Draw the ellipses to the screen
    ellipse(ellipsesX[i], ellipsesY[i], ellipsesW[i], ellipsesH[i]);
  }
}
```

Take a look at what's going on in `setup()`. We use *one* loop to set the values for all of our variables. Remember how I mentioned the importance of the fact that loops store things *in sequence*? That means that if we start at 0 and count up to `numEllipses` we'll always keep things in order and can easily keep track of things *across* multiple arrays. So in this case we can say that for the same `i` index across these arrays, we're going to be storing the parameters for *that particular* ellipse.

Then look at `draw()`. Again, we use a loop to iterate up to `numEllipses`. We then use the same loop index to get the values for each of our desired parameters.

What would we need to do if we wanted to have 100 ellipses? 1000? All we'd have to do is change *one* variable. Everything else works as before. Experiment with changing `numEllipses` to see what happens.

### A couple of further notes

There are a couple of things to note.

First, if you ever want to get the length of the array (the number of elements), you can access it through `arrayName.length`, using the same kind of syntax that we were using with classes. So, if we wanted to write a loop to iterate through the `fillR` array, you could write:

```java
  for (int i = 0; i < fillR.length; i++) {
    fillR[i] = random(0, 255);
  }
```

You will often see this method of writing the `for` loop alongside the one we used already (using a variable to decide on the length of the array).

Second, you might be asking how to print the contents of the array to the console window. You could iterate over the array, but there's a helper function called `printArray()`. This is how you could use it in the `draw()` function:

```java
void draw() {
  background(0);

  // Iterate over our ellipses
  for (int i = 0; i < numEllipses; i++) {
    // Set our fill color
    fill(fillR[i], fillG[i], fillB[i]);

    // Set the stroke to be the same as the fill
    stroke(fillR[i], fillG[i], fillB[i]);

    // Draw the ellipses to the screen
    ellipse(ellipsesX[i], ellipsesY[i], ellipsesW[i], ellipsesH[i]);
  }

  if ((frameCount % 30) == 0) {
    printArray(fillR);
  }
}
```

`printArray()` nicely formats the output.

### Arrays of objects

Let's return to the `Particle` class of last week. Can we create an array of `Particle`s? Indeed we can. Let's take a look at how. Read through the following code and comments carefully:

```java
/*
 * This is the code from last week with our Particle class
 * At this point you should be able to understand how it works
 */

// We declare our array of type Particle
Particle pArray[];

void setup() {
  // We're going to use variables below for setting the width
  // and height of each particle
  float w = 0;
  float h = 0;
  float r, g, b; // this is a shorthand you might not have seen before

  size(600, 600);
  background(0);
  smooth();
  frameRate(30);

  int count = 200;

  // And here we further declare our array of Particles
  // To reiterate: this is an *array* of Particles, so the
  // new keyword here is creating a new *array*.
  // We'll instantiate each particle in this array in a bit.
  pArray = new Particle[count];

  // We iterate over the array
  for (int i = 0; i < count; i++) {
    // Here's where we initialize the Particle class
    // for each element in the array. This is where we
    // actually instantiate each new Particle. The new keyword here
    // is creating a new object of class Particle *each time* in the
    // loop.
    pArray[i] = new Particle(width/2, height/2);

    // Choose random colors for the Particle
    r = random(0, 255);
    g = random(0, 255);
    b = random(0, 255);

    // Set the stroke and fill of the Particle
    pArray[i].setStroke(r, g, b);
    pArray[i].setFill(r, g, b);

    // Get a random width value
    w = random(10, 40);
    pArray[i].setWidth(w);
    pArray[i].setHeight(w);
  }

}

void draw() {
  // We need to clear the screen each frame
  background(0);

  // This is new syntax for iterating over an array
  for (Particle p: pArray) {
    // And this is all we need to draw things!
    p.update();
    p.display();
  }
}
```

So let's look at a couple of things.

* Line 7 is where we declare our array of `Particle`s. We do it just like we would create an array of `float`s, and that's because `Particle` is a data type like we discussed in the previous chapter.
* Line 27 is where we further declare our array, saying that we want to create a new array of `Particle`s that's `count` number long.
* We iterate over the array, and on line 36 we create a new `Particle` object for each element of the array. We then set random values for the color and size of the `Particle`.

Take **special note** of what's going on on line 60. This is a new syntax for the `for` loop. Other languages call this a `foreach` loop. Given the importance of loops for arrays, different programming languages create special loop syntax for iterating through arrays. Here we can think of the syntax like the following:

```java
for (datatype and name: name of array) {

}
```

Let's look at a specific example with our fill colors from before:

```java
for (float f: fillR) {
  println("Fill color is: ", f);
}
```

With this syntax you declare a *loop variable* of the type that's in the array. Then, after the colon, you name the array you're going to iterate over. You then use that loop variable in the block.

Now there are a couple of things to think about with this new syntax:

* This is a useful syntax if you're going to be working with a *single* array in the loop. Since we don't have an index we can't easily work with other arrays.
* Conversely, if you want to work with multiple arrays in a loop, it's preferable to use the standard `for` loop like we were using before.

In this case it makes sense to use this new syntax given that we're iterating over the `Particle` objects and just need to call the `update()` and `display()` methods.

## Further levels of array abstraction, or working in two dimensions

At this point you've learned all of the basics of working with arrays. We know how to iterate over arrays, use array indices or more compact syntax. We know that we can have arrays of objects in addition to `float`s or `int`s.

So the next question to ask is: can you have an array of arrays? Indeed you can. These are called *two-dimensional arrays (2d)* and they're going to be very useful as we transition to talking about images next week. 

The syntax for 2d arrays is just a generalization of what we've done so far. Instead of using a single set of straight brackets we use two sets. Here's how we declare the array:

```java
int[][] pixelR;
```

Here's how we do further declaration in the `setup()` function:

```java
pixelR = new int[300][300];
```

Here's how we iterate over all of the elements of the array. Remember *nested* loops from a couple of weeks ago? This is where they come in handy:

```java
for (int i = 0; i < 300; i++) {
  for (int j = 0; j < 300; j++) {
    pixelR[i][j] = int(random(0, 255));
  }
}
```

We can think of this as a set of rows and columns. When `i` is 0 we're on the first column and then iterate through the rows from 0 to `j`, which is also the `height` of the sketch. Then when `i` is 1 we move to the second column, go down all the rows, and so on until we reach the last column at the right hand side of the sketch at its `width`.

So let's put it all together in a sketch where we set each pixel of the window to a different color.

```java
// Declare our arrays
// We're using ints to cut down on memory usage
int[][] pixelR;
int[][] pixelG;
int[][] pixelB;

// Set some values for the size of the screen
int w = 300;
int h = 300;

void setup() {
  // Note the smaller size!
  size(300, 300);
  frameRate(30);
  smooth();

  // And here we do our further array declaration
  pixelR = new int[w][h];
  pixelG = new int[w][h];
  pixelB = new int[w][h];

  // We're going to loop through the arrays and set
  // a different color for each pixel
  for (int i = 0; i < w; i++) {
    for (int j = 0; j < h; j++) {
      pixelR[i][j] = int(random(0, 255));
      pixelG[i][j] = int(random(0, 255));
      pixelB[i][j] = int(random(0, 255));
    }
  }
}

void draw() {
  background(0);

  // Let's iterate over each pixel in the screen
  for (int i = 0; i < w; i++) {
    for (int j = 0; j < w; j++) {
      // Set the stroke color
      stroke(pixelR[i][j], pixelG[i][j], pixelB[i][j]);
      // This is a primitive we haven't used much
      // but it's perfect for drawing pixels
      point(i, j);
    }
  }
}
```

A couple of notes about this sketch:

* We use `point()` to draw a different color at each pixel.
* We use the loop indices `i` and `j` to refer to each dimension of the array.
* We've cut down on the size of our window, and we're using `int`s for the values rather than `float`s. This is where we have to think about how much work Processing has to do each frame. Think about it: Processing is working with `300 * 300 * 3 = 270000` values each frame. That's a lot! So we have to begin to think about how much processing power and memory our sketch uses. `int`s take up less space than `float`s, so we change to that data type, and then we decrease the size of the window as well.

So now you've just created your first image scratch at the pixel level. Next week we're return to move image manipulation using classes and functions that are optimized to the task.

## Array functions

Earlier I mentioned that the size of an array has to be defined when you declare the array. This might have implied that it's impossible to add or remove elements from the array later on in the program. That's not entirely true. There are a host of *array functions* that can manipulate the size and contents of the array while the program is running. We'll look at two of them `append()` and `shorten()`.

Let's return to our `Particle` example and think about how we could add new particles on screen when someone clicks the mouse button. Here's what we'd need to do in the `mousePressed()` function:

```java
void mousePressed() {
  // Create a new particle based on the position of the mouse
  Particle p = new Particle(mouseX, mouseY);

  // Local variables for colors, size
  float r, g, b, w;
  r = random(0, 255);
  g = random(0, 255);
  b = random(0, 255);
  p.setFill(r, g, b);
  p.setStroke(r, g, b);

  w = random(10, 40);
  p.setWidth(w);
  p.setHeight(w);

  // We have to *cast* the array to our type
  // The new object is added to the end of the array
  pArray = (Particle[]) append(pArray, p);
}
```

We first create a new local variable for the `Particle` object that we're going to create based on the location of the mouse. We then setup the object like we've done previously. The last few lines are the interesting part. We can use the function `append()` to add a new item to the *end* of the array. `append()` takes the name of the array as the first argument, and the item to add to the array as the second argument. But we also have to *cast* the array to our type, with is `Particle[]`, that is, an array of `Particle`s. Just like we used type casting earlier, we do it here. This is a necessary aspect of using `append()`.

We can extend this code to allow us to *remove* the last element from the array. But let's expand upon this possibility with a new provided function, `keyReleased()`. Let's say that we want to *add* `Particle`s when we click the mouse button, and *remove* particles when we click the mouse button while holding down the `shift` key. How could we do that?

First, we'd need a `boolean` to store the state of whether or not the `shift` key is pressed.

```java
// Boolean for shift being pressed
boolean shiftPressed = false;
```

Then we need to take of what happens when we press the `shift` key:

```java
void keyPressed() {
  if (key == CODED) {
    if (keyCode == SHIFT) {
      shiftPressed = true;
    }
  }
}
```

The `shift` key is one of our "coded" keys, so we have to check for that first.

Then, we need to do something for when we release the `shift` key. What could that be?

```java
// This is a function that we haven't talked about
// that is called every time a key is released
void keyReleased() {
  if (key == CODED) {
    if (keyCode == SHIFT) {
      shiftPressed = false;
    }
  }
}
```

Then, we need to check the `boolean` in our `mousePressed()` function:

```java
void mousePressed() {
  // ...
  // This is how we modify elements in the array
  if (shiftPressed) {
    // This will always remove the last element in the array
    pArray = (Particle[]) shorten(pArray);
  } else {
    // This adds an element to the end of the array
    pArray = (Particle[]) append(pArray, p);
  }
}
```

Just like with `append()`, we also have to cast the result of `shorten()`.

How might we *extract* a certain number of values from an array? With the `subset()` function. There are a couple of versions of this function, but the basic idea is to tell `subset()` which item we want to begin with *in* the array (*starting index*), and then how many items to take *including* the first item (*count*). So, in other words,

```java
int[] numArray = {3, 5, 10, 12, 4, 2, 1, 9};
// subset(array, starting index, count)
int[] extractedArray = (int[]) subset(numArray, 2, 3);
// Note that you have to cast the result of `subset()` just like other array functions
// extractedArray is now {10, 12, 4};
```

In this case we begin at index 2 (which is the *third* item in the array) and then we take that item and the next *two* items, so that we have *three* items in total.

This is a bit wonky to wrap your head around, so be sure to go over it again if it's confusing to you.

Let's look at another example:

```java
int[] numArray = {3, 5, 10, 12, 4, 2, 1, 9};
int[] extractedArray = (int[]) subset(numArray, 3, 5);
```

What would `extractedArray` be now?

It's up to you to ensure that *count* is such that you don't go beyond that end of the array. This will lead to an `ArrayIndexOutOfBoundsException`.

Like with other aspects of working with arrays, the real power of a function like `subset()` comes when you use it in conjunction with index variables in loops. But that's also a place where you're likely to have bugs in your code that cause you to go beyond the bounds of the array. So if you keep on getting exceptions like `ArrayIndexOutOfBoundsException` when using `subset()`, look carefully at your starting index and count and ensure that you're not going beyond the bounds of your array.

You can also combine two arrays of the same type together using `concat()`. 

```java
int[] numArray001 = {2, 4, -12, 5};
int[] numArray002 = {15, 3, -47, 0, 34, 127, -25, 6, 5012};
int[] combinedArray = (int[]) concat(numArray002, numArray001);
```

How many elements does `combinedArray` have now? What are they?

<!-- There are a lot of other array functions, like `sort()`, `subset()`, and so on. We'll leave a lot of these to the side until we get into our programming in Python, where it's *a lot easier* to work with arrays, which in Python are called *lists*. -->

## Strings

We're now able to turn to a *sequence* of *characters* in *order*. Hmm...that sounds like an array of characters, which we call a `String`. In fact, many programming languages define a *string* as an array of characters, and allow you to access different characters of the string using array indexing.

In Processing we can use the `String` class to represent strings of text. `String`s are enclosed in **double** quotation marks. And their declaration and initialization is somewhat simpler than for other objects.

```java
// Setup some `String`s
String hello;
String goodbye;
String combo;

void setup() {
  size(600, 600);
  frameRate(30);
  smooth();

  // Even though `String` is a class, we don't have to
  // use the `new` syntax like we've done before
  hello = "Hello, world!";
  goodbye = "Goodbye, interlocutor!";

  // We can "add" strings together like this
  // But you can't subtract, multiply, or divide...
  combo = hello + " " + goodbye;
}
```

We can declare our `String`s as global variables, and then initialize them in the `setup()` function. Remember, a string is enclosed in **double** quotes. 

You can also "add" `String`s together, as seen on line 18. The addition operator merely *concatenates* the strings together.

Given that our `String`s are also classes, there are different class methods that we can use on them. One of these class methods is quite important. Let's say that you want to check if two `String`s are equivalent. What would happen if you wrote an `if` statement like the following?

```java
if (hello == goodbye) {
  // do something
}
```

This *will not work*. What's going on here is that you're determining whether or not the *objects* are equivalent, or, more precisely, whether they're stored at the *same memory location*. This is obviously not what we want, as they are two different variables, two different objects, and are stored at two different memory locations. So we need to use a special class method that allows us to determine equivalence:

```java
if (hello.equals(goodbye)) {
  // do something
}
```

`String.equals()` returns `true` if the *contents* of the `String`s are the same, and `false` otherwise.

There are a number of other `String` methods that you can find in the Processing reference: https://processing.org/reference/String.html.

Two more things. How could you use a double quotation mark within a `String`? By using an *escape character*. You have to use a *forward slash* before *each* double quote that you want to use *within* the `String`, like this:

```java
String escaped = "This is a \"String\" with double quotes (\" \") within it.";
```

And how might you start a new line within your string? With another escaped character, the *newline*, "\\n". Use this combination whenever you'd like to start a new line in your string:

```java
String newlines = "This is on the first line.\nThis is on the second line.\nThis is on the third line.";
```

Here's a complete example with some uses of `String`:

```java
// Setup some `String`s
String hello;
String goodbye;
String combo;

void setup() {
  size(600, 600);
  frameRate(30);
  smooth();

  // Even though `String` is a class, we don't have to
  // use the `new` syntax like we've done before
  hello = "Hello, world!";
  goodbye = "Goodbye, interlocutor!";

  // We can "add" strings together like this
  // But you can't subtract, multiply, or divide...
  combo = hello + " " + goodbye;
}

void draw() {
  background(0);

  // Output the strings to the console
  if ((frameCount % 30) == 0) {
    println(hello);
  } else if ((frameCount % 80) == 0) {
    println(goodbye);
  } else if ((frameCount % 110) == 0) {
    println(combo);

    if (!hello.equals(goodbye)) {
      // We use the forward slash to "escale" the double quote character
      println("\"Hello, world\" is not equal to \"Goodbye, interlocutor\"");
    }

    println("We can also make things lower case");
    println(combo.toLowerCase());
  }
}
```

## Fonts and drawing text on the screen

Now we can *finally* learn how to draw text within our window. As we talked about a few weeks ago, *text is hard*, and the intricacies of fonts are nothing different. Before we talk about fonts, a word about how graphics can be represented in computers. The two main "types" of graphics format are *vector* and *bitmap*. With vector graphics, the graphics are defined using mathematical functions that draw the lines, determine the fill color of an object, and so on. Given that these values are defined using mathematical functions, it means that the graphics can be easily rescaled, resized, and transformed without losing any fidelity. If you create your graphics in a program like Adobe Illustrator, Corel Draw, or Inkscape, you're working with vector graphics. With bitmap graphics, the image is defined by particular colors at particular pixel coordinates. In this care, there is no information about *how* a particular line is drawn; instead, the line is drawn by coloring particular pixels particular colors. This means that it is difficult, and often undesirable, to scale bitmap graphics to be larger than their original size. It's also why bitmap graphics are always given a defined size in pixels, say 300 by 300. Taking photos from your smartphone's camera creates bitmapped graphics, and working in Adobe Photoshop or Gimp is editing bitmapped graphics.

Most fonts these days are stored as vector graphics. This allows the font to be scaled to any *point size* imaginable. For historical reasons fonts in Processing work differently, namely the fonts are stored as bitmap graphics. This is often easier to render, and was important in the early days of Processing in the early 2000s. What this means is that you have to do a bit of work to use particular fonts within your Processing sketch.

![How to create a font in Processing.](media/create_font.png)

You first have to *create* a font in Processing's format. You do this by going to the "Tools->Create Font..." menu item. There you'll see all of the fonts on your system. You have to create a separate font file for each *font family* you'd like to use (like italic, bold, etc.). You also have to decide what *size* in points to create your font. **Note**: choose the largest size you think you're going to need in your sketch. Because these fonts are saved as bitmaps, it will work to scale the font down to *smaller* point sizes, but will start to look blurry if you try and scale the font to *larger* point sizes than you began with.

Processing will then create a font in its special format with extension "vlw", saving it to a `data` directory in your sketch's folder. This folder needs to travel with your sketch for the font to be used.

Now let's look at how to use the font in your sketch, and how to write text to the screen.

Like with our `String`s, we use a special class, `PFont` for loading our font.

```java
// Setup a `String`
String info;

// Declare our font
PFont sourceSansPro;
```

We then need to initialize the `PFont`. Like with `String`, we use a shorthand method that doesn't require using the `new` keyword:

```java
void setup() {
  size(600, 600);
  frameRate(30);
  smooth();

  // Load the font
  // You have to create this font through "Tools -> Create Font..."
  sourceSansPro = loadFont("SourceSansPro-Regular-48.vlw");

  // Set the font
  textFont(sourceSansPro, 32);
}
```

We load the font with `loadFont()`, using the name of the "vlw" file that Processing created. We then set the font to be active by calling `textFont()` with the `PFont` object and the size we'd like to use in points.

To then draw text to the screen we need a `String` and a position in the window:

```java
void draw() {
  background(0);

  info = "Current mouse pointer: " + mouseX + " " + mouseY;
  text(info, 10, 560);
}
```

Here we're taking the current mouse position and writing it to the screen. `text()` takes the `String` to be drawn and the `x` and `y` position of the top left of the `String`s bounding box. Note here how we're using the addition operator with the string to combine different `String`s and variables together.

You can change the color of the text by calling `fill()` before you draw the text to the screen. You can also change how the coordinates of `text()` are used by calling `textAlign()` before you draw the text; see the Processing reference for details.


## Aside: Alpha channels and `color` datatype

So far we've only defined the colors for our `fill()` and `stroke()` using three colors. But we could add a fourth value for the *alpha* or *transparency* channel. Basically this is how *opaque* or *transparent* we want the color to be. The numbers run from 0 to 255, just like with the R, G, B channels. For the alpha channel, 0 is completely transparent, and 255 is completely opaque. If we wanted an ellipse to be 50% transparent red, we could write:

```java
fill(255, 0, 0, 127);
stroke(255, 0, 0, 127);
```

**Important note**: If you want to *fade in* or *fade out*, you can change the alpha channel over time.

One other way to define colors in Processing is to use the `color` datatype. We could have written the previous code as:

```java
color c1 = (255, 0, 0, 127);
fill(c1);
stroke(c1);
```

There are times in the future where using the `color` datatype will be necessary, so it's a good idea to read about it in the reference.

## Assignment

We're going to finish writing our game, building upon the work of last week. Now we can draw text to the screen, add many `Sprite`s to the screen at once, and so on. Here, in no particular order, is what your game needs to do:

* The score should be displayed on the bottom left of the screen.
* When you win, "YOU WIN" should be written in a bold font in the middle of the screen, and all drawing should be stopped.
* When the user clicks on a sprite, that sprite should no longer be drawn to the screen, and the total number of sprites should decrease by 1. **HINT** Think about how to use a loop to determine which sprite at which index has been clicked on. **HINT** Think about how to remove that sprite using the loop index, `subset()`, and `concat()` to cut up and recombine the original array of sprites. **HINT** Come to me if you're completely stuck. **HINT** This is probably the hardest part of the assignment.
* The winning state is when there are no more sprites on the screen.
* The losing state is when there are 50 or more sprites on the screen.
* When you lose, "YOU LOSE" should be written in a bold font in the middle of the screen, and all drawing should be stopped.
* Sprites spawn at random intervals. The number of seconds between sprites spawning will determine the difficulty of the game.
* When sprites are drawn to the screen, they should fade in from nothing to opaque over the course of 2 seconds.
* Pressing 'r' when you have either won or lost resets the game to the beginning again, and starts drawing things anew.

Here are some functions and variables that might be helpful: `concat()`, `subset()`, `loop()`, `noLoop()`, `frameRate`, `textAlign()`, `textFont()`.

It's probably best to think through these requirements and decide what is most important, and in what order you should try and implement things.

The assignment is due on Thursday night as usual at 11:59PM.

Good luck!
