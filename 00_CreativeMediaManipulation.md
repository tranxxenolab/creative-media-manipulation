---
title: "Creative Media Manipulation"
author: [Adriana Knouf]
date: "2021-02-20"
subject: "Processing, Python, Coding, Media"
keywords: [Processing, Python, Coding, Media]
subtitle: "Or, how to manipulate the materiality of digital media"
lang: "en"
book: true
titlepage: true
titlepage-rule-color: "360049"
titlepage-background: "backgrounds/background1.pdf"
suppress-bibliography: true
linkcolor: blue
mainfont: Adobe Jenson Pro
sansfont: Source Sans Pro
monofont: DejaVu Sans Mono
papersize: letter
header-includes:
    - \lstset{breaklines=true}
    - \lstset{language=Java}
    - \lstset{basicstyle=\small\ttfamily}
    - \lstset{extendedchars=true}
    - \lstset{tabsize=2}
    - \lstset{columns=fixed}
    - \lstset{showstringspaces=false}
    - \lstset{frame=trbl}
    - \lstset{frameround=tttt}
    - \lstset{framesep=4pt}
    - \lstset{numbers=left}
    - \lstset{numberstyle=\tiny\ttfamily}
    - \lstset{postbreak=\raisebox{0ex}[0ex][0ex]{\ensuremath{\color{red}\hookrightarrow\space}}}
---

     
\vspace{10cm}
\noindent
Copyright 2018--2021, Adriana Knouf.

\vspace{1cm}

\noindent
<https://zeitkunst.org>

\noindent
<asknouf@zeitkunst.org>

\noindent
\@zeitkunst on Twitter and Instagram

\vspace{1cm}

\noindent
<https://tranxxenolab.net>

\noindent
<asknouf@tranxxenolab.net>

\noindent
\@tranxxenolab on Twitter and Instagram



\vspace{1cm}


\noindent
This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).
