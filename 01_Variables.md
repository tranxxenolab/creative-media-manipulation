# Variables, or how to store things

## A bit about variables, before we turn to numbers

As we begin to learn the fundamentals of programming, we're going to start with one of the most basic elements, namely *variables*. There are a lot of ways to talk about variables, and different metaphors that can be used to help this understanding. But I think most of these metaphors fall flat, so let's just define variables this way:

variable

: A named location in the computer's memory for storing a value that can change

So there are a few things to unpack here. Let's go through them one by one.

1. "computer's memory": this refers to the part of your computer (or smartphone, or Arduino, or or or...) that is designed to store information temporarily. This is also known as RAM (for Random Access Memory). This information only exists in memory as long as the program is running, and is lost once the power is shut off to the computer.

2. "storing a value": Different types of values can be stored, and oftentimes the computer needs to be told what kind of value you're asking to be stored, as it (the computer) needs to *allocate* a specific amount of space to store that value. We'll get into this in more detail in a bit.

3. "named location": When you *declare* a variable (more on this in a bit as well), you're just creating a *reference* to a location in memory. Your "named location" (or *variable name*) is merely a *pointer* to the place in memory where the value of the variable is actually stored[^pointer]. 

[^pointer]: This gets to a fundamental aspect of computing that we're not going to get into in this class, namely the distinction between passing variables (that is, using variables as inputs to other functions) by *reference* or passing variables by *value*, and the importance of *pointers*. The more technical definition of a variable is thus: a variable stores the *address* which *points* to another place in memory where the value actually resides. All of this complexity is thankfully abstracted away for you by Processing, so you don't have to worry about it. Merely remember the definition of variable given above. I'm including the detail here for those of you who are interested. 

4. "value that can change": As the name *variable* suggests, the values that variables hold can change, and they can change over the lifetime of your program. That's what allows programs to do interesting things, otherwise the program's behavior would be hard-coded at the time of programming. There are a few exceptions to this in other programming languages, but we're not going to get into them now.

So a variable allows you to store particular kinds of data in memory. The values can change, and they often change in response to the input or activity of users[^users].

[^users]: A note on the use of the word "users". Many areas of computer science refer to the people who interact with programs as "users". This language implies a certain hierarchy, where the programmer creates something that another person "uses", thus denying a certain amount of agency to said "user". While I may use the word "user" throughout these texts and lectures, you should always question it, and think about what other words or terms could be used instead, and what it would mean to consider the "user" as a co-creator of the human/computer system.

Two slightly related things: 

* I'm going to be using the word *function* with some regularity. We'll be writing our own functions later on, but for right now think of a function as a repeatable, modular bit of code that we can call that does something for us.
* Sometimes in the code below you'll see me use two forward slashes, `//`. That sets off a part of the code called a *comment*. A comment is ignored by the computer when it runs your code, and exists only for you, the programmer. It's a good idea to use comments to help you document your programming. We'll get into comments in later parts of the course.

Before we get to talking about variables in more detail, we need to take a detour to talk about numbers and their representation.


## Numbering systems, or counting with 2 or 16 fingers

### Binary counting

If we know one thing about computers, it's that they're machines that work with *binary* numbers. What does that mean? In short, it means that computers can, at a very fundamental level, only represent two numbers: a zero, or a 1^[At a fundamental level, however, computers work with *voltages*, which can vary between a variety of values, usually anywhere from 0 to 5 volts. What a computer chooses to recognize as a "1" or a "0" depends on how one *thresholds* a voltage. Going into this further is beyond the scope of this class, but is an interesting topic in and of itself. In actuality, all so-called "digital" computers are actually analog devices!]. But by clever representations we can use binary numbers to represent any other number possible. How do we do that? Let's take a few examples.  If we have one binary digit, or *bit*, we can represent a 0 or a 1 (and we usually use the lower-case "b" at the end of the number to denote binary representation):

```java
0b // The number 0
1b // The number 1
```

How do we represent the numbers higher than this? Add another binary digit, or two bits:

```java
10b // The number 2
11b // The number 3
```

And higher than this, with three bits?

```java
100b // The number 4
101b // 5
110b // 6
111b // 7
```

Do you see the pattern? Just like with a decimal system, we take increasing exponents of the base, in this case base 2, going from right to left. So to get the number 5 above, we do the following:

```java
     //    1         0         1
101b // 1*(2^2) + 0*(2^1) + 1*(2^0) = 5
```

So what would the following binary numbers be in their decimal representations?

```java
10010b
1010b
11101010b
101010b
```

One of the best explanations of binary numbers can be found in the short film called [*A Communications Primer*](https://archive.org/details/communications_primer) (1953) by the designers and husband and wife team, Charles and Ray Eames. This short film also provides a great explication of the theories of information put forth by Claude Shannon, an incredibly important influence on our concepts of information that still shapes our thinking today in many disciplines, from computer science to psychology. If you have time, I very much recommend watching this film.

### Imagine you have 16 fingers...

You often don't have to work with binary numbers. But for better or for worse, you *do* often have to work with hexadecimal numbers, or base-16. So to represent *these* numbers we use the numerical digits from 0-9, and the letters A-F, and preface these numbers with "0x". You'll see in a bit why this is useful. Basically you work with hexadecimal just like you would with binary or decimal numbers:

```java
0x01 // 0*(16^1) + 1*(16^0) = 1
0x0F // 0*(16^1) + 15*(16^0) = 15
0x10 // 16
0x1F // 1*(16^1) + 15*(16^0) = 31
0xF0 // 15*(16^1) + 0*(16^0) = 240
0xFF // 255
```

Now it just so happens that computers like to do things in multiples of 8. And 8 bits allows for a possibility of 256 values, 0-255. And in hexadecimal you can represent that with two characters. Do you see now why hexadecimal would be helpful for representing numbers?

Try and convert the following hexadecimal numbers into decimal numbers:

```java
0xF6
0xB2
0x3B
0x2C
0xC1
```

### NUMBERING STARTS FROM ZERO

I put the title of this subsection in ALL CAPS because it's incredibly important. Let me put it again in all caps and in bold:

**NUMBERING ALWAYS STARTS FROM ZERO**

Computers start counting from zero. Thus counting eight items means counting 0, 1, 2, 3, 4, 5, 6, 7. You don't get to eight. You don't count eight as part of the eight items. You end at seven[^monty]. But you started from 0, so you actually have 8 items. This will become very important in a few weeks. But it's best to commit this to memory right now. In computers,

[^monty]: [Obligatory geek reference to *Monty Python and the Holy Grail*.](https://www.youtube.com/watch?v=xOrgLj9lOwk)
 
**NUMBERING ALWAYS STARTS FROM ZERO**

**NUMBERING ALWAYS STARTS FROM ZERO**

**NUMBERING ALWAYS STARTS FROM ZERO**

### But I'm more than just whole!

So far we've been considering numbers that are just *whole* numbers, not fractional, not rational, not irrational, not complex. It turns out that how to represent, in a computer, values that are not whole numbers is a rather complex topic in and of itself. Thankfully we don't need to worry about the details, but let's recall some definitions:

Whole number

: A number without a fractional component, i.e., 0, 1, 2, 3, 4

Integer

: Whole numbers both negative, positive, and 0, i.e., -2, -1, 0, 1, 2

Rational number

: A number that can be represented as the ratio of two integers, i.e., -2/3, 3/5, 4/7, -3/8

Irrational number

: A number that cannot be represented as the ratio of two integers and which doesn't repeat nor become periodic, but which also lies in-between two integers, i.e., 3.14159... (pi) 

Real number

: The set of rational and irrational numbers

We need to know these basics to know what *type* to give our variables.

## Variable types

As mentioned previously, when we *declare* a variable (and we're getting to that in just a moment!), we have to also decide upon its *type* so that the computer knows how much memory to reserve for its *value*. There are a few fundamental types, and this is where we'll connect with our previous review of numbers.

To represent numbers there are two oft-used types:

* `int`: for integers
* `float`: for real numbers

So, let's decide what types we'd use to represent the following numbers:

```java
-10000
172.4567
2
9
-128.4586949309684
```
Now, there are *limits* to how precise a computer can represent a number in binary form, and this is actually a very important issue for some areas of programming. But for most cases this semester we won't have to worry about this; using `int` and `float` for integers and real numbers, respectively, is all you need to know. If interested, you can look up these other number types: `byte, short, long, double`.

Now there are a couple of other types that will be useful for you going forward:

* `boolean`: a value that only be `true` or `false`
* `char`: a single letter, like `c`

Don't worry too much about these two types for now; we'll return to them when we talk about program flow and conditionals.

## Variable declaration

Now that we know about types we can pull it all together and talk about how to *declare* our variables.

There are limitations on how we can name our variables. Variable names:

* Have to begin with a letter
* Can only consist of letters, numbers, and the underscore `_`.

But there are also some good guidelines to follow, that aren't strictly requirements. Of course your variable name should be descriptive. Calling a variable `a` likely isn't going to be too useful to you[^returning]. So if you're creating a variable about the width of an ellipse, you might want to name the variable `ellipseWidth`. Do you see what I did there? I started the variable name with a lower case letter, and then changed to Upper case when I moved to the next word. This is just a *convention* in Processing, but a useful one to follow. In almost all cases, it's best to begin your variable names with a lower case letter; you'll understand why in a few weeks.

[^returning]: One thing to think about vis a vis variable names: will you know what the variable name means if you were to return to your code three months or three years from now? If so, that's a good variable name.

So a variable declaration consists of its *type*, its *name*, and, optionally, its *initial value*, which is termed *initialization*. Here's an example:

```java
int ellipseWidth = 40; // type (int) variable name (ellipseWidth) initial value (40)
```

We've just declared a variable named `ellipseWidth` as an integer and set its initial value to be `40`.

You can declare variables without initializing them, but then you might not know what value they have! So it's best to always initialize them. If you don't know what to initialize them to, set them to 0 for numbers, 'a' for letters, and 'false' for booleans.

How would you declare the following?

```java
// an integer that stores the rectangle's width and has a default of 32?
// a boolean called toggle that begins false?
// a float called distance that defaults to 23.45?
```

Now you can use all sorts of arithmetic operations on variables too. *In general* you should only combine variables of like types, so only `float`s with `float`s and `int`s with `int`s. If you have two types of variables that are of different types but you'd like to combine them together you can *type cast*; see below.

```java
// Notice how I'm using parentheses to be very explicit about what order of operations I want to use
int size = 30;
int inc = 5;
int dec = -40;
int finalIntValue = size + (2 * inc) + (15 * dec) // what's finalIntValue?

float base = 3.459;
float exponent = 2.678;
float correction = 34.6874;
float finalFloatValue = (pow(base, exponent)) - (5*correction) // what's finalFloatValue?
```


### Where or where do I declare?

Now comes the question of *where* in your code you should declare your variables. Until we get to the question of *scope* in a couple of weeks, the best place to declare your variables is at the top of your code, before the `void setup()` function. This makes your variables *global* and accessible anywhere in your program. We'll get into the details of *global* versus *local* variables in the coming weeks.

### Converting variables from one type to another, or *type casting*

Sometimes you might want to convert a variable of one type to another type. For example, some code might want an `int` but you only have a value that's a `float`. How can you convert form one type to another? You can do what's called *type casting*[^typecasting]. So let's say you had the following:

[^typecasting]: Yes, this is one of those places in programming where the terminology conflicts with regular usage. In programming, type casting is not necessarily a bad thing!

```java
float currentValue = 6.436;
int redColor = 0;
redColor = int(currentValue);
```

So what's going on here? We have a `float` that we then want to convert to an `int`. We do this by using the function `int()` to *cast* the `float` to an `int`. What value do you think `redColor` will now have? If you said `6` you'd be right. Type casting a `float` to an `int` *truncates* all of the numbers past the decimal point. So if you had a `float` that was, say, `-7.5`, casting it to an `int` would give you `-7`[^round].

[^round]: If you really want to round your value, use the function `round()`. You will still need to cast it to an `int`, but at least you will be explicit about what you're doing.

Why do you want to type cast? Remember the code that sets the color of the stroke of our lines or shapes? The values that you pass should be integers. If you have a float, that you might have gotten from another function or from somewhere else in your code, you should cast it to an integer.

To convert an `int` to a `float`...can you guess?


## Using variables in our example from last week

Let's take our code from last week and add variables that allow us to easily change the colors of the background and the stroke of the line. First, here's the original code:

```java
void setup() {
  size(400, 400);
  background(0, 0, 0);
}

void draw() {
  stroke(255, 0, 0);
  line(pmouseX, pmouseY, mouseX, mouseY);
}
```

How might we change this to use variables? The following is one example:

```java
int backgroundRed = 0;
int backgroundGreen = 0;
int backgroundBlue = 0;

int strokeRed = 255;
int strokeGreen = 0;
int strokeBlue = 0;

void setup() {
  size(400, 400);
  background(backgroundRed, backgroundGreen, backgroundBlue);
}

void draw() {
  stroke(strokeRed, strokeGreen, strokeBlue);
  line(pmouseX, pmouseY, mouseX, mouseY);
}

```

Now, this may at first look like extra work. We've added more lines to the program, and perhaps they're unnecessary. To know what values you should turn into variables is something you learn with practice, as there are no hard-and-fast rules. Nevertheless there are a few guidelines:

* If you think you're going to re-use a value somewhere else in the program, turn that value into a variable.
* If you think that value might change over the course of the program, turn that value into a variable.
* If it will improve readability, turn that value into a variable.

I will be grading your code, in part, on how you name your variables.

## Randomize

How to generate truly random numbers is another fascinating area of research. But suffice it to say that your computer is not able to generate *truly* random values, but for most of our purposes the randomness is "good enough". There's a function appropriately called `random` that allows us to get a random number between two other numbers. Every time we call this function we get a different random number. Here's an example:

```java
float randomValue;
// random(lower bound, upper bound);
// This function returns a float
// Note that if you run this code yourself
// `randomValue` will take on different values than
// the ones below
randomValue = random(0, 255); // randomValue is 230.52736
randomValue = random(0, 255); // randomValue is 81.16182
randomValue = random(0, 255); // randomValue is 69.17902
randomValue = random(0, 255); // randomValue is 25.321993
```

Now, let's say that you wanted `randomValue` to be an `int`. How would you do that? Can you think of the most simple way to do it?

## Provided variables

Now there are some variables that Processing provides for you that are automatically updated. It also means that your own variables *should not* be named the same as the provided variables. That bears repeating: *never* name your own variables the same as the provided variables. To do so is to invite frustration, and that's not something I want you to experience. If something strange is going on with your sketch, a good thing to look for are any locations where you may have inadvertently named your own variables the same as the provided ones.

With that out of the way, here are some of the more useful provided variables; we'll come across others later on:

* `width`: the width of the Processing window
* `height`: the height of the Processing window
* `mouseX`: the current X coordinate of the mouse
* `mouseY`: the current Y coordinate of the mouse
* `pmouseX`: the previous (last frame) X coordinate of the mouse
* `pmouseY`: the previous (last frame) Y coordinate of the mouse

## Events

So far we've only written code before `void setup`, code in `void setup`, and code in `void draw`. Let's go into a bit more detail about what's going on here:

* Code before `void setup`: consists of our variable declarations
* Code in `void setup`: code that runs only when we start the program, and defines things like the size of the window and the background color of the window
* Code in `void draw`: code that is called on every *frame*

We could spend a lot of time talking about what a *frame* is, and we'll delve into this in a bit more detail in later weeks. But for now, think of a frame in the same way as a movie frame: a sequence of stills that are generated at a regular rate based on the code that you put in `void draw`. We see see *movement* because the frames are generated so quickly that our brains "fill in the blanks" in what is called *persistence of vision*. Things work a bit differently on computer screens than with film reels, but the basic principle is the same.

But let's say that we want to respond to something that the user does, like click the mouse button or press a key. Then we have to deal with an *event*. We don't know when the event will happen, we just know that we need to respond to it when it occurs.

This turns out to be a rather difficult problem, but thankfully Processing takes care of the heavy lifting for us. We can write code in another function called `void mousePressed` that is called every time you click a mouse button. Let's look at how we can draw an ellipse on every mouse click:

```java
void mousePressed() {
  stroke(0);
  fill(175); // If there is only one number, then the number is repeated for red, green, and blue, meaning that the fill color here is a medium light grey
  ellipse(mouseX, mouseY, 20, 20);
}
```

There is a similar function for when a key is pressed, `void keyPressed` that we'll return to in the future.

## Drawing primitives

In our example from last week we used a couple of different *drawing primitives* like the line and the ellipse. Let's go through each of them in order of increasing complexity.

Before we do, though, let's talk about the coordinate system of the Processing window. We refer to *points* in this window using standard Cartesian coordinates, with the x-coordinate moving from left to right and the y coordinate moving from top to bottom. The *origin* of this coordinate system, (0, 0), is at the top left of the window^[This origin can be changed, and we will be doing that in a couple of weeks.]. So the x-coordinate increases as you move from left to right, and the y-coordinate increases as you move from top to bottom. Thus, if you have a window that is 200 pixels wide and 200 pixels high, the center of the window is at coordinate (100, 100), or `(width/2, height/2)`.

In all of the examples that follow, assume that the window is 400 pixels wide by 400 pixels high (created in your `setup` function using `size(400, 400);`).

Let's say you want to draw a point in the middle of the screen. Using two of the *provided* variables that we talked about earlier, this would work:

```java
point(width/2, height/2);
```

![A point in the middle of the window](media/resize/point.png)

And a line from the top left to the bottom right, with the first two numbers being the starting coordinates of the line, and the second two numbers being the ending coordinates of the line:

```java
line(20, 20, 275, 320);
```

![A line from the top left to the bottom right](media/resize/line.png)

Rectangles are a bit more complicated. There are two main ways to define rectangles. The default way asks you to define the top left coordinate of the rectangle, along with its width and height in pixels:

```java
rect(20, 20, 275, 320);
```

![A rectangle defined by its top left coordinate and width and height](media/resize/rect.png)

We can change the way the `rect()` function works by calling another one just beforehand, `rectMode(CENTER);`. In this case, we define the *center* of the rectangle with the first two numbers, while the second two numbers define the rectangle's width and height:

```java
rectMode(CENTER);
rect(width/2, height/2, 50, 20);
```

![A rectangle defined by its center coordinate and width and height](media/resize/rect_rectModeCENTER.png)

Now, drawing circles and ellipses. A circle is just an ellipse with the same width and height. In Processing the default for the ellipse is to use the first two coordinates to define the center of the ellipse, and the last two to define its width and height:

```java
ellipse(100, 120, 30, 30);
```

![A circle defined by its center coordinate and width and height](media/resize/ellipse_circle.png)

And an ellipse:

```java
ellipse(100, 120, 10, 50);
```

![An ellipse defined by its center coordinate and width and height](media/resize/ellipse_ellipse.png)

## Drawing primitives in modern art

The American conceptual and minimal artist Sol LeWitt created a number of *Wall Drawings* over the course of his life that consist of a series of instructions. The drawing itself can then be done by anyone else who follows these instructions exactly. MassMoCA, the Massachusetts Museum of Contemporary Art in North Adams, MA, contains three floors of his realized *Wall Drawings*. One of them, *Wall Drawing 1180* (2005), consists of the following instruction:

> Within a four-meter (160”) circle, draw 10,000 black straight lines and 10,000 black not straight lines. All lines are randomly spaced and equally distributed.

A close-up image of this drawing can be seen in the following image.

![*Wall Drawing 1180* (2005; detail). See more at \url{http://massmoca.org/event/wall-drawing-1180/}.](media/sol_lewitt_1180-1.jpg)

A major part of LeWitt's artwork is the necessary *duration* and *concentration* it takes to realize the artwork on the wall of the gallery. Using some of the information you've learned here, in terms of drawing primitives like lines, would allow you to realize a digital version of LeWitt's artworks, like *Wall Drawing 1180*, in seconds. What is the purpose, then, of LeWitt's artwork in the age of digital technology? How would a digital realization of a LeWitt drawing be similar or different from a LeWitt drawing made using a marker on the wall?

## Assignment

Assignments are due Monday nights at 11:59:59PM. You should upload the assignment into a new folder on the Sakai dropbox for that week's assignment. This is `01_Variables`.

For both programming parts (parts 2 and 3) also include a screenshot of your working program.

You can use Cyberduck on a Mac, or WinSCP on Windows, to upload your files. When you save your Processing sketch, please upload the entire *folder* that contains your code, not just the file(s) ending in `.pde` file by themselves.

**NOTE ON SOLUTIONS**: You may know of students who have taken this course in the past. *You are not allowed to look at their solutions to these assignments*. While there are many ways to solve these assignments, viewing previous years' solutions will give you an unfair advantage. So please don't do it!

**NOTE ON COLLABORATION**: I fully expect you--and encourage you!--to discuss the topics of the course with other students. This can help your understanding greatly. *However*, all solutions that you submit *must be your own work*. Thus, to be explicit: you can talk about *how* to solve a problem with other students, but you cannot share specific *fragments of code*. There may be times during the semester where I will allow you to work *collaboratively* on a problem. But by default, what you submit should be the result of your own work. I do this not to *discourage* the idea of collaboration, but because I think there is great value in the feelings of "a ha!" that come with solving a problem on your own--and sometimes solving that problem in consultation with others!

If you have any questions about this policy please let me know.


1. Convert the following hexadecimal numbers into decimal. Show your work.

* `0x49`
* `0x9E`
* `0x52`
* `0x7C`
* `0x11`

Name whatever file you use to upload this work LASTNAME_FIRSTNAME_hexadecimal.

2. Modify the code of last week in the following ways:
* The color of the line needs to change color randomly as the user moves the mouse within the window.
* The user can draw an ellipse whenever she/they clicks the mouse button. The color of the ellipse needs to be different each time she/they clicks the mouse.

 Name this sketch LASTNAME_FIRSTNAME_Variables_random.

3. Draw a face using the drawing primitives explained earlier. Your face needs to use at least five different primitives. (To be pedantic: if you used five different line segments to draw your face, that would be enough to meet the basic requirements.) Expression, colors, and complexity are up to you. Name this sketch LASTNAME_FIRSTNAME_Variables_face.
