# Images and video, or manipulating pixels; and a bit of sound

<!-- working with pixels, 2d into 1d, animation, tint etc, different types of algorithms for working with images and manipulating them, beginning of 3D coordinates (?) -->

We've now learned all of the basics of *object oriented programming*, or OOP. Think about how far you've come so far. You perhaps began without even knowing what a variable was, and now you know about variables, conditionals, loops, functions, classes and objects, and arrays. These are the building blocks for everything else that we're going to be exploring. Not that things will necessarily be easy from here on out; but rather you now have the foundations for everything to come.

We're going to start working with images and video this week. First, we'll learn how to efficiently load, create, analyze, and manipulate images. Next, we'll work with your computer's webcam to do the same for video, as video is basically just a series of still images. For this week in particular, I've extensively drawn upon Daniel Shiffman's chapters on images and video in *Learning Processing*; see Chapters 15 and 16 for more details. Many of the examples below come directly from his book.

I'm also going to be giving less detail than I have been in previous weeks. We're at the point in the semester, and in your learning about programming, where it's useful to start *learning how to learn programming*. What I mean by that is that at a certain point you're going to want to do things that we haven't *specifically* learned in class. That's going to require some extra work on your part to read the documentation, try and experiment, probably fail, try and experiment again, maybe fail again, but try and experiment yet again and finally make things work. That sort of iterative process is very much a part of programming. It can be frustrating, I will admit. But the rewards are worth it when you feel the accomplishment of making something work.

## Loading and animating images

So the first thing you might want to do is to load an image from a file. To do this we use another custom class that Processing provides for us called `PImage`. And, like you discovered when using fonts, you have to have your images in the `data` folder within your sketch. You can either create that folder manually, or you can have Processing create it for you when you choose "Sketch -> Add File". For a number of the following examples I'm going to use images from NASA's Chandra X-Ray telescope that you can freely download from the web.

To load images and display them in the window, we can do the following:

```java
// Image from https://www.nasa.gov/mission_pages/chandra/galaxy-pictor-a.html

// Processing provides a `PImage` class for working with images
PImage img;

void setup() {
  size(500, 500);
  
  // Load the image from a file
  // This image needs to exist in the "data" folder
  img = loadImage("pictora_500x500.jpg");
}

void draw() {
  // Display the `PImage` starting from the upper left corner
  image(img, 0, 0);  
}
```

A few things to note here:

* You use objects from the `PImage` class to work with images
* Like with `String`, you don't have to use `new` to create a new `PImage` object, but rather just load an image using `loadImage()`, which returns a new `PImage` object. This image *needs* to already exist within your `data` folder, either put there manually or added through "Sketch -> Add File".
* Processing can load the following kinds of image files: .gif, .jpg, .tga, .png.
* You should *always* load your images in your `setup()` function. Think about what happens if you try to load images in your `draw()` function...you'd be loading the image every frame of your sketch! That's going to slow things down dramatically. Reading files from disk is a relatively slow operation, and it's best to do that *before* the draw method.
* To actually display the image you use the `image()` method. The first argument is the `PImage` object that holds the image data, and the second two arguments are the `x` and `y` coordinates of where the upper left corner of the image should start. In this case, we've chosen an image that is 500 by 500 pixels wide, and the window is 500 by 500 pixels wide, so the image fills the entire window. What would happen if the `x` and `y` values were 100 and 200, respectively?

![Loading an image from a file.](media/loadImage.png)

So, now we know how to load a single image. Let's see what we can do with the image data right now.

## Simple image manipulation using `tint`

Let's say that once we've loaded the image, we'd like to manipulate some aspects of the color or transparency. Processing offers a function called `tint()` that has a fair number of arguments to it, so it's best to look at the reference[^tint] for the full details of how it works.

[^tint]: https://processing.org/reference/tint_.html

But the basic idea can be seen in the following example:

```java
// Image from https://www.nasa.gov/mission_pages/chandra/galaxy-pictor-a.html

// Processing provides a `PImage` class for working with images
PImage img;

void setup() {
  size(500, 500);
  
  // Load the image from a file
  // This image needs to exist in the "data" folder
  img = loadImage("pictora_500x500.jpg");
}

void draw() {
  // Standard tint
  tint(255);
  // Display the `PImage` starting from the upper left corner
  image(img, 0, 0);
  
  // Change the values below to see what happens
  tint(255, 0, 0);
  // Display the tinted image halfway across the screen
  image(img, width/2, 0);
}
```

In this example we display the normal image on the left hand side of the screen. On the right we have the tinted image. On line 21 we say that we want to display the full red channel, and then none of the green or blue channels. (The values for `tint()` work on a range of 0 to 255, just like other color functions we've seen before.) Thus, the tinted image has a red cast to it.

![Tinting an image.](media/loadImage_tint.png)

As another example, try to change the `tint()` function to the following:

```java
tint(0, 255, 0, 140);
```

Here we use only the green channel, but also add an alpha channel of 140, thus about 55% transparent. In the image you can see the greenish cast to the image, as well as some of the background (original) image showing through.

![Tinting an image with transparency.](media/loadImage_tint_transparency.png)

Again, take a look at the reference for more examples.

## Arrays of images, or animation!

If we can load single images, then we can also load multiple images into an array. And if we can load multiple images, then we can create simple animations!

In short, film, video, or animation simply consist of a number of *still* images presented in sequence *frame by frame*. Coupled with a psychological phenomenon known as *persistence of vision*, this allows us to *perceive* the *illusion* of motion. Because it's just that: an illusion. If you ever have a chance to look at old film reels (say of 8mm or 16mm film) you'll see that the film consists of individual frames, and it's the projection of the sequence of these frames, one by one with a shutter in-between, that allows us to see motion.

Thus, if we have an array of images in Processing, then we can create the illusion of motion from individual frames.

For this example I've taken a short video of my hand waving, and cut out my hand from the background into individual frames. (I did a quick and dirty job on this; if you were to do this professionally you'd want to take more time on the Photoshop work.) I've created 10 individual frames that I've then put into the `data` folder for the sketch[^png]. Let's go through the Processing code bit by bit to see how we take the frames and turn them into an animation.

[^png]: You can save your `.png` files with transparency, which allows you to have your frames appear over whatever type of background you'd like.

```java
// Create an array of PImages
PImage[] frames;

// Set the number of frames
int numFrames = 10;

// Set a counter to keep track of what image we're on
int counter = 0;

// Set a boolean to know whether to decrease the counter
boolean decrease = false;
```

We declare an array of `PImage` objects that we'll initialize shortly. We set the number of frames in our animation, and we declare a couple of variables that we'll use to keep track of where we are in the animation sequence.

Each of our frames is named in a special fashion: animation_frames_000.png, animation_frames_001.png, animation_frames_002.png, and so on. This will come handy in our `setup()` function.

```java
void setup() {
  size(1000, 900);
  
  // Create an array of images
  frames = new PImage[numFrames];
  
  // Load all of our frames
  for (int i = 0; i < numFrames; i++) {
    // This allows us to load each image programatically!
    frames[i] = loadImage("animation_frames_00" + i + ".png");  
  }
}
```

So like we did with our arrays before, we have to declare our array with a specific size, `numFrames`. Then the magic occurs in our `for` loop. Because we named our frames in a specific manner, we can easily load each image programmatically. This is a common idiom in programming, and is one reason why you'll often see files named in the fashion I described above[^nf].

[^nf]: What happens if we have more than 10 images? Then we'll be trying to load `animation_frames_0010.png`, which is probably not what we want. Take a look at `nf()`, which allows us to format a number into a string. So, for example, we might want to allow only three digits (allowing for up to 999 images), with zeros *padding* the number. We could use `nf()` to do that, where `nf(10, 3)` would output "010". Using `nf()` our code, to be more general, would read: `frames[i] = loadImage("animation_frames_" + nf(i, 3) + ".png");`.

Now we're set to draw the frames to the window:

```java
void draw() {
  background(255);

  // Display the specified image
  image(frames[counter], 0, 0);
  
  if ((frameCount % 2) == 0) {
    if (decrease) {
      counter -= 1;
    } else {
      counter += 1;
    }
  }
  
  // Note the numFrames - 2 to take into account zero indexing
  if (counter > (numFrames - 2)) {
    decrease = true;  
  } else if (counter == 0) {
    decrease = false;  
  }
}
```

![Animation through displaying successive frames.](media/loadImages_animation.png)

We clear the background each time through the animation, since we're showing frame by frame. Then we display the frame at the current `counter` value. We then update the counter. If we just started over each time the counter reached the maximum, `numFrames`, then we'd get an ugly "break" in the animation as it started over from the beginning. So the best thing to do is to *decrease* our counter so that it goes back to 0. We do this by checking a `boolean`, `counter`, which determines whether or not we increment or decrement the counter. We do this every other frame so as to have a bit slower animation. Then we have to determine whether or not to set `decrease` to `true` or `false`, and we do that by comparing `counter` to its minimum and maximum values. Thus `counter` oscillates between 0 and 9, the maximum number of frames.

Now let's look at some other kinds of animation we can do with a single frame.

## Resetting the origin...

If you recall from a while back, we used the `translate()` function to reset the origin of our sketch. This allowed us to easily draw without having to do complicated calculations with respect to an origin at the top left of the window. We're going to do the same thing with a single image, allowing us to move the image all around the screen. We'll also add in another related function, `rotate()`, that does the same thing, only with angles.

Let's look at how we can code this:

```java
// The variable to hold our image
PImage img;

// Variables to hold position and angle
float x, y;
float angle;

// How much to rotate the angle each frame
float rotValue = 0.1;
```

We just need a single image object. Then we need some variables to keep track of the current position and angle; this is what we're going to pass to `translate()` and `rotate()`, respectively. Then we need a value by which to increment the angle.

`setup()` looks pretty straightforward:

```java
void setup() {
  size(500, 500);
  
  // Default initial variables
  x = width/2;
  y = height/2;
  angle = 0;
  
  // Load our image
  img = loadImage("hand.png");
}
```

We set some default values for the variables. The real interesting parts come in `draw()`:

```java
void draw() {
  // Clear the screen each frame
  background(255);

  // Translate to a new origin, and rotate to a new angle
  translate(x, y);
  rotate(angle);
  
  // Rather than displaying the image using the top, left coords
  // set the coordinate origin to the center of the image
  imageMode(CENTER);
  image(img, 0, 0);
  
  // Update the x and y values
  x += random(-3, 3);
  y += random(-3, 3);
  angle += rotValue;
  
  // Ensure that we don't exceed the bounds of the window
  if (x <= 0) {
    x = 0;
  } else if (x >= width) {
    x = width; 
  }
  
  if (y <= 0) {
    y = 0;
  } else if (y >= height) {
    y = height;
  }
}
```

Just like we did when we were drawing the parametric lines, we translate to a new origin based on our `x` and `y` variables. We also *rotate* our coordinate system to the given `angle`. **Note**: the `angle` argument to `rotate()` is given in radians.

![Translating and rotating an image file to create animation.](media/rotated_image.png)

Then we actually draw the image to the screen. Like with rectangles, we can change the `imageMode()` for drawing to be `CENTER`, thus allowing us draw the image using the *center* of the image as the origin, rather than the top left. We thus can draw the image very simply in the next line of code, drawing it at `0, 0`. But, given that we had translated and rotated the origin in the earlier lines, this simple `image()` code actually gives us varying output. The next set of lines updates the `x`, `y`, and `angle` values, and the last lines ensure that we don't go outside the bounds of the screen.

This allows us to move images around the screen at will. This means that you can easily use images as sprites! To do this, however, we need to talk about one issue with `translate()` and `rotate()`.

When working with graphics transformations, behind the scenes we are really changing the values in various *transformation matrices*. Any graphics transformation can be expressed in matrix form. Thus, when we are translating or rotating something on screen, we're simply changing the values in the corresponding transformation matrix.

What that means is that we need to keep track of the *graphics state* when we do these transformations. If we don't, then every time we call `translate()` or `rotate()`, we will be doing that *with respect to the last transformation*. So, for example, if we translate 200 pixels to the right, and then rotate by `PI` radians as our first transformation, all further transformations will be *relative* to that last transformation. That would get confusing very quickly!

Enter `pushMatrix()` and `popMatrix()`. This allows us to *push* or *pop* various transformations onto the *stack*. This gets a bit tedious, so bear with me. The idea is that there is a increasing or decreasing "stack" of graphics transformations that Processing (and the underlying drawing code) keeps track of. Say you want to keep track of the current set of transformations, i.e., keeping the origin at 0,0 and no rotation. You can tell Processing to start *pushing* graphics transformations to the stack through `pushMatrix()`. This *isolates* the graphics transformations you'd like to do. So you do your transformations and rotations, and then, to get back to the original context, you call `popMatrix()` to *pop* your transformations off of the stack.

A graphic of the stack is useful.

![`pushMatrix()` onto the stack.](media/stack_pushMatrix.png){ width=50% }

![`popMatrix()` off of the stack.](media/stack_popMatrix.png){ width=30% }

A few things to note:

* every `pushMatrix()` **must** have a corresponding `popMatrix()`, otherwise things will very quickly get wonky.
* You can do multiple levels of pushing and popping, if you want to isolate various aspects of your transformations. That's an advanced topic that we're not going to get into now.

So, let's say that you want to use the same `translate()` and `rotate()` techniques with the image, but this time with multiple images on the screen at once, all moving in different kinds of directions and rotations. How might we do that? The only real thing we'd need to do is to use what we just learned about the stack to push and pop the graphics matrix so as to isolate our transformations for each image/sprite.

Here's the code to make this work, with the main logic split out to a class. It's very similar to what we learned with arrays and sprites from before.

The `Sprite` class:

```java
class Sprite {
  float x = 0;
  float y = 0;
  float angle = 0;
  float rot = 0.1;
  PImage img;
  
  Sprite(String filename) {
    x = width/2;
    y = height/2;
    img = loadImage(filename);
    rot = random(-0.2, 0.2);
  }
  
  Sprite(String filename, float xarg, float yarg, float anglearg, float rotarg) {
    img = loadImage(filename);
    x = xarg;
    y = yarg;
    angle = anglearg;
    rot = rotarg;
  }
  
  void update() {
    x += random(-3, 3);
    y += random(-3, 3);
    angle += rot;
    checkBounds();
  }
  
  void display() {
    pushMatrix();
    translate(x, y);
    rotate(angle);
    imageMode(CENTER);
    image(img, 0, 0);
    popMatrix();
  }
  
  void checkBounds() {
    // Ensure that we don't exceed the bounds of the window
    if (x <= 0) {
      x = 0;
    } else if (x >= width) {
      x = width; 
    }
    
    if (y <= 0) {
      y = 0;
    } else if (y >= height) {
      y = height;
    }    
  }
}
```

Look at the `display()` function to see how we use `pushMatrix()` and `popMatrix()`. It's that simple!

And here's the code for the sketch:

```java
Sprite[] spriteArray;
int numSprites = 10;
String spriteFilename = "hand.png";

void setup() {
  size(500, 500);

  spriteArray = new Sprite[numSprites];
  
  for (int i = 0; i < numSprites; i++) {
    spriteArray[i] = new Sprite(spriteFilename);  
  }
}

void draw() {
  // Clear the screen each frame
  background(255);
  
  for (int i = 0; i < numSprites; i++) {
    spriteArray[i].update();
    spriteArray[i].display();
  }
}
```

This code is fundamentally the same as when we learned how to use arrays of objects.

![Multiple sprites moving around the screen.](media/loadImage_sprites.png)

## Working with raw pixels

If you remember from our discussion of arrays, we talked about how the window is simply a two dimensional array of pixels. But as you saw, if we tried to change the color of those pixels randomly *every frame*, it caused our sketch to run quite slowly.

We're now going to learn how to access these pixels directly, first by scratch, and then using some images as a base. This requires two new functions, `loadPixels()` and `updatePixels`, and a one dimensional array, `pixels[]`. Let's look at the code. Our `setup()` function is straightforward. Since we're working with raw pixels here, we don't need to create nor initialize any `PImage` objects:

```java
void setup() {
  // We can go back to our standard size
  size(600, 600);
  frameRate(30);
  smooth();
}
```

We now can turn to our `draw()` function. Here it is in its entirety; we'll go through specific parts of it below:

```java
void draw() {
  int loc = 0;
  background(0);
  
  // This has to be called before any pixel operations
  loadPixels();
  
  // Let's iterate over each pixel in the screen
  
  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      // So this is how we get the index into that array from
      // two dimensions, width and height
      // Think about this as moving from left to right in one row,
      // then moving to the next row down, and so on from top
      // to bottom.
      // This then allows us to index into a 1d `pixels` array
      loc = i + j * width;
      
      // `color` allows us to define, well, a color 
      pixels[loc] = color(random(0, 255), random(0, 255), random(0, 255));
    }
  }
  
  // Once we've modified the pixel array, we need to update it 
  updatePixels();
}
```

We need to have quick, easy, efficient access to our pixels on the screen, as we found that doing it manually by creating and manipulating arrays is too slow. Enter `loadPixels()`, which takes a snapshot of the current window and loads it into a one dimensional array, `pixels[]`. This 1D array is of length `width * height`. Why a 1D array? Why do it this way? It's simply more efficient to store the window in this fashion in the graphics memory. So this means that we need a way to convert our location in the window of `x` and `y` coordinates into an *index* into this 1D array. Thus we need a `loc` variable to keep track of our location. 

For us, as programmers, it's often easier to think of our position in terms of the `x` and `y` coordinates of the window. We use a nested loop (looping over each pixel from left to right, top to bottom). Think of `i` as keeping track of what column we're on, and `j` as what row. To convert that nested loop, those rows and columns, into a 1D array, we simply say, "take our current column (`i`), and add it to whichever row we're on (`j`), multiplying by how many columns we have (`width`)".

Now that we have the location into the 1D `pixels[]` array, we can use that `loc` to set its value to a random color. `pixels[]` is an array of variables of type `color`; review our discussion of that or read the Processing reference if you've forgotten how to work with type `color`[^color].

[^color]: See https://processing.org/reference/color_datatype.html.

Finally, whenever you change the `pixels[]` array, you have to call `updatePixels()` to then display the updated pixels on the screen.

![Example of randomly changing the pixel color each frame.](media/loadPixels.png)

Run this code and see how improved it is from last time!

## Manipulating the raw pixels of an image

So if we can create random images by manipulating raw pixels, can we do that with the pixels of an image? Yes, and the technique is fundamentally the same. Let's think about how we can change the saturation of an image from completely desaturated, to fully saturated, as someone moves the mouse pointer from left to right in the window. To do that, we need to talk about the concept of *color spaces*.

Up until now we've exclusively worked in the RGB color space. Here, colors are represented by various values of red, green, and blue components. But this is not the only way to represent color. If you've ever worked with print, you might have dealt with the CMYK color space, which defines colors by the relative percentages of cyan, magenta, yellow, and black. This allows one to accurately represent color in print, which functions entirely differently to how color functions on screen (light *reflecting* off material in print, versus light being *emitted* on screen).

A very different color model is HSV, for hue-saturation-value. A similar version, used in Processing, is called HSB, for hue-saturation-brightness. The motivation here is to create a color model that more accurately represents how we perceive color in the world. The details of this are complex, and the exploration of color models is a major topic in and of itself[^hsv]. For now, just realize that any color you define in RGB in Processing can also have its values converted to, and represented in, HSB space.

[^hsv]: For more information, see https://en.wikipedia.org/wiki/HSL_and_HSV.

So, if you have `color c`, represented with the usual RGB values, you can get its hue, saturation, and brightness with the following:

```java
color c = color(255, 130, 120);
int h = hue(c);
int s = saturation(c);
int b = brightness(c);
```

You can change the color mode that you're working in by calling, appropriately enough, `colorMode(HSB)`. When doing this, however, it's best to call it in a different form, that sets the upper limits for the HSB values. (Sometimes it's useful to have the values of the components go from something other than 0 to 255, but we're not going to be dealing this those cases here. Even so, it's better to be explicit with this.) So, if we want to switch the color mode to HSB, it's best to call:

```java
colorMode(HSB, 255, 255, 255);
```

This basically says, set the color mode to HSB, and set the scale to go from 0 to 255 for all of its components.

### Aside: `map()`

A useful thing to do, very often in Processing, is to map numbers from one scale to another. Perhaps you measure some values that go from 0 to 1 in floats, but you'd like them to go from 0 to 255. Now, the math to do this is not complex, but Processing provides you with a function, called `map()`, that does this for you. Let's say that you had a `float randVal` that you wanted to map to the scale just described. Here's how you could do that:

```java
float mappedRandVal;
mappedRandVal = map(randVal, 0, 1, 0, 255);
```

Given that `map()` involves division behind the scenes, it always will return a `float`. For more details, see the Processing reference.

### Aside: `mouseMoved()`

We've quite often used `mousePressed()` to do something whenever someone presses a mouse button. But maybe you want to do something *only* when the mouse pointer is moved. This allows you to do things with `mouseX` or `mouseY` at a slightly slower rate than doing it every frame in `draw()`. So, in that case, Processing provides a `mouseMoved()` function that is called *only* when the mouse is moved. You use it in similar ways to `mousePressed()`.

### Returning to our example

Let's get back to our original desire:

> How can you change the saturation of an image from 0 to fully saturated when someone moves the mouse pointer from the left to the right of the screen?

Let's use a new image downloaded from https://www.nasa.gov/mission_pages/chandra/westerlund-2.html and scaled to 1000 by 700 pixels. Here's what we need to do. Our global variables and `setup()` function are not surprising:

```java
// Image from https://www.nasa.gov/mission_pages/chandra/westerlund-2.html

PImage img;

float sat = 127;

void setup() {
  size(1000, 700);
  frameRate(30);
  img = loadImage("wd2_1000x700.jpg");
  smooth();
}
```

We then write a `mouseMoved()` function to map a new saturation value that varies based on where the mouse pointer is in the window, using `mouseX` for this purpose:

```java
// This is called each time the mouse is moved
void mouseMoved() {
  // Map the x position of the mouse to between 0 and 255
  // This allows us to take a variable, mouseX,
  // and its bounds, 0 and width, and map that to another
  // set of values, in this case a saturation value between 0 and 255
  sat = map(mouseX, 0, width, 0, 255);
}
```

The `draw()` function is where the interesting things happen.

```java
void draw() {
  // Some local variables that we're going to use
  int loc = 0;
  color c;
  float h = 0;
  float s = 0;
  float b = 0;
  
  // Load the image's pixels
  img.loadPixels();
  
  // Loop through our image
  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      // Get our location into the 1D array
      loc = i + j * width;
 
      // Get current pixel's color
      c = img.pixels[loc];
      
      // Get pixel's hue and brightness
      h = hue(c);
      b = brightness(c);
      
      // Set our colorMode
      colorMode(HSB, 255, 255, 255);
      
      // Change the pixel's color to be dependent on the changing saturation value
      img.pixels[loc] = color(h, sat, b);
    }
  }
  
  img.updatePixels();
  image(img, 0, 0);
}
```
Like our earlier example, we need to call `loadPixels()`, but this time, we call it via our `img` that we loaded in `setup()`. So, we can call `img.loadPixels()` to load the pixel array with the pixels in the image we read from file. Then, we have our nested loops to go through our rows and columns in the image. We get the 1D location into the array like before, and save the pixel's color to a local variable. We then save the hue and brightness values of the color to a local variable too. We set our color mode to HSB. And then the magic: we *reset* the pixel's color to a *new* saturation value based on the saturation that we calculated in `mouseMoved()` via the `map()` function. Once we're doing with the loop, we need to update the image's pixels, like usual, and then display the new, updated image using `image()`.

So in pretty much all respects we followed the same ideas of the previous random pixels example, except here we're doing it based on the existing pixel values in the image. Foreshadowing a bit, can you imagine how we might do something different for *live video* captured from your webcam?

## Thresholding an image

So we now know how to manipulate the raw pixels of images that we loaded from a file. Let's think about a different question:

>How could we *threshold* our image; that is, color all pixels above a certain brightness value as white, and all pixels below that value as black? And how might we change that threshold depending on where the mouse pointer is along the `x` axis?

To do this we need to create a separate, *destination* image that will hold our thresholded image. So the logic is this: 

* Load the original image
* Check each pixel in the original image for its brightness value.
* If it's above the threshold, set the pixel in the *destination* image to be white; if not, set it to be black
* Display the resulting *destination* image

So we never actually display the original image to the screen. Rather, we just use it as a source for data for our destination image. This is a common idiom when working with images in Processing, and we'll see it pretty extensively when we get to video.

Here's the code for doing thresholding.

Our global variables and `setup()`:

```java
// We need two images objects:
// * One for the source image (the downloaded NASA image)
// * One for the destination image (that will hold our thresholded image)
PImage sourceImg;
PImage destinationImg;

// Define a global threshold variable
float threshold = 127.0;

void setup() {
  size(500, 500);
  
  // Load the source image like usual
  sourceImg = loadImage("pictora_500x500.jpg");
  
  // Create a new (blank) image that's the same size as the source image (or the window) 
  // We have to tell it what color space to use for the image, in this case, RGB
  destinationImg = createImage(width, height, RGB);
}
```

We do a similar thing as before with `mouseMoved()` and the calculation of the threshold value:

```java
// This is called anytime the mouse is moved, but not pressed
void mouseMoved() {
  // Map the x position of the mouse to between 0 and 255
  // This allows us to take a variable, mouseX,
  // and its bounds, 0 and width, and map that to another
  // set of values, in this case a threshold between 0 and 255
  threshold = map(mouseX, 0, width, 0, 255);
}
```

And then the `draw()` method:

```java
void draw() {
  int loc; // location into the 1d arrays
  color c; // pixel color
  float f; // brightness value
  
  // Load the pixels for both images
  sourceImg.loadPixels();
  destinationImg.loadPixels();
    
  // Loop through the 2d pixels
  for (int i = 0; i < sourceImg.width; i++) {
    for (int j = 0; j < sourceImg.height; j++) {
      // Get our 1d location based on 2d values
      loc = i + j * sourceImg.width;
      
      // Get the color of the pixel at this `loc`
      c = sourceImg.pixels[loc];
      
      // Get the brightness of this pixel
      f = brightness(c);
      
      // If the brightness is above the threshold...
      if (f > threshold) {
        // Set the pixel color in the destination image to be white
        destinationImg.pixels[loc] = color(255, 255, 255);  
      } else {
        // Otherwise, set it to be black
        destinationImg.pixels[loc] = color(0, 0, 0);  
      }
    }
  }
  
  // Update the pixels array for the destination image
  destinationImg.updatePixels();
  
  // Finally display the new destination image
  image(destinationImg, 0, 0);
}
```

We need some local variables that we'll use in the loop below. We have to call `loadPixels()` on *both* `PImage` objects, our `sourceImg` (which we loaded from file in `setup()`), and our `destinationImg`, that will hold either black or white pixels based on the threshold value. We then loop through the image as before and get the location into the 1D array. We get the color at the given location, as well as its brightness. Then comes the thresholding logic: if the pixel's brightness is above the threshold, set the *corresponding pixel* in the *destination image* to white; if not, set it to black. Once we finish our two loops, we need to call `updatePixels()` on the `destinationImg`, since we've changed the values of the pixels. We finish by displaying the new `destinationImg` to the screen. Note here again: the `sourceImg` is never actually displayed to the screen! We just use it as, well, a *source* for the `destinationImg`.

## Programming problem: pointilist image

>How might you create a slowing accumulating *pointilist* representation of an image? That is, how might you randomly draw circles (of randomly varying sizes) at different coordinates on the screen, whose color is dependent on the color of the underlying pixel at those coordinates?

We'll get to doing this in the case of video below.

## Working with video

We've got a pretty good idea now of how to work with images, and how to create different kinds of manipulation based off of some underlying image. We've just begun to scratch the surface, however. If you're interested in this, be sure to look at Shiffman's chapter on Images, as he goes into a couple of advanced topics, such as convolution and edge detection (which we'll actually be getting to shortly with video).

But now it's time to work with *moving* images, or video. To do this, we need to extend our Processing environment a bit. And it's here that we can talk about the concept of *libraries*. 

Up until now we've been exclusively using the Processing *standard library*. This has allowed us to work with arrays, classes, images, fonts, strings, etc. But recall our early concepts of programming: representation, abstraction, and modularity. It's this last aspect that allows for the *extending* of programming environments through *libraries*.

library

: In programming, a self-contained unit of code that provides specialized functions for the programmer.

So these are bits of code that other programmers have written that allow you to do specialized tasks. Perhaps they allow you to download certain information from the internet, say news headlines. Or they allow you to work with certain kinds of sound files. In our case, we need a library that allows us access to your computer's webcam.

In Processing you can find a host of libraries that you can download if you go to "Tools => Add Tool". There, click on the "Libraries" tab in the window, and in the search box at the upper left, type "video". Click on the line that begins with "Video" and says "GStreamer-based video library for Processing", and then click on the "Install" button at the bottom right. This video library will be downloaded to your computer and installed automatically for use in Processing.

![Adding the Video library to Processing.](media/add_tool.png)

To tell Processing that we want to use this library in our code, you have to include the following at the *top* of your code before any of your global variable declarations:

```java
import processing.video.*;
```

By default, these external libraries are not loaded automatically for your sketch, as they increase the size of your sketch and sometimes can slow it down if you're not using them directly. So you have to explicitly tell Processing to load the library[^java].

[^java]: The syntax for loading libraries in Processing is *exactly* the same as in Java, which is not surprising given that Processing is based on Java. This means that, if installed properly, you can use *any* Java package in your Processing sketch. This is extremely powerful.

So now we're ready to work with live video from your webcam. We have a new object to work with, called `Capture`. Let's go through a minimal example that simply reads from the webcam[^camera] and displays the images in a window.

[^camera]: We're going to be using the built-in webcams on your laptop. But you can also use the video library to work with USB webcams or other external cameras.

Here's our preamble:

```java
// Import the video library
import processing.video.*;

// Create an object for working with the camera's video
Capture video;
```

We have to import the library, and then we declare a new object of type `Capture` that is provided by the library.

```java
void setup() {
  // Set a window size that is appropriate for capture
  size(640, 480);

  // Create a new `Capture` object
  // We first have to provide a reference to the current sketch
  // which we do through the `this` keyword
  // We then define the size of the Capture to use.
  // This needs to be a resolution that your camera supports.
  video = new Capture(this, width, height);
  
  // We then start the capture
  video.start();
}
```

Webcams usually have relatively small resolutions, so we set our window to the same size as one of the resolutions that the webcam supports. To get a list of supported resolutions, you can call `Capture.list()`, which will return an array. To nicely print this out, use `printArray(Capture.list());`, which will print the supported resolutions to your console.

The next line of code might look a bit familiar, and a bit strange. We have to initialize the `Capture` object, so we do something similar to what we've done with our own objects in the past. But this is somewhat odd: what is that `this` appearing as the first argument? In object-oriented programming, you very often need a way to refer to the code that is itself calling other code. That's somewhat confusing, I'm sure. Let's anthropomorphize the code for just a moment. If the code needed to refer to itself, it could use the words "I", or "me". Since the code isn't human, we instead use the word "this" to refer to the current sketch. The details of *why* this is needed are not something we need to worry about, just that we have to use `this` as the first argument. The documentation for the library will be clear about when you need to use `this` as an argument to a function.

Okay, the last two arguments are self-explanatory: they are the resolution (the width and height) of the webcam.

Finally, we actually have to start the webcam capture.

Next, we need to read from the camera at certain intervals. We only want to read from the camera when there is a new image. The camera will often be working at a different framerate from your Processing sketch, so it doesn't make sense to always read from the camera every frame in `draw()`. Like with the mouse, we can wait for an *event* to occur to read from the camera. This process is called *event handling* and the event itself is called `captureEvent()`:

```java
// We only read from the camera when there is a new frame ready.
// Since we can have multiple cameras attached to the computer,
// we have to pass a specific `Capture` object to this function.
void captureEvent(Capture video) {
  // Actually read from the camera
  video.read();  
}
```

Given that it's possible for multiple cameras to be attached to your computer, you have to pass a specific `Capture` object to the event handler. We then `read()` from the camera.

Now we need to figure out how to draw that frame to the screen. This is quite easy!

```java
void draw() {
  // The `Capture` object functions in many ways like a `PImage`,
  // so you can just use it in `image()` like usual.
  image(video, 0, 0);
}
```

The `Capture` object behaves, in many respects, just like a `PImage`. Thus, we can just pass our `video` variable as the first argument to `image()`. This code means that every frame of `draw()`, the most up-to-date frame from our webcam will be displayed to the screen. And we get the most up-to-date webcam frame because we wrote our `captureEvent` event handler to read from the webcam when a new camera frame is ready to be read.

If you run this code, you'll see a live webcam image displayed in your Processing window. For more information about how the library functions, see https://processing.org/reference/libraries/video/index.html.

## Thresholding live video

Let's go back to our thresholding example with the image. How might you convert that code so that it works with live webcam video? Like with that example, we're going to need a separate destination image to hold our thresholded data. And given that the `Capture` object functions much like a `PImage`, there's probably not much that's going to be different with the logic of the sketch. In fact, the only real difference is that the source image (in this case, the webcam video) will be changing at a regular rate.

Take a look at the code below for one way to approach this problem.

```java
import processing.video.*;

Capture video;

// Like the still image version, we need a destination
// image and threshold value
PImage destinationImg;
float threshold = 127;
```

We just need a `PImage` to store the `destinationImg` and a `threshold` to store that value.

```java
void setup() {
  size(640, 480);

  // This is just like the still image example
  destinationImg = createImage(width, height, RGB);

  video = new Capture(this, width, height);
  video.start();
}
```

There is little different here; we just setup the video capture and destination image objects.

```java
// Read from the camera when ready
void captureEvent(Capture video) {
  video.read();  
}
```

We read from the camera when ready.

```java
void draw() {
  // Local variables to store the brightness and color
  // of the given pixel
  float b;
  color c;
  
  // Since the `Capture` object functions like a `PImage`,
  // we can just loadPixels() like before
  video.loadPixels();
  destinationImg.loadPixels();
  
  // This loop is exactly like the still image example,
  // we just use `video` instead of a source still image.
  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      int loc = i + j * video.width;
      
      c = video.pixels[loc];
  
      b = brightness(c);
      
      if (b > threshold) {
        destinationImg.pixels[loc] = color(255, 255, 255);        
      } else {
        destinationImg.pixels[loc] = color(0, 0, 0);  
      }
      
    }
  }
  
  // And the update and display is just like before
  destinationImg.updatePixels();
  image(destinationImg, 0, 0);
}
```

Since the `Capture` object functions just like a `PImage`, we can call `loadPixels()` like before. Nothing is really different, except we're using the `video` as our source, rather than a still `sourceImg`.

And our `mouseMoved()` doesn't change:

```java
// This is exactly the same as before
void mouseMoved() {
  threshold = map(mouseX, 0, width, 0, 255);  
}
```

Run this code, and you'll be able to change the threshold of *live video* in *real time*!

Hopefully this example shows you that relatively little needs to be done to adapt, for video, code that works for images. But given that video also has a *temporal* aspect to it, we can begin to think of ways to *track* aspects of the video over time. We'll get to that in a bit. First, a few problems to try your hand at.

### Problem: saturation

>Adapt the saturation example with still images to video. Note that you might get some wonky results due to the differences in frame rates between your `draw()` function and your camera.

### Problem: pointilist video

>Adapt your pointilist still image code for video. Can you think of a way to "speed up" the appearance of points in your window?

## Aside: edge detection

It's possible to adapt the thresolding code to create an "edge detector", which does what the name suggests. I'm not going to go into the code below in any detail; read the comments for more information.

```java
// Load the video library
import processing.video.*;

// Global variables that we're going to need
Capture video;
PImage destImage;
float threshold;

void setup() {
  size(640, 480);

  // Create and start the capture
  video = new Capture(this, width, height);
  video.start();

  // Create our destination image and set a default threshold
  destImage = createImage(width, height, RGB);
  threshold = 10;
}

// Implement our event handler
void captureEvent(Capture video) {
  video.read();  
}

void draw() {
  // Local variables
  // We'll need to keep track of our current and previous lcoations
  int loc, prevLoc;
  float b;
  color c, prevC;
  
  // Load the pixels
  video.loadPixels();
  destImage.loadPixels();
  
  // Since we're comparing values between *adjacent* locations,
  // we're going to be starting from 1 rather than 0.
  for (int i = 1; i < width; i++) {
    for (int j = 0; j < height; j++) {
      // We need the index into the current location and the previous one
      loc = i + j * width;
      prevLoc = (i - 1) + j * width;
  
      // Load the two colors
      c = video.pixels[loc];
      prevC = video.pixels[prevLoc];

      // Calculate the *absolute value* of the *difference* in brightness
      // between the two colors.
      // This is a simple "edge detector". Edges often occur in images
      // when there is a sharp difference in brightness. This calculation
      // can give us an idea of when things go from light to dark, or from
      // dark to light, indicating an edge.
      b = abs(brightness(c) - brightness(prevC));
      
      // This should look similar to our thresholding example.
      if (b > threshold) {
        destImage.pixels[loc] = color(255, 255, 255);  
      } else {
        destImage.pixels[loc] = color(0, 0, 0);  
      }
    }
  }
  
  // Update the pixels in the destination image
  destImage.updatePixels();
  
  // This is a bit of a trick to get the resulting image to not 
  // be mirrored in the window
  pushMatrix();
  scale(-1, 1);
  image(destImage, -width, 0);
  popMatrix();
  
}

// This is similar to the threshold example, just with some different values
void mouseMoved() {
  threshold = map(mouseX, 0, width, 5, 50);   
}
```

## Brightness tracking in video

Here's a problem: how could you track the *brightest* part of your video over time? What would you need to know? First, you'd need to loop through every pixel in your video frame. You'd then need to compare the brightness of each pixel to the *previously known brightest pixel*. Once you found a pixel that was *brighter* than this *previously known* pixel, you'd save that *new* brightness value, as well as the `x` and `y` coordinates of it. You'd continue until you got to the end of the `pixels` array. Now you know which pixel in your video frame is the brightest!

How could you then *mark* that area of your frame? Well, it's possible to *draw* primitives (or anything else, for that matter) *on top of* your video frame. You simply have to do it in the right order. First, draw the current video frame. Then, draw something, like a circle, whose center is the coordinates of your brightest pixel.

Let's go through how this is implemented in code.

Our preamble:

```java
import processing.video.*;

Capture video;

// Global variables to save the coordinates of the
// brightest pixel, as well as its value
int brightx = 0;
int brighty = 0;
float brightvalue = 0;
```

We need some values to store the brightest pixel's coordinates and value.

The following `setup()` and `captureEvent()` should not be surprising:

```java
void setup() {
  size(640, 480);

  video = new Capture(this, width, height);
  video.start();
}

void captureEvent(Capture video) {
  video.read();  
}
```

The interesting part, of course, comes in the `draw()` method:

```java
void draw() {
  // Local variables for brightness and color
  float b;
  color c;
  
  // Load the pixels
  video.loadPixels();
  
  // Give a starting value for `brightvalue` based on
  // the first pixel in the array
  brightvalue = brightness(video.pixels[0]);
  
  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      // Get the location
      int loc = i + j * video.width;
      
      // Read the values
      c = video.pixels[loc];
      b = brightness(c);
      
      // If the current pixel's value is brighter than 
      // the previous brightest value, update the values
      if (b > brightvalue) {
        brightx = i;
        brighty = j;
        brightvalue = b;
      }
    }
  }
  
  // Draw the video frame to the screen
  image(video, 0, 0);
  
  // Then draw a red circle at the location of the
  // brightest pixel
  fill(255, 0, 0);
  noStroke();
  ellipse(brightx, brighty, 50, 50);
}
```

Once we've loaded the pixels, we set the initial "brightest pixel" to be the first pixel in the array. We then loop through the array, getting the brightness value for each pixel. We then compare *that* value with the previously known brightest value. If it's larger, then we save the *new* value and the coordinates of the pixel.

Once we're out of the loop, we first draw the video frame to the window. We can then draw a red circle at the location of the brightest pixel. This circle appears *on top of* the video image. Once there's a new video frame, `video` gets updated, and we're on the search again for the brightest pixel. And so on for the duration of the sketch.

Congratulations, you've just built your first video tracker! You now have begun to implement basic *computer vision* algorithms.

### Aside: auto-exposure and background

Most webcams are designed to automatically adjust the "exposure" of the image, depending on ambient lighting conditions. So, for example, the contrast will be heightened, or the brightness adjusted. As you can image, this wrecks havoc on any code that is trying to track brightness! You might need to turn off auto-exposure in your system settings to make the previous example, and later examples, work well.

Even illumination is extremely important. For best results, ensure that you have a constant light source, that doesn't cast a lot of shadows, and that provides the same intensity of light over your scene.

As well, the techniques that we're learning here are useful, but they work the best when you have an "uncomplicated" background. So if you're not seeing the effects as expected, try to ensure that your background is as uncluttered as possible (i.e., a blank wall, a sheet, etc.). More advanced techniques--beyond the scope of this class--can deal with these issues, but computer vision is still an inexact science.

## Color tracking

Now that we're able to track the brightness, how might we track a particular color? We'd need some way to determine whether or not a given pixel's color was *close to* or *far away* from another target color. We can think of color as existing in a three dimensional space of R, G, and B. And we can think of *two* colors existing as two separate points in that space. We can then consider the *Euclidian* distance between the two points. Two colors that are closer to each other in that space are going to be more similar, and are going to have a smaller distance between them.

In general, you can consider the Euclidian distance between two points $(x_1, y_1)$ and $(x_2, y_2)$:

$\sqrt((x_1 - x_2)^2 + (y_1 - y_2)^2)$

In three dimensions (for $(x_1, y_1, z_1)$ and $(x_2, y_2, z_2)$), the equation is:

$\sqrt((x_1 - x_2)^2 + (y_1 - y_2)^2 + (z_1 - z_2)^2)$

Thus, we can think of distance between two different colors as distances between two different points in an "RGB" space.

Luckily Processing provides a function to calculate distance, appropriately enough called `dist()`. To use it in the previous example, you do the following:

```java
float d = dist(x1, y1, z1, x2, y2, z2);
```

You list the first set of three coordinates, and then the second set of three coordinates. A similar formulation of `dist()` exists for two dimensions as well. For more information see https://processing.org/reference/dist_.html.

So now we can get to our problem. We need a way to select a target color. The best way to do that would be through a `mousePressed()` function that takes the color of the pixel immediately under the mouse pointer. Then, in our `draw()` method we need to calculate the distance between the current pixel and our target pixel. Similar to our brightness example, we need to determine whether the new distance is *smaller* than the *previous smallest distance*. If it is, then we've found a pixel that's *closer* in color to our target color. Once we've gone through all of our pixels, we then have the pixel that's *closest* in color to our target pixel color. 

But there's one last rub. We only want to draw a circle on the screen when the smallest distance that we just calculated is *less than* some target threshold. This allows us to not simply draw something to the screen based on spurious colors. This threshold often has to be determined by trial and error depending on lighting conditions.

So, let's see the actual code:

```java
import processing.video.*;

Capture video;

// Keep track of current and tracked colors
color trackedColor;
color currentColor;

// Variables to track the best color, as well as current distance
float d = 0;
float bestd = 500;
int bestx = 0;
int besty = 0;
```

We've got some global variables that will keep track of the colors that we're tracking, as well as the best pixel.

Our `setup()` function isn't surprising, but we do need to setup a default `trackedColor` before the user has selected anything:

```java
void setup() {
  size(640, 480);

  // Setup our video
  video = new Capture(this, width, height);
  video.start();
  
  // Define a default trackedColor
  trackedColor = color(255, 0, 0);
}

void captureEvent(Capture video) {
  video.read();  
}
```

To actually capture the color, we need to write a special `mousePressed()` function:

```java
void mousePressed() {
  // Get the current location into the array based on the mouse coordiantes
  int loc = mouseX + mouseY * width;
  
  // Load the pixels
  video.loadPixels();
  
  // Set the `trackedColor` to the pixel underneath the mouse pointer
  trackedColor = video.pixels[loc];
}
```

Here we get the 1D index into the `pixels` array based on the mouse coordinates. We then get the `trackedColor` based on that. Since we didn't modify the `pixels` array at all, we don't need to call `updatePixels()`.

The magic, of course, happens in the `draw()` method:

```java
void draw() {
  // Setup some local variables
  int loc;
  float r1, g1, b1, r2, g2, b2;
  
  // Set an arbitrarily large best distance
  bestd = 500;
  
  // Load the pixels
  video.loadPixels();
  
  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      // Get the current color
      loc = i + j * width;
      currentColor = video.pixels[loc];
      
      // Start saving the RGB values of the current color
      r1 = red(currentColor);
      g1 = green(currentColor);
      b1 = blue(currentColor);
      
      // And the tracked color
      r2 = red(trackedColor);
      g2 = green(trackedColor);
      b2 = blue(trackedColor);

      // Compute the Euclidian distance between the two RGB values
      d = dist(r1, g1, b1, r2, g2, b2);
      
      // If this new distance is less than the previous best distance,
      // save it
      if (d < bestd) {
        bestd = d;
        
        bestx = i;
        besty = j;
      }
    }
  }
  
  // Display the video frame
  image(video, 0, 0);
  
  // If the best distance is smaller than some arbitrary value,
  // draw a red circle on top of the best tracked pixel
  if (bestd < 10) {
    fill(255, 0, 0);
    noStroke();
    ellipse(bestx, besty, 50, 50);
  }
}
```

Much of this should be familiar by now. We setup local variables for our computation, including an arbitrarily large best distance that will almost certainly be bested by any pixel in our image. We load the pixels and start our nested loops. We then save the RGB values for both our `currentPixel` and our `trackedPixel`. We compute the distance using `dist()`. If this new distance is less than the previous `bestd`, then we save it and the coordinate. After we get out of the loop, we draw the image to the screen. Finally, if the `bestd` is smaller than some arbitrary value, we draw a red circle on top of the best coordinate. This helps us only track colors that are relatively close to the `targetColor`.

This is somewhat of a brittle solution, but is the best we can do based on your knowledge of computer vision. But still, think about what kind of things you could do with this! Could you think of how you might *tint* the video frame based on what quadrant your `targetColor` is in? What else could you do?

## Working with movie files

You can also read in movie files and do the same kind of processing on these files as you do with live video. You of course need to add the movie file to your sketch's `data` folder using the same procedure you used earlier to add image files. 

Now, you can certainly use movies that you've recorded from your phone, for example. However, those videos are going to be at a rather large resolution that will likely be hard for Processing to play back efficiently. It'll be best for you to rescale those videos to something smaller before trying to play them in Processing. How you do that will be up to you and will depend on what kinds of video editing software you're familiar with. 

You work with movie files almost exactly as you would with capturing live video; just the object is different. Here's a complete example of reading in a movie file. Read the comments for the details, or look at the reference https://processing.org/reference/libraries/video/Movie.html:

```java
// You use the same library for reading movie files as you do
// for reading from cameras
import processing.video.*;

// Instead of `Capture`, we instantiate an object of type `Movie`
Movie movie;

void setup() {
  // This is the size of the movie file
  // You'll probably need to crop and resize a movie file using
  // other programs
  size(500, 500);
  
  // Like `Capture`, you need to use the `this` keyword, along with the
  // name of the movie file you want to load.
  // See the Processing reference for what kinds of files `Movie`
  // can read.
  movie = new Movie(this, "waving_500x500.mov");
  
  // You need to tell the movie object to start playing.
  // `play()` will cause the movie to play through once,
  // while `loop()` will loop it forever.
  movie.play();
  // movie.loop();
}

// Instead of `captureEvent` you have `movieEvent`, which is called
// every time there is a new frame to be read
void movieEvent(Movie m) {
  m.read(); 
}

void draw() {
  // You display each frame of the movie like you do with reading
  // frames from a camera
  image(movie, 0, 0);  
}
```

The library offers a few different ways to manipulate playback of the movie files. Some useful options to consider would be `jump()` or `speed()`; for more details, see the references.

Here's another programming exercise:

>Modify your *pointilist video* example to work with a movie file.

See the solution for one "gotcha" to be aware of.


## Playing back sound

So we know how to read images, manipulate them; take live video from the webcam, and manipulate that; and read in movie files, and manipulate that. Last thing for us to learn how to do is to load in sound. We're going to jump to a more advanced way to use sound files, rather than going through things step-by-step like before. I'm doing this to get you to *generalize* your knowledge about what you already know about programming to a new example.

Our goal here: load a series of sound files (stored in an array), and play back a different one based on which quadrant of the screen the mouse pointer is in. Color the quadrant based on the location of the mouse pointer as well.

Now, the main sound library that people have used in Processing in the past seems to have some problems these days, so we're going to use a *third-party library*. This actually works out well for us, as it allows you to learn how to make things work with libraries designed and constructed by other people. It means having to learn the specific *API* of the library (recall the definition of this from our time learning classes). And sometimes it means delving into complexity that you'd rather not get into, but sometimes you have to in order to make things work.

Recall that we can download and install external libraries using the "Tools->Add Tool" menu item, and searching within the "Libraries" tab. Here, search for "sound" and see what comes up. Now, it's hard to know which library to choose from this list. But the best way to figure it out is to search around on the web, looking for what other people are using, what people say about the library, and so on. Really, it's no more scientific than that. In my searching I saw a lot of people talking about "Minim" and saying that it works well, so we're going to go with that. Click on "Minim" and install it.

![Adding "Minim".](media/add_sound.png)

Now we have to figure out how to use it. Sometimes you can find information about the library online. In this case there's a "Quickstart Guide" available here: http://code.compartmental.net/tools/minim/quickstart/ .

But another way you can figure out how to use a newly installed library is to go to "File->Examples". Go down to "Contributed Libraries", and you should see a folder for "Minim". In there is a set of examples that you can look at to help you figure out how to use the library. Minim provides a fair number of examples that are well-commented. That's how I was able to learn how to use the library.

So read through the code below. There are some things in it that even I don't fully understand. For example, on line 28 we have to *initialize* the class. Why? I don't know; that's just what the class requires according to its documentation. As a programmer I can *imagine* why that might need to happen, but I don't know *for sure*. This is something that is quite common in programming when you use external libraries. Since these classes are very often black boxes, you have to just take the external interface *on faith* and accept that it works the way it does. That means, sometimes, changing things about how you code to match the interface. That can be frustrating, but is sometimes necessary to interface *your code* with the code of the library.

Now to the code. I've already created, and moved to the `data` directory, the sound files that will be loaded. Much of this code for playing back sound is relatively straightforward, at least compared to the video manipulation we were doing before. The difficulty, actually, is in ensuring that we draw the rectangles properly, and only play back the sound once per quadrant.

```java
// Import the library for reading sound files
import ddf.minim.*;

// Color for quadrant backgrounds
color quadColor;

// Keeping track of which quad we're in
// 0 -> top left
// 1 -> top right
// 2 -> bottom right
// 3 -> bottom left
int quad = 0;

// Have we started moving yet?
boolean moved = false;

// Filenames for each of our sounds
String[] soundFilenames = {"bell.wav", "blonk.wav", "key.wav",
                      "metallic_tap.wav", "plink.wav", "spring.wav",
                      "tink_tonk.wav", "tomp.wav"};
Minim minim;
AudioSample[] samples;

void setup() {
  size(500, 500);
  
  // Initialize the library
  minim = new Minim(this);
  
  // Create a new array of `AudioSample`s
  samples = new AudioSample[soundFilenames.length];
  
  // Load each sample
  for (int i = 0; i < soundFilenames.length; i++) {
    println(soundFilenames[i]);
    // We give the filename to the sample as the first argument,
    // and then the "buffer size". You don't need to worry too
    // much about this, just set it to 512.
    samples[i] = minim.loadSample(soundFilenames[i], 512);  
  }
  
}

void draw() {
  // Clear the background
  background(0);
  
  // Only change the color once we've moved the mouse.
  if (moved) {
    // Set the colors
    fill(quadColor);
    stroke(quadColor);
    rectMode(CORNER);
    
    // Define the rectangles for the various quadrants
    if (quad == 0) {
      rect(0, 0, width/2, height/2);  
    } else if (quad == 1) {
      rect(width/2, 0, width/2, height/2);  
    } else if (quad == 2) {
      rect(width/2, height/2, width/2, height/2);  
    } else if (quad == 3) {
      rect(0, height/2, width/2, height/2);  
    }
  }
}

void mouseMoved() {
  // Which quad are we currently in?
  int currentQuad = 0;
  
  // We have moved, indeed
  moved = true;
  
  // Set the colors and `currentQuad` based on which quadrant we're in
  if ((mouseX < (width/2)) && (mouseY < (height/2))) {
    // top left
    quadColor = color(255, 0 , 0);
    currentQuad = 0;
  } else if ((mouseX >= (width/2)) && (mouseY < (height/2))) {
    // top right 
    quadColor = color(0, 255, 0);
    currentQuad = 1;
  } else if ((mouseX >= (width/2)) && (mouseY >= (height/2))) {
    // bottom right 
    quadColor = color(0, 0, 255);
    currentQuad = 2;  
  } else if ((mouseX < (width/2)) && (mouseY >= (height/2))) {
    // bottom left 
    quadColor = color(255, 255, 255);
    currentQuad = 3;  
  } else {
    currentQuad = 0;  
  }
  
  // Only when we move *out* of one quadrant and into another do we
  // play back ("trigger") the sound
  if (currentQuad != quad) {
    quad = currentQuad;
    samples[quad].trigger();   
  }
  
}
```

## Aside: saving frames

At this point in your Processing career, you might want to be able to save some sort of recording of your sketch in action. This can be really useful when creating a portfolio of your work to share with others. While you can use a screen capture program for this purpose, such programs will often degrade the quality of your sketch.

The best thing to do is to save each frame of your sketch to an image file, and then use some other program to combine all of those frames into a movie. Luckily, Processing offers the `saveFrame()` function that will, as the name suggests, save each frame as an image file in the directory of your sketch. It's best to put this function as the *last line* of your `draw()` method, as it will then save everything drawn to the window in the current frame.

By default, `saveFrame()` will save the frames as `.tif`, which will write to your computer faster, but will be larger in size. You can also pass a *formatting string* to `saveFrame()` to tell it to save the images as `.png`, which will be smaller in size, but will take longer to write to disk. So, for example, you could write:

```java
saveFrame("movie_points-#####.png");
```

This would save each frame as a `.png` file in the sketch directory, and it would allow saving upwards of 99999 frames (as each use of the hash/pound sign demarcates a possible digit in the filename). The part "movie_points-" is arbitrary; you can name it whatever you want. The extension (`.png`) is important, as it tells Processing to save the image file in PNG format.

In either case, it's probably best to set your framerate to 30 or so (`frameRate(30);`) so as to both cut down on the number of frames being saved to disk, as well as how much writing to disk your sketch has to do.

Now that you have the frames saved, you need to turn it into a movie. You can do it one of two ways. The easiest way is to go to "Tools -> Movie Maker". There you can point the "Movie Maker" tool to your directory with the individual frames. You then set the size of the movie you'd like (set it to the same size as your window for your sketch) and the frame rate (set it to the frame rate of your sketch). Click on "Create movie..." and tell it where to save the resulting movie.

Or, you can use a program like Adobe Premiere, which can take a series of images (which Premiere calls an "image sequence") and easily turn it into something that is editable. There are a number of tutorials on YouTube for doing this, so check there for details.

Of course, once you've made your movie, be sure to remove or comment out the `saveFrame()` call before submitting or sharing your code!

## Assignment

We've finally come to the assignment for this week! This will be the last Processing assignment for the course, so I'm going to leave part of it more open-ended than I have in the past.

This assignment will be due next Thursday at 11:59PM.

1. Modify the color tracking code to play a different sound based on *which quadrant* the tracked color is in. You will need to create your own short sound files to play.
2. Create a unique "software mirror" (in the language of Daniel Shiffman) for live video. We've made one software mirror so far: our "pointilist" manipulation of an image or video. Think of something else to create. Perhaps you scribble lines on a screen based on the underlying color. Or cause particles to be created wherever a tracked color is found. Or you draw shapes based on the brightness of an underlying pixel. Or something else! What you do is up to you. But I'd like to see the full range of your creativity on this. Wow me with what you've learned so far. Because you should be proud of how far you've come in so short of a time!
