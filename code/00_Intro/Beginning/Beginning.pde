
// This code is run when your sketch is first opened
void setup() {
  size(400, 400);
  background(0, 0, 0);
}

// This code is run on every frame
void draw() {
  stroke(255, 0, 0);
  line(pmouseX, pmouseY, mouseX, mouseY);
}

// This code is run every time someone 
// presses a mouse button
void mousePressed() {
  stroke(0);
  fill(175);
  // rectMode(CENTER);
  // rect(mosueX, mouseY, 16, 16);
  ellipse(mouseX, mouseY, 16, 16);
}

// This code is run every time someone
// presses a key
void keyPressed() {
  background(255, 255, 255); 
}
