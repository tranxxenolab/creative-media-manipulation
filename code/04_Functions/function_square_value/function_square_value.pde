void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
}

void draw() {
  int squareX = 0;
  int squareY = 0;
  
  squareX = square_value(mouseX);
  squareY = square_value(mouseY);
  
  if ((frameCount % 30) == 0) {
    println("Squared mouse coordinates: ", squareX, " ", squareY);  
  }

}

// This function returns a value, an int
int square_value(int x) {
  return (x * x);  
}