void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
}

void draw() {
  background(0);
  translate(width/2, height/2);
  
  for (int i = 0; i < 20; i += 1) {
    stroke(255);
    fill(255);
    ellipse(x(frameCount + i), y(frameCount + i), 15, 15);  
  }

}

float x(float t) {
  return (154 * sin(t * 0.09) - 100 * (cos(t * 0.02)));  
}

float y(float t) {
  return (50 * sin(t * 0.03) + 200 * (cos(t * 0.07)));  
}