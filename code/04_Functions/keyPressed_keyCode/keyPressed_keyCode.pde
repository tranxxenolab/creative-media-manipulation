void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
}

void draw() {
  
}

void keyPressed() {
  // Check whether coded key
  if (key == CODED) {
    // Check keyCode
    if (keyCode == UP) {
      println("UP pressed");  
    } else if (keyCode == DOWN) {
      println("DOWN pressed");  
    }
  } else {
    if (key == 'e') {
      println("'e' key pressed");  
    } else if (key == 102) { // can also check ASCII code
      println("'f' key pressed");  
    }
  }
}