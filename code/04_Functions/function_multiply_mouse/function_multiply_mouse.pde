void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
}

void draw() {
  int x = 0;
  int y = 0;
  
  x = mouseX;
  y = mouseY;
  
  // Call our function every second
  if ((frameCount % 30) == 0) {
    multiply_mouse(x, y);
    println("The value of x and y is: ", x, " ", y);
  }
}

// This is a function that takes x and y coordinate values as input
// We have to declare the type of the variables that we're going to use
// as input
void multiply_mouse(int x, int y) {
  // This might look wonky, but it's perfectly
  // acceptable Processing code
  // We're assigning to x, which was already
  // defined as an argument,
  // the result of multiplying x by x.
  // When you're reading assignment expressions like this,
  // read them from right to left if they look confusing.
  // And think of "=" as "assign to"
  x = x * x;
  y = y * y;
  println("Mouse coordinates squared: ", x, " ", y);
}