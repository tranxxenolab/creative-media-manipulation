void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
}

void draw() {
  int squareX = 0;
  int squareY = 0;
  
  squareX = square_value(mouseX);
  squareY = square_value(mouseY);
  
  if ((frameCount % 30) == 0) {
    println("Squared mouse coordinates: ", squareX, " ", squareY);  
  }

  if ((frameCount % 60) == 0) {
    float randomValue = random(100, 200);
    // Note how I'm calling the function here within the println statement
    // And it is exactly the same name as above, but only called with a float
    println("Squared float value: ", square_value(randomValue));
  }
}

// This function returns a value, an int
int square_value(int x) {
  return (x * x);  
}

// This function returns a value, a float
// It's an overloaded version of the previous function
float square_value(float x) {
  return (x * x);  
}