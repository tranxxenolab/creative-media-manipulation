void setup() {
   size(600, 600);
   background(0);
   frameRate(30);
}

void hello_world() {
  println("Hello, world: ", frameCount); 
}

void draw() {
  if ((frameCount % 30) == 0) {
    hello_world();  
  }
}
