// Define the coordinates of the sprite
float spriteX = 0;
float spriteY = 0;

// Decide on its speed
float speed = 0.09;

void setup() {
  size(600, 600);
  background(0);
  //frameRate(30);
  
  // Set the beginning location based on the size of the screen
  spriteX = 0;
  spriteY = height/2;
}

void draw() {
  background(0);
  
  // Compute our easing values
  float xPos = xeasing(mouseX, spriteX, speed);
  float yPos = yeasing(mouseY, spriteY, speed);
 
  // Draw the sprite
  draw_sprite(xPos, yPos);
  
  // Print out the distance every 10 frames
  if((frameCount % 10) == 0) {
    float d= distance(spriteX, spriteY, mouseX, mouseY);
    println("Distance: ", d);
  }
  
  //saveFrame("sprite_follow-######.png");
}

// Calculate the x easing value
float xeasing(float mx, float sx, float e) {
  float dx = mx - sx;
  return (sx + e*dx);  
}

// Calculate the y easing value
float yeasing(float my, float sy, float e) {
  float dy = my - sy;
  return (sy + e*dy);
}

// Calculate the distance based off of the equation
// given in the assignment
float distance(float x1, float y1, float x2, float y2) {
  float dx = x1 - x2;
  float dy = y1 - y2;
  float d = sqrt(dx*dx + dy*dy);
  return d;
}


void draw_sprite(float x, float y) {
  spriteX = x;
  spriteY = y;
  fill(255);
  stroke(255);
  ellipse(x, y, 80, 80);
  
  fill(0, 0, 255);
  stroke(0, 0, 255);
  ellipse(x-20, y-20, 20, 20);
  
  fill(0, 0, 255);
  stroke(0, 0, 255);
  ellipse(x+20, y-20, 20, 20);
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      // Increase the speed
      speed += 0.01;
      println("Speed: ", speed);
    } else if (keyCode == DOWN) {
      // Decrease the speed
      speed -= 0.01;
      println("Speed: ", speed);
    }
  }
}
