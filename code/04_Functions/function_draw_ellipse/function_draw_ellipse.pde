// This sketch moves an ellipse randomly around the window
// Global variables
int ellipseX = 0;
int ellipseY = 0;
int speed = 1;

void setup() {
  size(600, 600);
  // We initialize the starting point of the ellipse
  // after we've set the size, and we set it to the
  // middle of the window, ensuring that the value
  // is an int
  ellipseX = int(width/2);
  ellipseY = int(height/2);
  background(0);
  frameRate(30);
}

void draw() {
  // Clear the background each frame
  background(0);
  
  // Update the speed every second
  if ((frameCount % 30) == 0) {
    update_speed();
  }
  
  // Update the X or Y position approximately
  // half of the time, respectively
  if (random(0, 1) >= 0.5) {
    update_positionX();  
  } else {
    update_positionY();
  }
  
  // Actually draw the ellipse
  draw_ellipse(20, 20);
}

// Choose a random speed, either positive or negative
// Ensure that it is an int
void update_speed() {
  speed = int(random(-7, 7));
  
  // This computes a "moving average" of the speed
  //speed = int(((random(-7, 7)) + float(speed))/2);
}

// Update the X coordinate of our ellipse
void update_positionX() {
  ellipseX = ellipseX + speed;
  
  // Ensure that the ellipse stays within the bounds
  // of the window
  if (ellipseX < 0) {
    ellipseX = 0;
  } else if (ellipseX > width) {
    ellipseX = width;
  }
}

// Do the same for the Y position

void update_positionY() {
  ellipseY = ellipseY + speed;
  
  if (ellipseY < 0) {
    ellipseY = 0;
  } else if (ellipseY > height) {
    ellipseY = height;
  }
}

// Draw our ellipse
// Note that here we can control the fill and stroke
// without worrying about what it is elsewhere
// in our code
void draw_ellipse(int w, int h) {
  fill(255);
  stroke(255);
  ellipse(ellipseX, ellipseY, w, h);
}
