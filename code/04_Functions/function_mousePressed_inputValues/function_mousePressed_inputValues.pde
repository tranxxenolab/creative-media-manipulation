void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
}

void draw() {
  int x = 0;
  int y = 0;
  
  x = mouseX;
  y = mouseY;
  
  // Call our function every second
  if ((frameCount % 30) == 0) {
    multiply_mouse(x, y);
    println("The value of x and y is: ", x, " ", y);
  }
}

// This is a function that takes x and y coordinate values as input
// We have to declare the type of the variables that we're going to use
// as input
void multiply_mouse(int x, int y) {
  x = x * x;
  y = y * y;
  println("Mouse coordiantes squared: ", x, " ", y);
}