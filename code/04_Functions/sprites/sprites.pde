// Keep track of whether or not the screen needs to be updated
boolean dirty = true;

// Set the default number of sprites
int numSprites = 10;

void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
}

void draw() {
  // if the screen needs to be redrawn...
  if (dirty) {
    // We clear the screen before we draw the sprites
    background(0);
    // Our loop goes up to numSprites
    for (int i = 0; i < numSprites; i++) {
      // And we draw the sprite at a random location
      draw_sprite(int(random(0, width)), int(random(0, height)));  
    }
    // And then we set the dirty flag to be zero
    dirty = false;
  }
  
  //saveFrame("sprites-######.png");
}

// Here's our function for drawing sprites
void draw_sprite(int x, int y) {
  fill(255);
  stroke(255);
  ellipse(x, y, 80, 80);
  
  fill(0, 0, 255);
  stroke(0, 0, 255);
  ellipse(x-20, y-20, 20, 20);
  
  fill(0, 0, 255);
  stroke(0, 0, 255);
  ellipse(x+20, y-20, 20, 20);
}

void keyPressed() {
  // Check whether coded key
  if (key == CODED) {
    // Check keyCode
    if (keyCode == UP) {
      println("UP pressed");
      // If it's up, add one to numSprites
      numSprites += 1;
      println("numSprites: ", numSprites);
      // Mark the screen as dirty, so it's drawn
      dirty = true;
    } else if (keyCode == DOWN) {
      println("DOWN pressed");
      // Subtract one from the number of sprites
      numSprites -= 1;
      
      // Ensure that it doesn't go below 0
      if (numSprites < 0) {
        numSprites = 0;  
      }
      println("numSprites: ", numSprites);
      
      // Mark the screen to be redrawn
      dirty = true;
    }
  } else {
  }
}
