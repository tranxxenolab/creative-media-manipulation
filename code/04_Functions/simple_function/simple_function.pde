void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
}

void draw() {
  if ((frameCount % 30) == 0) {
    hello_world();
  }
}

// This is our function that we're defining
void hello_world() {
  println("Hello, world: ", frameCount);
}