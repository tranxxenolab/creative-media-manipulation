String hello;
String goodbye;
String combo;

String info;

PFont sourceSansPro;

void setup() {
  size(600, 600);
  frameRate(30);
  smooth();
  
  hello = "Hello, world!";
  goodbye = "Hello, world!";
  
  sourceSansPro = loadFont("SourceSansPro-Regular-48.vlw");
  textFont(sourceSansPro, 32);
  
  combo = hello + "\n\n" + goodbye;
}

void draw() {
  background(0);
  
  info = "Current\nmouse\npointer:\n" + mouseX + " " + mouseY;
  text(info, 10, 400);
}
