// Declare our arrays
// We're using ints to cut down on memory usage
int[][] pixelR;
int[][] pixelG;
int[][] pixelB;

// Set some values for the size of the screen
int w = 300;
int h = 300;

void setup() {
  // Note the smaller size!
  size(300, 300);
  frameRate(30);
  smooth();

  // And here we do our further array declaration
  pixelR = new int[w][h];
  pixelG = new int[w][h];
  pixelB = new int[w][h];
  
  // We're going to loop through the arrays and set
  // a different color for each pixel
  for (int i = 0; i < w; i++) {
    for (int j = 0; j < h; j++) {
      pixelR[i][j] = int(random(0, 255));
      pixelG[i][j] = int(random(0, 255));
      pixelB[i][j] = int(random(0, 255));
    }
  }
}

void draw() {
  background(0);
  
  // Let's iterate over each pixel in the screen
  for (int i = 0; i < w; i++) {
    for (int j = 0; j < w; j++) {
      // Set the stroke color
      stroke(pixelR[i][j], pixelG[i][j], pixelB[i][j]);
      // This is a primitive we haven't used much
      // but it's perfect for drawing pixels
      point(i, j); 
      
      // We could try to update things every frame
      // This will run rather slowly!
      /*
      pixelR[i][j] = int(random(0, 255));
      pixelG[i][j] = int(random(0, 255));
      pixelB[i][j] = int(random(0, 255));
      */
      
    }
  }
}