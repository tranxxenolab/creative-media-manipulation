// Setup some `String`s
String hello;
String goodbye;
String combo;

void setup() {
  size(600, 600);
  frameRate(30);
  smooth();

  // Even though `String` is a class, we don't have to
  // use the `new` syntax like we've done before
  hello = "Hello, world!";
  goodbye = "Goodbye, interlocutor!";
  
  // We can "add" strings together like this
  // But you can't subtract, multiply, or divide...
  combo = hello + " " + goodbye;
}

void draw() {
  background(0);

  // Output the strings to the console
  if ((frameCount % 30) == 0) {
    println(hello);
  } else if ((frameCount % 80) == 0) {
    println(goodbye);
  } else if ((frameCount % 110) == 0) {
    println(combo);
    
    if (!hello.equals(goodbye)) {
      // We use the forward slash to "escale" the double quote character
      println("\"Hello, world\" is not equal to \"Goodbye, interlocutor\"");  
    }
    
    println("We can also make things lower case");
    println(combo.toLowerCase());
  }
}