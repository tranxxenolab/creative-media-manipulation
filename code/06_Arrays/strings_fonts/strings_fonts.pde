// Setup a `String`
String info;

// Declare our font
PFont sourceSansPro;

void setup() {
  size(600, 600);
  frameRate(30);
  smooth();
  
  // Load the font
  // You have to create this font through "Tools -> Create Font..."
  sourceSansPro = loadFont("SourceSansPro-Regular-48.vlw");
  
  // Set the font
  textFont(sourceSansPro, 32);
}

void draw() {
  background(0);

  info = "Current mouse pointer: " + mouseX + " " + mouseY;
  text(info, 10, 560);
}