// Declare our arrays
float[] fillR;
float[] fillG;
float[] fillB;
float[] ellipsesX;
float[] ellipsesY;
float[] ellipsesW;
float[] ellipsesH;

int numEllipses = 10;

void setup() {
  size(600, 600);
  frameRate(30);
  smooth();

  // And here we do our further array declaration
  fillR = new float[numEllipses];
  fillG = new float[numEllipses];
  fillB = new float[numEllipses];
  ellipsesX = new float[numEllipses];
  ellipsesY = new float[numEllipses];
  ellipsesW = new float[numEllipses];
  ellipsesH = new float[numEllipses];
  
  // Here's the loop to iterate over the array
  for (int i = 0; i < numEllipses; i++) {
    // Initialize colors to different random values
    fillR[i] = random(0, 255);
    fillG[i] = random(0, 255);
    fillB[i] = random(0, 255);
    
    // Initialize position to random values
    ellipsesX[i] = random(0, width);
    ellipsesY[i] = random(0, height);
    
    // Initialize width and height to be random values
    ellipsesW[i] = random(10, 40);
    ellipsesH[i] = random(10, 40);
  }

}

void draw() {
  background(0);
  
  // Iterate over our ellipses
  for (int i = 0; i < numEllipses; i++) {
    // Set our fill color
    fill(fillR[i], fillG[i], fillB[i]);
 
    // Set the stroke to be the same as the fill
    stroke(fillR[i], fillG[i], fillB[i]);
    
    // Draw the ellipses to the screen
    ellipse(ellipsesX[i], ellipsesY[i], ellipsesW[i], ellipsesH[i]);
  }
  
  // Output the array to the console
  if ((frameCount % 30) == 0) {
    printArray(fillR);  
  }
}