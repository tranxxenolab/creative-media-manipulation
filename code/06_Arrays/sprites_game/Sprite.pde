class Sprite {
  float x = 0;
  float y = 0;
  float xoffset = 0;
  float yoffset = 0;
  int spriteFrame = 0;
  int spriteFrameDuration = 10;
  int spriteW = 80;
  int spriteH = 80;
  boolean hit = false;
  float spriteFadeIn = 0.0;
  float fadeInterval = 0.0;
  float alpha = 0.0;
  
  Sprite() {
    x = width/2;
    y = height/2;
  }
  
  Sprite(float sx, float sy) {
    x = sx;
    y = sy;
  }
  
  void draw_sprite(float x, float y) {
    
    if ((spriteFrame < spriteFrameDuration)) {
      draw_sprite_frame001(x, y);  
    }
    
    if ((spriteFrame >= spriteFrameDuration) && (spriteFrame <= (2 * spriteFrameDuration))) {
      draw_sprite_frame002(x, y);  
    }
    
    if ((spriteFrame > 2*spriteFrameDuration) && (spriteFrame <= (3 * spriteFrameDuration))) {
      draw_sprite_frame003(x, y);  
    }
    
    if (spriteFrame >= (3 * spriteFrameDuration)) {
      spriteFrame = 0;
    } else {
      spriteFrame += 1;
    }
  }
  
  // Here's our function for drawing sprites
  void draw_sprite_frame001(float x, float y) {

    if (spriteFadeIn > 0) {
      alpha += fadeInterval;
      if (alpha >= 255) {
        alpha = 255;  
      }
    } else {
      alpha = 255;  
    }
    
    fill(255, 255, 255, alpha);
    stroke(255, 255, 255, alpha);
    ellipse(x, y, spriteW, spriteH);
    
    fill(0, 0, 255, alpha);
    stroke(0, 0, 255, alpha);
    ellipse(x-20, y-20, 20, 20);
    
    fill(0, 0, 255, alpha);
    stroke(0, 0, 255, alpha);
    ellipse(x+20, y-20, 20, 20);
  }
  
  // Here's our function for drawing sprites
  void draw_sprite_frame002(float x, float y) {
    
    if (spriteFadeIn > 0) {
      alpha += fadeInterval;
      if (alpha >= 255) {
        alpha = 255;  
      }
    } else {
      alpha = 255;  
    }
    
    fill(255, 255, 255, alpha);
    stroke(255, 255, 255, alpha);
    ellipse(x, y, spriteW, spriteH);
    
    fill(0, 255, 0, alpha);
    stroke(0, 255, 0, alpha);
    ellipse(x-20, y-20, 20, 20);
    
    fill(0, 255, 0, alpha);
    stroke(0, 255, 0, alpha);
    ellipse(x+20, y-20, 20, 20);
  }
  
  // Here's our function for drawing sprites
  void draw_sprite_frame003(float x, float y) {
    
    if (spriteFadeIn > 0) {
      alpha += fadeInterval;
      if (alpha >= 255) {
        alpha = 255;  
      }
    } else {
      alpha = 255;  
    }
    
    fill(255, 255, 255, alpha);
    stroke(255, 255, 255, alpha);
    ellipse(x, y, spriteW, spriteH);
    
    fill(255, 0, 0, alpha);
    stroke(255, 0, 0, alpha);
    ellipse(x-20, y-20, 20, 20);
    
    fill(255, 0, 0, alpha);
    stroke(255, 0, 0, alpha);
    ellipse(x+20, y-20, 20, 20);
  }
  
  void display() {
    update();
    draw_sprite(x, y);  
  }
  
  void respawn() {
    hit = false;
    x = random(0, width);
    y = random(0, height);
    alpha = 0.0;
  }
  
  // Update the particle's position
  void update() {
    xoffset = (xoffset + random(-7, 7))/2;
    yoffset = (xoffset + random(-7, 7))/2;
    
    // Check on the x coordinate
    if ((x + xoffset) < 0) {
      x = 0;
    } else if ((x + xoffset) > width) {
      x = width;
    } else {
      x += xoffset;
    }
    
    // Check on the y coordinate
    if ((y + yoffset) < 0) {
      y = 0;
    } else if ((y + yoffset) > height) {
      y = height;
    } else {
      y += yoffset;
    }
  }
  
  boolean checkBoundingBox(float mx, float my) {
    if (((mx < (x + spriteW/2)) && (mx > (x - spriteW/2))) && 
    ((my < (y + spriteH/2)) && (my > (y - spriteH/2)))) {
      hit = true;  
    } else {
      hit = false;  
    }
    
    return hit;
  }
  
  // Getter functions
  float getx() {
    return x;  
  }
  
  float gety() {
    return y;  
  }
  
  // Setter functions
  void setSpriteFadeIn(float fade) {
    spriteFadeIn = fade;
    fadeInterval = 255 / (spriteFadeIn * frameRate);
  }
}