// Declare a PImage
PImage cosmos;

void setup() {
  // We can go back to our standard size
  size(600, 600);
  frameRate(30);
  smooth();

  // Load image from file
  // Image should already be in data directory.
  // That can be added using "Sketch -> Add File..."
  cosmos = loadImage("potw1010a_0_600px.jpg");
}

void draw() {
  background(0);
  
  //image(cosmos, 0, 0);
  
  //loadPixels();
  for (int i = 0; i < 600; i++) {
    for (int j = 0; j < 600; j++) {
      int index = j + i * 600;
      color pixelColor = cosmos.pixels[index];
      
      float r, g, b;
      r = red(pixelColor);
      g = green(pixelColor);
      b = blue(pixelColor);
      
      float avgColor = (r + g + b)/3;
      float frac = (avgColor/255.0) * 50;
      
      fill(pixelColor);
      ellipse(i, j, frac, frac);
    }
    println(i);
  }
  //updatePixels();
  noLoop();
}