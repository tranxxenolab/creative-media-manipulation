

void setup() {
  // We can go back to our standard size
  size(600, 600);
  frameRate(30);
  smooth();

}

void draw() {
  background(0);
  
  // This has to be called before any pixel operations
  loadPixels();
  
  // Let's iterate over each pixel in the screen
  // `pixels` is a 1D array
  for (int i = 0; i < pixels.length; i++) {
    // `color` allows us to define, well, a color 
    pixels[i] = color(random(0, 255), random(0, 255), random(0, 255));
  }
  
  // Once we've modified the pixel array, we need to update it 
  updatePixels();
}