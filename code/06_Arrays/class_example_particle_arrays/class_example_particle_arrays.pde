/*
This is the example code that we wrote
in class where we created moving particles
from scratch. We're going to now use arrays
to draw many particles on the screen.
*/

// Declare our Particles
// Note how the syntax is different from
// "primitive" datatypes like float
// Read this as:
// "declare an array of Particles called pArray"
Particle pArray[];

boolean shiftPressed = false;


void setup () {
  size(600, 600);
  background(0);
  smooth();
  frameRate(30);
  
  // We're going to use these values below
  // when we're setting up our particles
  float w = 0;
  float h = 0;
  float r, g, b;
  
  // How many particles should we draw on the
  // screen?
  int count = 10;
  
  // This is where things are a bit confusing.
  // This is our further array declaration like
  // with datatypes like float.
  // So we're saying here to Processing, we are
  // creating a new *array* of Particle objects 
  // of size count.
  pArray = new Particle[count];
  
  // We now need to instantiate each of those
  // Particle objects
  for (int i = 0; i < count; i++) {
    // In the loop, we go over *each* Particle object
    // in the array. We then *instantiate* each
    // of those objects as a new object of class
    // Particle. We set the initial position on
    // the screen to the middle of the window, and
    // give it an initial size of 10.
    pArray[i] = new Particle(width/2, height/2, 10);
    
    // We then choose random colors for each particle
    r = random(0, 255);
    g = random(0, 255);
    b = random(0, 255);
    
    // Read this syntax as:
    // "get the Particle object at position i
    // that is, pArray[i], and then call the 
    // setStroke method to set the stroke.
    // Same thing with setting the fill.
    pArray[i].setStroke(r, g, b);
    pArray[i].setFill(r, g, b);
    
    // We do the same thing to set the width 
    // to be random as well
    w = random(20, 50);
    pArray[i].setSize(w);
  }
  
}

// Our draw method becomes relatively easy!
void draw() {
  background(0);
  
  // New local varaibles to set 
  // novel random values to the particles
  // each time through the draw loop
  float r, g, b;
  
  // Loop through each element in the array.
  // Note the use of pArray.length to get
  // the size of the array!
  for (int i = 0; i < pArray.length; i++) {
    /*
    r = random(0, 255);
    g = random(0, 255);
    b = random(0, 255);
    
    // This is just like we did in the setup function.
    // We're now setting different random colors
    // *each time* through the draw loop
    pArray[i].setStroke(r, g, b);
    pArray[i].setFill(r, g, b);
    */
 
    // And this is all we have to do to draw things to
    // the screen:
    // * call the update method to update the position
    // randomly
    // * call the display method to actually draw
    // the particle to the screen
    pArray[i].update();
    pArray[i].display();  
  }
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == SHIFT) {
        
    }
  }
}
