class Particle {
  float x = 0;
  float y = 0;
  float size = 0;
  float xoffset = 0;
  float yoffset = 0;
  float strokeR = 255;
  float strokeG = 255;
  float strokeB = 255;
  float fillR = 255;
  float fillG = 255;
  float fillB = 255;
  
  Particle (float xinit, float yinit, float sizeinit) {
    x = xinit;
    y = yinit;
    size = sizeinit;
  }
  
  Particle () {
    x = width/2;
    y = height/2;
    size = 10;
  }
  
  void display() {
    fill(fillR, fillG, fillB);
    stroke(strokeR, strokeG, strokeB);
    ellipse(x, y, size, size);
  }
  
  void update() {
    xoffset = random(-7, 7);
    yoffset = random(-7, 7);
    
    if ((x + xoffset) < 0) {
      x = 0;  
    } else if ((x + xoffset) > width) {
      x = width;  
    } else {
      x += xoffset;  
    }
    
    if ((y + yoffset) < 0) {
      y = 0;  
    } else if ((y + yoffset) > height) {
      y = height;  
    } else {
      y += yoffset;  
    }
  }
  
  float getXPosition() {
    return x;  
  }
  
  float getYPosition() {
    return y;  
  }
  
  void setStroke(float r, float g, float b) {
    strokeR = r;
    strokeG = g;
    strokeB = b;
  }
  
  void setFill(float r, float g, float b) {
    fillR = r;
    fillG = g;
    fillB = b;
  }
  
  void setSize(float s) {
    size = s;  
  }
}
