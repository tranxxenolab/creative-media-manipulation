# Adafruit CircuitPython imports
import microcontroller
import board
import neopixel

# Standard Python library imports
import time

# Setup the neopixel
# We're going to use pin A0, and now we have two pixels connected to this pin, and we want the brightness to be a bit faint
pixels = neopixel.NeoPixel(board.A0, 2, brightness=0.2)

while True:
    # We're going to cycle through different colors for each NeoPixel.
    # `pixels` is a `list` whose length is the same as the number of NeoPixels defined above
    # We can set the color of each NeoPixel by assigning a `tuple` of RGB values to the appropriate element in the list
    pixels[0] = (0, 0, 0)
    pixels[1] = (255, 255, 255)
    # Show the pixels (i.e., send the commands to change the color)
    pixels.show()
    # Sleep for half a second
    time.sleep(0.5)
    
    pixels[0] = (255, 0, 0)
    pixels[1] = (0, 0, 255)
    pixels.show()
    time.sleep(0.5)
    
    pixels[0] = (0, 255, 255)
    pixels[1] = (0, 255, 0)
    pixels.show()
    time.sleep(0.5)