import time
import pulseio
import board

# Setup PWM on pin A1 
led = pulseio.PWMOut(board.A1, frequency=5000, duty_cycle=0)

# Setup some default values
# Changing `duty_cycle_inc` will change how fast the 
# LED fades in and out.
# The duty cycle is given as a 16bit number, so it goes from
# 0 to 65535. (See, knowing binary representation *does* help you!)
MIN_DUTY_CYCLE = 1000
MAX_DUTY_CYCLE = 65535
duty_cycle_inc = 50
current_duty_cycle = MIN_DUTY_CYCLE

while True:
    # Let's set the duty cycle of the LED to the `current_duty_cycle`
    led.duty_cycle = current_duty_cycle
    # Then update the duty cycle.
    current_duty_cycle += duty_cycle_inc
    # And then, ensure that we stay within our default limits.
    if (current_duty_cycle >= MAX_DUTY_CYCLE):
        duty_cycle_inc *= -1
        current_duty_cycle = MAX_DUTY_CYCLE
    elif (current_duty_cycle <= MIN_DUTY_CYCLE):
        duty_cycle_inc *= -1
        current_duty_cycle = MIN_DUTY_CYCLE