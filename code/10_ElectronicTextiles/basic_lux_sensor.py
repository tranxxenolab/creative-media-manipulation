# Adafruit CircuitPython imports
from digitalio import DigitalInOut, Direction, Pull
from analogio import AnalogIn, AnalogOut
import microcontroller
import board
import neopixel
import busio
import adafruit_tsl2561
import neopixel
import simpleio

# Standard library imports
import time

# Setup the neopixel
# We're going to use pin A0, we only have 1 neopixel attached to this pin, and we want the brightness to be somewhat faint
pixels = neopixel.NeoPixel(board.A0, 1, brightness=0.2)

# A2 (pad #0) is SDA
# A1 (pad #2) is SCL
i2c = busio.I2C(board.SCL, board.SDA)
tsl = adafruit_tsl2561.TSL2561(i2c)

while True:
    try:
        lux = tsl.lux
        if (lux is not None):
            print("Lux value is: %f" % lux)
    
            # Map our lux range to a range more suited to RGB values
            rValue = int(simpleio.map_range(lux, 6, 45, 0, 255))
            pixels.fill((rValue, 255, 255))
            pixels.show()
    except OSError:
        pass
    
    time.sleep(0.5)