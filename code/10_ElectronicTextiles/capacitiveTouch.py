import board
import time
from digitalio import DigitalInOut, Direction, Pull
from analogio import AnalogIn, AnalogOut
from touchio import TouchIn
import neopixel
import pulseio

# Standard Python library imports
import time

# Setup the neopixel
# We're going to use pin A0, we only have 1 neopixel attached to this pin, and we want the brightness to be somewhat faint
pixels = neopixel.NeoPixel(board.A0, 1, brightness=0.2)

# Capacitive touch on A2
touch2 = TouchIn(board.A2)

# Let's setup some default values for things, these are called *constants* in other languages.
# In Python the convention is to define these variables using
# ALL CAPS

# Some colors
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
WHITE = (255, 255, 255)
NUM_COLORS = 4

# Some values for brightness of the NeoPixel
BRIGHTNESS_MAX = 0.3
BRIGHTNESS_MIN = 0.01


# Now let's setup the default values for the variables we're going to use
# in our infinite loop.
counter = 0
currentColor = WHITE

# Changing `brightnessStep` will change how slow or fast the LED fades.
brightness = 0.3
brightnessStep = 0.0008

# These values have to do with the "touchiness" of the capacitive sensor.
# It can be triggered very easily, and if we let it be constantly triggered we'll cycle through
# our colors a lot faster than we'd like. So we need to use some sort of time threshold to determine
# when to move on to the next color. The function `time.monotonic()` works similarly to `frameCount` in
# Processing; it returns a value that is constantly increasing since the Gemma was powered on.
threshold = 0.2
currentTime = time.monotonic()

while True:

    # For our fading in and out, we determine whether or not our current brightness is within the
    # range of our BRIGHTNESS_MIN and BRIGHTNESS_MAX, and then increment or decrement the 
    # brightness accordingly. The size of `brightnessStep` determines how fast the NeoPixel
    # fades in or out.
    if (brightness >= BRIGHTNESS_MAX):
        brightness = BRIGHTNESS_MAX
        brightnessStep *= -1
    elif (brightness <= BRIGHTNESS_MIN):
        brightness = BRIGHTNESS_MIN
        brightnessStep *= -1
    brightness += brightnessStep
    pixels.brightness = brightness
    pixels.show()
    

    # Now we look at our capacitive touch value. If the Gemma senses a touch on the pin, then `value` will
    # be True
    if touch2.value:
        # Here's where we check whether or not the current monotonic time is greater than our threshold.
        # This keeps the capacitive sensor from being too "touchy".
        if ((time.monotonic() - currentTime) > threshold):
            # This should look familiar to you. We keep track of a counter, and then do modular division
            # to cycle through the colors.
            counter += 1
            
            if ((counter % NUM_COLORS) == 0):
                currentColor = WHITE
            elif ((counter % NUM_COLORS) == 1):
                currentColor = RED
            elif ((counter % NUM_COLORS) == 2):
                currentColor = BLUE
            elif ((counter % NUM_COLORS) == 3):
                currentColor = GREEN
            else:
                currentColor = (255, 255, 0)
            
            pixels.fill(currentColor)
            pixels.show()
            
            # We set the current time to be the current monotonic value so that the next time through the loop
            # we can easily check whether or not we're past the threshold value.
            currentTime = time.monotonic()
