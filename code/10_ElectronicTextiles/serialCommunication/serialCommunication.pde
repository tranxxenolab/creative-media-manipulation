import processing.serial.*;

Serial myPort;
String val;
int touchVal = 0;
float floatVal = 0;

void setup() {
  // Use the following code to determine what serial port your Gemma device is connected to.
  // Use the one that has a name including something like "/dev/tty.usbmodem....".
  // Once you've figured out which serial port to use, use index of that array value
  // in the port name code below (line 16).
  //printArray(Serial.list());
  size(600, 600);
  frameRate(5);
  String portName = Serial.list()[1];
  myPort = new Serial(this, portName, 115200);
  background(0, 0, 0);
}

void draw() {
  if (myPort.available() > 0) {
    // This line reads data from the serial port, i.e., your Gemma, until it finds
    // a newline
    val = myPort.readStringUntil('\n');
    //val = myPort.readString();
  }
  
  if (val != null) {
    // These two lines are not needed unless you're working with the 
    // capcitive sensor
    //println(val.length());
    //touchVal = int(val.charAt(0));
    
    // This line converts the received string over the serial port into a float value
    // Once you've done this conversion, you can use this elsewhere in your code.
    floatVal = float(val);
    println(floatVal);
  }
  
  /*
   * This is only needed to test the touch/capacitive sensing
  if (touchVal == 49) {
    background(255, 0, 0); 
  } else if (touchVal == 48) {
    background(0, 255, 0); 
  }
  */
}