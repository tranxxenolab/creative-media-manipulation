# Gemma IO demo
# Welcome to CircuitPython 2.0.0 :)

import board
import time
from digitalio import DigitalInOut, Direction, Pull
from analogio import AnalogIn, AnalogOut
from touchio import TouchIn
import time

# Built in red LED
led = DigitalInOut(board.D13)
led.direction = Direction.OUTPUT

# Capacitive touch on A2
touch2 = TouchIn(board.A2)

val = 0
while True:
    if val != touch2.value:
        val = touch2.value
        # This line below will print a single digit (either 1 or 0) that
        # can then be read by Processing
        print("%d" % val)
    led.value = touch2.value
    
    # If, however, you want to send a float, like with a lux sensor or
    # accelerometer, you can use the following to print a nicely formatted float
    # value (with, in the example below, three values past the decimal point on the right
    # being printed)
    print("%.3f" % 4.567483)
    
    time.sleep(0.25)
    
    