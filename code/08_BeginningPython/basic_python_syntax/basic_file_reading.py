"""
In this example we're going to look at reading in a file and doing basic manipulation of it.
"""

if __name__ == "__main__":
    # This is the syntax for reading in a file.
    # This construct ensures that the file is closed once you're out of this
    # block.
    with open("data/AliceInWonderland.txt", "rU") as f:
        # This reads each line of the text file separately, and adds it to a
        # list. Thus, `lines` contains every line of the text file as a
        # separate entry in the list.
        lines = f.readlines()

    # Since each line of the text file is separate, and we'd like to do some
    # operations on one very long string, we just `join` all of the lines
    # together. This python construct basically says, "take each entry in the
    # list `lines` and join it with the next one with an empty string".
    aliceText = "".join(lines)

    # Now we do some replacements. We're going to try and tally up each word
    # in the text to get an idea of distribution. But there are a lot of extra
    # characters we don't care about, like most punctuation and the such.
    # We're going to try and remove as many of those as we can.
    aliceTextReplaced = aliceText
    aliceTextReplaced = aliceTextReplaced.replace("\"", "")
    aliceTextReplaced = aliceTextReplaced.replace(",", "")
    aliceTextReplaced = aliceTextReplaced.replace(".", "")
    aliceTextReplaced = aliceTextReplaced.replace(";", "")
    aliceTextReplaced = aliceTextReplaced.replace("!", "")
    aliceTextReplaced = aliceTextReplaced.replace("?", "")
    aliceTextReplaced = aliceTextReplaced.replace("[", "")
    aliceTextReplaced = aliceTextReplaced.replace("]", "")
    aliceTextReplaced = aliceTextReplaced.replace("(", "")
    aliceTextReplaced = aliceTextReplaced.replace(")", "")
    aliceTextReplaced = aliceTextReplaced.replace("_", "")
    aliceTextReplaced = aliceTextReplaced.replace(":", "")
    aliceTextReplaced = aliceTextReplaced.replace("-", "")

    # One wrinkle is that there are a lot of "newlines" in the file.
    # Let's replace those all with spaces.
    aliceTextReplaced = aliceTextReplaced.replace("\n", " ")

    # Now we need to start working on the individual word. How might we do
    # that? Well, one thing that we can do with strings quite easily is split
    # a long string into a list of individual strings based on a *separator*.
    # What's the best separator to use with a text? The *space*.
    aliceTextList = aliceTextReplaced.split(" ")

    # Now let's print out how many words we have total.
    aliceTextNumWords = len(aliceTextList)
    print("_Alice in Wonderland_ has %d words" % aliceTextNumWords)

    # We now want to find out how many times the word "the" occurs in the text,
    # or "Alice", or so on. We do this by setting up a dictionary, whose keys
    # will be the words in the text, and whose values will be a running count
    # that increases by one each time the word occurs. In natural language
    # processing we don't necessarily think of each element as a "word" but
    # rather as a *token*; we'll see that terminology again in a bit.

    # But first we need to setup our dictionary before we start going through
    # the list. This creates an empty dictionary.
    aliceTokensDict = {}

    # And now we just iterate through our list of words that we created earlier.
    for token in aliceTextList:
        # Given that some words might have upper or lower case letters in them,
        # let's just transform all words to lower case.
        lowerToken = token.lower()

        # Now we need to check if this token exists in the dictionary yet
        if lowerToken in aliceTokensDict.keys():
            # So if the token is already in the list of keys in our dict,
            # then we can access it and just add 1 to the total
            aliceTokensDict[lowerToken] += 1
        else:
            # And if it's not there, we need to add the key and set it to 1,
            # initially.
            aliceTokensDict[lowerToken] = 1

    # A simpler way to do this is to call
    # from collections import Counter
    # at the top of your code (where you import other packages),
    # and then call
    # aliceFreq = Counter(aliceTextList)
    # to count up all of the frequencies of the words

    # And now we're done! Print out the dictionary to see the values
    print(aliceTokensDict)

    # Remember the one rub with working with dictionaries: nothing is sorted!
    # So to do that we can use the following bit of mess.
    # Basically the idea is this: `sorted` allows you to sort a list.
    # But we have a dictionary, not a list, so by default it's going to sort
    # the keys, rather than the values. But we can pass *a function* to the
    # argument named `key`. The function we're going to pass is...the function
    # to get a value from the dictionary, `get`. So `sorted` will then sort
    # based on the values, but return, as a list, the *keys* that are associated
    # with each value. Since we'd like to get the most used first, we
    # `reverse` the list. So we now have a list of the words, in descending
    # order. Let's add those to a new "list of lists", where each element in
    # the list is another list consisting of the word and the number of times
    # it exists in the text.
    sortedTokensList = []
    for w in sorted(aliceTokensDict, key = aliceTokensDict.get, reverse=True):
        sortedTokensList.append([w, aliceTokensDict[w]])


    # Now this should be sorted
    print(sortedTokensList)

    # We can then ask some questions about the text
    print("\n")

    # What percentage of the time does Alice's name appear in the text?
    # Yes, the three percent signs looks odd! But in order to print a percent
    # sign, you use two percent signs to "escape" it.
    print("Alice's name is %f%% of the text." % (100 * (aliceTokensDict["alice"]/aliceTextNumWords)))

    print("The word \"the\" is %f%% of the text." % (100 * (aliceTokensDict["the"]/aliceTextNumWords)))

    print("The word \"cat\" is %f%% of the text." % (100 * (aliceTokensDict["cat"]/aliceTextNumWords)))

    print("The word \"cheshire\" is %f%% of the text." % (100 * (aliceTokensDict["cheshire"]/aliceTextNumWords)))

    print("The word \"hatter\" is %f%% of the text." % (100 * (aliceTokensDict["hatter"]/aliceTextNumWords)))

    print("The word \"queen\" is %f%% of the text." % (100 * (aliceTokensDict["queen"]/aliceTextNumWords)))
