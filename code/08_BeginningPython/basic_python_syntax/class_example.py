import random

intval = 3

floatval = -5.6748393

floatvalConverted = int(floatval)

# This is a comment, and while it was raining when I wrote the sample code it's not today!!
raining = True
sun = False

hello = "Hello, world!"
helloLowercase = hello.lower()

# In python arrays are called lists
intlist = [1, 4, -9, 2, 3]
strlist = ["Some", "string", "with", "some", "words"]
mixedList = ["Here", "we", "have", 45, "some", -2.345]

mixedList.append("five")

strvalue = mixedList[2]
value = mixedList[3]
# ["Here", "we", "have", 45, "some", -2.345, "five"]
value = mixedList[-4]

mydict = {"Hello": "goodbye", "one": 2, "five": 23.4568}
value = mydict["Hello"]
value = mydict["five"]
mydict["once"] = "upon a time"
listofkeys = mydict.keys()
listofvalues = mydict.values()

if raining:
    print("It's raining!")

    if not sun:
        print("The sun isn't shining :-(")


print("this line will run even if raining is true")

snowing = False

if (raining or snowing):
    print("Wetness in April, I guess")
elif sun:
    print("It's actually sunny out!")
else:
    print("Who knows what the weather is like today!")

if raining and not sun:
    print("Can't see the sun through those rainclouds!")

for item in mixedList:
    print(item)

for i in range(0, 10):
    print(i)

name = "Adriana"
print("Hello, %s" % (name))
number = 42
print("Hello, %s. The answer to life universe and everything is %d" % (name, number))
floatNumber = -23.456
print("The number is %f" % floatNumber)

def hello_world():
    print("Hello world!")

hello_world()

def hello_name(personName, val = 127):
    print("Hello %s. The value is %d" % (personName, val))

hello_name("N. Adriana", 42)

def square_number(x):
    result = x * x
    return result

print("The result of 10 * 10 is %d" % square_number(10))