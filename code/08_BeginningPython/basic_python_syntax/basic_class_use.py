"""
Here we're going to learn how to use the class we wrote in
`Computer.py`
"""


from Computer import Computer

if __name__ == "__main__":
    c1 = Computer(name = "nomadic", computerType="Mac")
    c2 = Computer(name = "whatsit", computerType="PC")
    c3 = Computer(name = "ultranorm", computerType="PC", manufacturer="Dell")

    computers = [c1, c2, c3]

    for computer in computers:
        print(computer.getName())
        print(computer.printKind())
