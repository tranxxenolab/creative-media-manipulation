"""
In this file we're going to go through some of the basic python syntax.
A lot of this is might begin to look familiar to you from your experience
with Processing.
There are definitely some specific Python ideosyncrasies that we'll go
into in depth, however.
"""

# In Python there *are* specific datatypes, but you don't declare them
# when you define your variable. As well, you can declare varaibles wherever
# you'd like in your code.
# Basic Python datatypes include ints, floats, booleans, strings, tuples,
# sets, lists, and dictionaries. We're not going to worry too much about
# tuples and sets.
# You've seen all of the other datatypes in Processing, excepting dictionaries.

# This means that you can define variables where you like, and without needing
# to explictly say what they are. The type is inferred from context.


# This would be an int
xpos = 5

# This a float
randomval = 4.357684593

# Booleans are either `True` or `False` (note the capital letters!)
raining = False

snowing = False

sun = True

# Strings are declared very easily
hello = "Hello, world!"

# Lists are like arrays, and they are enclosed in square brackets.
# Also, strings exist as their own datatype and are enclosed in double quotes.
towns = ["Wellesley", "Natick", "Needham", "Boston", "Cambridge"]

# Lists can include unlike datatypes.
values = [3, -4.5679, True, "What's this?"]

# Dictionaries are a new datatype you haven't seen before.
# In a dictionary you associate a "key" with a "value". Each "key"
# can only refer to a *single* value. Dictionaries have no particular
# order to them; that is, unlike a list, you can't assume that items in a
# dictionary will be in the same order each time you access it.

# See https://www.youtube.com/watch?v=wa2nLEhUcZ0 for context
thoughts = {"Monday": "blue",
            "Tuesday": "gray",
            "Wednesday": 2,
            "Thursday": "don't care",
            "Friday": "love"}

# Dictionaries can also include other datatypes
combo = {"a list": towns,
         "days": thoughts,
         5: 20,
         "a really long key that I wouldn't recommend using but you can": values}

# Each of the datatypes as a corresponding `function` that you can use to
# convert one kind of datatype to another. Note that this won't always do what
# you want!
# These are the datatype functions:
# int, float, bool, str, list, dict
intval = int(4.356784)
floatval = float(3)

# Now, here's another thing about Python: **everything** is an object!
# That means your intval is an object. Your floatval is an object. And, like
# Processing, your strings are objects too. As well as your lists and dicts.
# Which means you can sometimes see something wonky like the following,
# which we'll explain in a bit:
sentence = " ".join(["This", "is", "now", "a", "sentence."])

# Now, let's go through how to do things with these variables.
if __name__ == "__main__":
    # You can print out values just like you did in Processing.
    # By default, `print` also includes a newline at the end of each statement.
    print("PRINTING VALUES")
    print(xpos)
    print(randomval)

    print("\n")

    # Blocks are defined by spaces, not by curly brackets.
    # You can do boolean tests with English words, rather than the symbols
    # used in Processing
    print("WORKING WITH BOOLEANS")
    if ((not raining) and (not snowing)):
        print("Wow, it's not raining nor snowing???")

    # The following is *also* legit Python code, although I almost always prefer
    # to use parentheses to be explicit about things.
    if not raining and not snowing:
        print("Wow, it's not raining nor snowing?")

    print("\n")

    # Booleans work like you would expect
    if (sun):
        print("The sun is shining")

    print("\n")

    # Just like normal
    if (xpos > randomval):
        print("That value (%d) is larger than the other value (%f)" % (xpos, randomval))

    print("\n")

    # You can print strings like you would expect
    print("PRINTING STRINGS")
    print(hello)

    print("\n")

    print("WORKING WITH LOOPS")
    for town in towns:
        print(town)

    print("\n")

    print(towns[3])

    print("\n")

    for town in towns:
        print(town.upper())

    print("\n")

    for value in values:
        print("Value: %s" % value)

    print("\n")

    # Python doesn't have a specific syntax for the `for (int i = 0; i < 10;
    # i++)` kind of loops. So we use a function called `range()` that
    # takes a lower and upper limit to produce a list of indices.
    print("WORKING WITH LOOP INDICES")
    for i in range(0, 10):
        print("Current index is %d" % i)

    print("\n")

    # One thing to note! You can think of strings as lists of characters
    # too, # just like we discussed when we learned about them in Processing.
    # Thus, you # can use the syntax you learned just before with lists to
    # access individual characters in the string.
    print("WORKING WITH STRING INDICES")
    print(hello[3])

    print("\n")

    # And, you can use *negative* indices to start counting from the *end* of
    # the string. This will be helpful at times.
    print(hello[-4])


    print("\n")

    print("WORKING WITH DICTIONARIES")
    for key in thoughts.keys():
        print(thoughts[key])

    print("\n")

    for key in combo.keys():
        print("Key (%s) and value (%s)" % (key, combo[key]))

    print("\n")

    print(sentence)
