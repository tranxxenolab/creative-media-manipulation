void setup() {
  size(500, 500);
  background(0);
  smooth();
}

void draw() {
  stroke(255);
  fill(255);
  background(0);
  
  // Check which quadrant the mouse pointer is in
  // Change the background color of the entire window
  // depending on which quadrant the pointer is in
  if ((mouseX <= (width/2)) && (mouseY <= (height/2))) {
    background(0, 0, 0);
  } else if ((mouseX > (width/2)) && (mouseY <= (height/2))) {
    background(255, 255, 255);
  } else if ((mouseX > (width/2)) && (mouseY > (height/2))) {
    background(0, 0, 255);
  } else if ((mouseX <= (width/2)) && (mouseY > (height/2))) {
    background(0, 255, 0);
  } else {
    println("We should never get here");
  }
}