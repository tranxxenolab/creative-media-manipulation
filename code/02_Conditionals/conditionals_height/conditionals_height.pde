void setup() {
  size(500, 500); // Let's work with a bigger window
  background(0);
  smooth(); // This turns on "anti-aliasing", which smooths out jagged edges
}

void draw() {
  stroke(255);
  fill(255);
  background(0);
  
  // Check if the mouse is in the top or bottom of the window
  if (mouseY < (height/2)) {
    background(0, 255, 0);
  } else {
    background(0, 0, 255);
  }
}