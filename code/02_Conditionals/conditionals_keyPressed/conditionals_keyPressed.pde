boolean yellow = false;

void setup() {
  size(500, 500);
  background(0);
  smooth();
}

void draw() {
  stroke(255);
  fill(255);
  
  // Change the background color of the window
  // depending on the state of the variable yellow
  if (yellow) {
    background(255, 255, 0);
  } else {
    background(127);
  }
  
  // Check which quadrant the mouse pointer is in
  // Draw a differently colored square depending on
  // which quadrant the mouse pointer is in
  if ((mouseX <= (width/2)) && (mouseY <= (height/2))) {
    fill(0);
    stroke(0);
    rect(0, 0, width/2, height/2);
  } else if ((mouseX > (width/2)) && (mouseY <= (height/2))) {
    fill(255);
    stroke(255);
    rect(width/2, 0, width, height/2);
  } else if ((mouseX > (width/2)) && (mouseY > (height/2))) {
    fill(0, 0, 255);
    stroke(0, 0, 255);
    rect(width/2, height/2, width, height);
  } else if ((mouseX <= (width/2)) && (mouseY > (height/2))) {
    fill(0, 255, 0);
    stroke(0, 255, 0);
    rect(0, height/2, width/2, height);
  } else {
    println("We should never get here");
  }
}

void keyPressed() {
  // Note the single quotes and double equals sign!
  if (key == 'y') {
    yellow = true;  
  }
}