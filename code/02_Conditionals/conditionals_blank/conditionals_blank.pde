void setup() {
  size(500, 500); // Let's work with a bigger window
  background(0);
  smooth(); // This turns on "anti-aliasing", which smooths out jagged edges
  
  int val1 = 98;
  int val2 = 23;
  int val3 = 15;

  if (val1 > val2) {
    println("val1 > val2");
  }
  
  if (val2 > val3) {
    println("val2 > val3");
  }
  
  if (val3 > val1) {
    println("val3 > val1");
  } else {
    println("this will be printed when none of the conditions are met");
  }
}

void draw() {
 
}
