int xPos = 0;

void setup() {
  size(500, 500);
  background(0);
  smooth();
}

void draw() {
  stroke(255);
  fill(255);
  background(0);
  //ellipse(xPos, height/2, 10, 10);
  ellipse(xPos, 90*sin(frameCount*0.01) + height/2, 10, 10);
  ellipse(xPos, 50*sin(frameCount*0.1) + height/2, 10, 10);
  ellipse(xPos, 30*sin(frameCount*0.2) + height/2, 10, 10);
  ellipse(xPos, 10*sin(frameCount*0.3) + height/2, 10, 10);
  xPos += 1;
  
  if (xPos >= (width)) {
    xPos = 0;  
  }
  
  /*
  if ((mouseX <= (width/2)) && (mouseY <= (height/2))) {
    background(0, 0, 0);
  } else if ((mouseX > (width/2)) && (mouseY <= (height/2))) {
    background(255, 255, 255);
  } else if ((mouseX > (width/2)) && (mouseY > (height/2))) {
    background(0, 0, 255);
  } else if ((mouseX <= (width/2)) && (mouseY > (height/2))) {
    background(0, 255, 0);
  } else {
    println("We should never get here");
  }
  */
  
  /*
  if (mouseX < (width/2)) {
    background(255);
  } else {
    background(0);
  }
  */
  
  /*
  if (mouseY < (height/2)) {
    background(0, 255, 0);
  }  else {
    background(0, 0, 255);
  }
  */
}
