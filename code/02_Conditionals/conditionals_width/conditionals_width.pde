void setup() {
  size(500, 500); // Let's work with a bigger window
  background(0);
  smooth(); // This turns on "anti-aliasing", which smooths out jagged edges
}

void draw() {
  stroke(255);
  fill(255);
  background(0);
  
  // Check if the mouse is in the left or the right of the window
  if (mouseX < (width/2)) {
    background(255);
  } else {
    background(0);
  }
}