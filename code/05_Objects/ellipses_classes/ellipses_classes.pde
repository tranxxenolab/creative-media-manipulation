Particle p1;
Particle p2;
Particle p3;

void setup() {
  size(600, 600);
  background(0);
  smooth();
  frameRate(30);
  
  // Here we create three instances of type Particle
  // And we use the Particle constructor to define
  // the width and height of each particle.
  p1 = new Particle(width/2, height/2);
  p2 = new Particle(width/2, height/2);
  p3 = new Particle(width/2, height/2);
}

void draw() {
  background(0);

  // Update each particle's position
  p1.update();
  p2.update();
  p3.update();
  
  // Draw each particle on the screen
  p1.display();
  p2.display();
  p3.display();
}