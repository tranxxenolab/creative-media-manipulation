class Sprite {
  float x = 0;
  float y = 0;
  float xoffset = 0;
  float yoffset = 0;
  int spriteFrame = 0;
  int spriteFrameDuration = 10;
  int spriteW = 80;
  int spriteH = 80;
  boolean hit = false;
  
  Sprite() {
    x = width/2;
    y = width/2;
  }
  
  Sprite(float sx, float sy) {
    x = sx;
    y = sy;
  }
  
  void draw_sprite(float x, float y) {
    
    if ((spriteFrame < spriteFrameDuration)) {
      draw_sprite_frame001(x, y);  
    }
    
    if ((spriteFrame >= spriteFrameDuration) && (spriteFrame <= (2 * spriteFrameDuration))) {
      draw_sprite_frame002(x, y);  
    }
    
    if ((spriteFrame > 2*spriteFrameDuration) && (spriteFrame <= (3 * spriteFrameDuration))) {
      draw_sprite_frame003(x, y);  
    }
    
    if (spriteFrame >= (3 * spriteFrameDuration)) {
      spriteFrame = 0;
    } else {
      spriteFrame += 1;
    }
  }
  
  // Here's our function for drawing sprites
  void draw_sprite_frame001(float x, float y) {
    fill(255);
    stroke(255);
    ellipse(x, y, spriteW, spriteH);
    
    fill(0, 0, 255);
    stroke(0, 0, 255);
    ellipse(x-20, y-20, 20, 20);
    
    fill(0, 0, 255);
    stroke(0, 0, 255);
    ellipse(x+20, y-20, 20, 20);
  }
  
  // Here's our function for drawing sprites
  void draw_sprite_frame002(float x, float y) {
    fill(255);
    stroke(255);
    ellipse(x, y, spriteW, spriteH);
    
    fill(0, 255, 0);
    stroke(0, 255, 0);
    ellipse(x-20, y-20, 20, 20);
    
    fill(0, 255, 0);
    stroke(0, 255, 0);
    ellipse(x+20, y-20, 20, 20);
  }
  
  // Here's our function for drawing sprites
  void draw_sprite_frame003(float x, float y) {
    fill(255);
    stroke(255);
    ellipse(x, y, spriteW, spriteH);
    
    fill(255, 0, 0);
    stroke(255, 0, 0);
    ellipse(x-20, y-20, 20, 20);
    
    fill(255, 0, 0);
    stroke(255, 0, 0);
    ellipse(x+20, y-20, 20, 20);
  }
  
  void display() {
    update();
    draw_sprite(x, y);  
  }
  
  void respawn() {
    hit = false;
    x = random(0, width);
    y = random(0, height);
  }
  
  // Update the particle's position
  void update() {
    xoffset = (xoffset + random(-7, 7))/2;
    yoffset = (xoffset + random(-7, 7))/2;
    
    // Check on the x coordinate
    if ((x + xoffset) < 0) {
      x = 0;
    } else if ((x + xoffset) > width) {
      x = width;
    } else {
      x += xoffset;
    }
    
    // Check on the y coordinate
    if ((y + yoffset) < 0) {
      y = 0;
    } else if ((y + yoffset) > height) {
      y = height;
    } else {
      y += yoffset;
    }
  }
  
  boolean checkBoundingBox(float mx, float my) {
    if (((mx < (x + spriteW/2)) && (mx > (x - spriteW/2))) && 
    ((my < (y + spriteH/2)) && (my > (y - spriteH/2)))) {
      hit = true;  
    } else {
      hit = false;  
    }
    
    return hit;
  }
  
  // Getter functions
  float getx() {
    return x;  
  }
  
  float gety() {
    return y;  
  }
}