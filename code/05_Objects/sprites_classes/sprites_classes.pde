// Keep track of whether or not the screen needs to be updated
boolean dirty = true;

int totalScore = 0;

int scoreIncrement = 10;

Sprite s1;
Sprite s2;
Sprite s3;

void setup() {
  size(600, 600);
  background(0);
  frameRate(30);
  
  s1 = new Sprite(random(0, width), random(0, height));
  s2 = new Sprite(random(0, width), random(0, height));
  s3 = new Sprite(random(0, width), random(0, height));
}

void draw() {
  background(0);
  s1.display();
  s2.display();
  s3.display();
}

void updateScore(float posx, float posy) {
  totalScore += scoreIncrement;
  
  println("Hit sprite at ", posx, " ", posy);
  println("Total score now ", totalScore);
}

void mousePressed() {
  if (s1.checkBoundingBox(mouseX, mouseY)) {
    s1.respawn();
    updateScore(s1.getx(), s1.gety());
  }
  
  if (s2.checkBoundingBox(mouseX, mouseY)) {
    s2.respawn(); 
    updateScore(s2.getx(), s2.gety());
  }
  
  if (s3.checkBoundingBox(mouseX, mouseY)) {
    s3.respawn();
    updateScore(s3.getx(), s3.gety());
  }
}