Particle p1;
Particle p2;
Particle p3;

void setup() {
  // We're going to use variables below for setting the width
  // and height of each particle
  float w = 0;
  float h = 0;
  
  size(600, 600);
  background(0);
  smooth();
  frameRate(30);
  
  // Here we create three instances of type Particle
  // And we use the Particle constructor to define
  // the width and height of each particle.
  p1 = new Particle(width/2, height/2);
  p2 = new Particle(width/2, height/2);
  p3 = new Particle(width/2, height/2);
  
  // We set our colors accordingly
  p1.setStroke(255, 0, 0);
  p1.setFill(255, 0, 0);
  p2.setStroke(0, 255, 0);
  p2.setFill(0, 255, 0);
  p3.setStroke(0, 0, 255);
  p3.setFill(0, 0, 255);
  
  // We define the width and height of each particle randomly
  w = random(10, 40);
  h = w;
  p1.setWidth(w);
  p1.setHeight(h);
  
  w = random(10, 40);
  h = w;
  p2.setWidth(w);
  p2.setHeight(h);
  
  w = random(10, 40);
  h = w;
  p3.setWidth(w);
  p3.setHeight(h);
}

void draw() {
  background(0);

  // Update each particle's position
  p1.update();
  p2.update();
  p3.update();
  
  // Draw each particle on the screen
  p1.display();
  p2.display();
  p3.display();
}