class Particle {
  // Define our class variables
  float x = 0;
  float y = 0;
  float xoffset = 0;
  float yoffset = 0;
  float strokeR = 0;
  float strokeG = 0;
  float strokeB = 0;
  float fillR = 0;
  float fillG = 0;
  float fillB = 0;
  float particleW = 10;
  float particleH = 10;
  
  // Define a constructor with no input arguments
  // Set the default values of x and y to be the middle of the window
  Particle() {
    x = width/2;
    y = height/2;
    
    // We define our default color to be red
    strokeR = 255;
    fillR = 255;
  }
  
  // Define our constructor
  Particle(float xinit, float yinit) {
    x = xinit;
    y = yinit;
    
    // We define our default color to be red
    strokeR = 255;
    fillR = 255;
  }
  
  // Draw the particle to the screen
  void display() {
    // Here we use our instance variables to define
    // the stroke and fill of our ellipse
    stroke(strokeR, strokeG, strokeB);
    fill(fillR, fillG, fillB);
    ellipse(x, y, particleW, particleH);
  }
  
  // Update the particle's position
  void update() {
    xoffset = random(-7, 7);
    yoffset = random(-7, 7);
    
    // Check on the x coordinate
    if ((x + xoffset) < 0) {
      x = 0;
    } else if ((x + xoffset) > width) {
      x = width;
    } else {
      x += xoffset;
    }
    
    // Check on the y coordinate
    if ((y + yoffset) < 0) {
      y = 0;
    } else if ((y + yoffset) > height) {
      y = height;
    } else {
      y += yoffset;
    }
  }
  
  // Getter functions
  float getx() {
    return x;  
  }
  
  float gety() {
    return y;  
  }
  
  // Setter functions
  void setStroke(float r, float g, float b) {
    strokeR = r;
    strokeG = g;
    strokeB = b;
  }
  
  void setFill(float r, float g, float b) {
    fillR = r;
    fillG = g;
    fillB = b;
  }
  
  void setWidth(float w) {
    particleW = w;  
  }
  
  void setHeight(float h) {
    particleH = h;  
  }
}