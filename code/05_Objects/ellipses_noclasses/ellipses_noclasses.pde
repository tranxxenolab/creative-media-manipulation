float x1 = 0;
float x2 = 0;
float x3 = 0;
float y1 = 0;
float y2 = 0;
float y3 = 0;

void setup() {
  size(600, 600);
  background(0);
  smooth();
  frameRate(30);
  
  x1 = width/2;
  x2 = width/2;
  x3 = width/2;
  
  y1 = height/2;
  y2 = height/2;
  y3 = height/2;
}

void draw() {
  background(0);
  
  updateEllipses();
  
  fill(255, 0, 0);
  stroke(255, 0, 0);
  ellipse(x1, y1, 20, 20);
  
  fill(0, 255, 0);
  stroke(0, 255, 0);
  ellipse(x2, y2, 20, 20);
  
  fill(0, 0, 255);
  stroke(0, 0, 255);
  ellipse(x3, y3, 20, 20);
  
}

void updateEllipses() {
  float x1offset = 0;
  float x2offset = 0;
  float x3offset = 0;
  float y1offset = 0;
  float y2offset = 0;
  float y3offset = 0;
  
  x1offset = random(-7, 7);
  if ((x1 + x1offset) < 0) {
    x1 = 0;
  } else if ((x1 + x1offset) > width) {
    x1 = width;
  } else {
    x1 += x1offset;
  }
  
  x2offset = random(-7, 7);
  if ((x2 + x2offset) < 0) {
    x2 = 0;
  } else if ((x2 + x2offset) > width) {
    x2 = width;
  } else {
    x2 += x2offset;
  }
  
  x3offset = random(-7, 7);
  if ((x3 + x3offset) < 0) {
    x3 = 0;
  } else if ((x3 + x3offset) > width) {
    x3 = width;
  } else {
    x3 += x3offset;
  }
  
  y1offset = random(-7, 7);
  if ((y1 + y1offset) < 0) {
    y1 = 0;
  } else if ((y1 + y1offset) > height) {
    y1 = height;
  } else {
    y1 += y1offset;
  }
  
  y2offset = random(-7, 7);
  if ((y2 + y2offset) < 0) {
    y2 = 0;
  } else if ((y2 + y2offset) > height) {
    y2 = height;
  } else {
    y2 += y2offset;
  }
  
  y3offset = random(-7, 7);
  if ((y3 + y3offset) < 0) {
    y3 = 0;
  } else if ((y3 + y3offset) > height) {
    y3 = height;
  } else {
    y3 += y3offset;
  }
}