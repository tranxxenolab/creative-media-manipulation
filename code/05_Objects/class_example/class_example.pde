/*
This is the example code that we wrote
in class where we created moving particles
from scratch.
*/

// Declare our Particles
Particle p1;
Particle p2;
Particle p3;
Particle p4;

void setup () {
  size(600, 600);
  background(0);
  smooth();
  frameRate(30);
  
  // Initialize and instantiate our particles
  // Note the use of the "new" keyword that
  // tells Processing we're *instantiating* an
  // *object* from the *class* Particle.
  // This also shows the overloading of the
  // constructor through the creation of particle
  // p4
  p1 = new Particle(width/2, height/2, 10);
  p2 = new Particle(width/2, height/2, 30);
  p3 = new Particle(width/2, height/2, 40);
  p4 = new Particle();
}

void draw() {
  // Because we're drawing everything from scratch each frame,
  // we need to clear the screen (set the background to 0) before
  // we draw anything.
  background(0);
  
  float strokeR = random(0, 255);
  float strokeG = random(0, 255);
  float strokeB = random(0, 255);
  
  // We then can call the update function for each
  // particle. Because we're using classes, the
  // ways in which each particle moves is independent
  // of each other. We have the same *interface* (we
  // use the method update() to update position with each
  // particle), but each particle *object* has different
  // variables associated with it, which means they're all
  // kept separate from each other.
  p1.update();
  p2.update();
  p3.update();
  p4.update();
  
  // We then update the display of the particle, i.e., draw it
  // in a new position on the screen.
  p1.display();
  p2.display();
  p3.display();
  p4.display();
  
  if ((frameCount % 30) == 0) {
    // Using getter functions, we get the x position 
    // of each particle
    println("Particle 1: ", p1.getXPosition());
    println("Particle 2: ", p2.getXPosition());
    println("Particle 3: ", p3.getXPosition());
    println("Particle 4: ", p4.getXPosition());
    
    // We then use setter functions to set the stroke, fill, 
    // and size to random values
    p1.setStroke(strokeR, strokeG, strokeB);
    p1.setFill(strokeR, strokeG, strokeB);
    p1.setSize(random(20, 200));
    
    p2.setStroke(random(0, 255), random(0, 255), random(0, 255));
    p2.setFill(random(0, 255), random(0, 255), random(0, 255));
    p2.setSize(random(20, 200));
    
    p3.setStroke(random(0, 255), random(0, 255), random(0, 255));
    p3.setFill(random(0, 255), random(0, 255), random(0, 255));
    p3.setSize(random(20, 200));
    
    p4.setStroke(random(0, 255), random(0, 255), random(0, 255));
    p4.setFill(random(0, 255), random(0, 255), random(0, 255));
    p4.setSize(random(20, 200));
    
  }
}
