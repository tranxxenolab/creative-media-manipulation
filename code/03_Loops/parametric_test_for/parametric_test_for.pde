// loosely based off of https://www.youtube.com/watch?v=LaarVR1AOvs

float frac = 0.05;
int randR = 0;
int randG = 0;
int randB = 0;

float x1 = 0;
float x2 = 0;
float y1 = 0;
float y2 = 0;

int originX = 0;
int originY = 0;

int type = 1;

boolean full = false;
boolean stop = false;

void setup() {
  size(600, 600);
  background(0, 0, 0);
  originX = width/2;
  originY = height/2;
  frameRate(30);
  smooth();
}

void draw() {
  //background(0, 0, 0);
  stroke(255);
  translate(originX, originY);
  
  if (stop) {
  } else {
    if (type == 1) {
      background(0);
      for (int i = 0; i < 10; i = i + 1) {
        x1 = 50*sin(((frameCount + i) * frac)/5) + 80*cos(((frameCount + i) * frac) + 1);
        y1 = 100*cos(((frameCount + i) * frac)/3) + 200*sin(((frameCount + i) * frac));
      
        x2 = 80*sin(((frameCount + i) * frac)/3) + 100*cos(((frameCount + i) * frac)/4);
        y2 = 100*cos(((frameCount + i) * frac)/3) + 40*sin(((frameCount + i) * frac)/6 - 50);
        
        if (full) {
      stroke(random(170, 255), random(170, 255), random(170, 255));  
    } else {
      randB = int(random(170, 255));
      stroke(randB, randB, randB);  
    }
    line(x1, y1, x2, y2);
      }
    } else if (type == 2) {
      x1 = 90*sin((frameCount * frac) / 3) + 10*cos(frameCount * frac - 56);
      y1 = 67*cos((frameCount * frac) / 2) + 100*sin(frameCount * frac + 20);
    
      x2 = 78*sin((frameCount * frac) / 10 + 45) + 60*cos(frameCount * frac);
      y2 = 23*cos((frameCount * frac) / 60 - 20) + 50*sin(frameCount * frac);
    }
    
    
    //ellipse(x1, y1, 1, 20);
    //ellipse(x2, y2, 1, 20);
  }
}

void keyPressed() {
  if (key == 'e') {
    background(0, 0, 0);
    println("erasing background");
  } else if (key == 'w') {
    background(255);
    println("setting backgorund to white");
  } else if (key == 'b') {
    background(0);
    println("setting background to black");
  } else if (key == '1') {
    type = 1; 
    println("setting type to 1");
  } else if (key == '2') {
    type = 2;
    println("setting type to 2");
  } else if (key == 'f') {
    full = true;
    println("full color");
  } else if (key == 'm') {
    full = false;
    println("monochrome");
  } else if (key == 'r') {
    background(0, 0, 0);
    originX = width/2;
    originY = height/2;
    type = 1;
    full = false;
    println("resetting everything");
  } else if (key == 's') {
    stop = !stop;
    if (stop) {
      println("stopping drawing");
    } else {
      println("starting drawing");
    }
  }
}

void mousePressed() {
  originX = mouseX;
  originY = mouseY;
  println("resetting origin");
}