void setup() {
  size(600, 600);
  background(0);
  smooth();
  frameRate(30);
}

void draw() {
  stroke(255);
  
  // This is a different way of using a for loop
  // whereby you iterate based on the 
  // *number of times* that you want
  // something to happen
  int inc = 20;
 
  // So here we basically say, "we want to draw
  // a line every 20 pixels from top to bottom,
  // so let's divide height by that increment,
  // and then use *that* as a factor to multiply
  // the increment by"
  for (int i = 0; i < (height/inc); i++) {
    line(100, i*inc, 500, i*inc);  
  }
}
