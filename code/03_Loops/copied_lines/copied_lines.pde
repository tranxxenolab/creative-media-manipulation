void setup() {
  size(600, 600);
  background(0);
  smooth();
}

void draw() {
  fill(255);
  stroke(255);
  
  // Draw a set of lines at regular intervals from 
  // the top of the screen to the bottom
  line(100, 0, 500, 0);
  line(100, 50, 500, 50);
  line(100, 100, 500, 100);
  line(100, 150, 500, 150);
  line(100, 200, 500, 200);
  line(100, 250, 500, 250);
  line(100, 300, 500, 300);
  line(100, 350, 500, 350);
  line(100, 400, 500, 400);
  line(100, 450, 500, 450);
  line(100, 500, 500, 500);
  line(100, 550, 500, 550);
}