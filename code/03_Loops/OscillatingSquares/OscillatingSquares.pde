int squareW = 20;
int squareH = 20;
boolean fullcolor = false;

void setup() {
  size(600,600);
  frameRate(30);
  smooth();
}

void draw() {
  background(0);
  
  for (int i = 0; i < (width/squareW); i++) {
    for (int j = 0; j < (height/squareH); j++) {
      //int r = int(random(0, 255));
      int r = int(128 + 127*sin(frameCount*(i+1)*(j+1)*0.0005));
      int g = int(128 + 127*sin(frameCount*(i+1)*(j+1)*0.0004));
      int b = int(128 + 127*sin(frameCount*(i+1)*(j+1)*0.0002));
 
      if (fullcolor) {
        stroke(r, g, b);
        fill(r, g, b);
      } else {
        stroke(r, r, r);
        fill(r, r, r);
      }
      
      rect(i*squareW, j*squareH, (i+1)*squareW, (j+1)*squareH); 
    }
  }
  
  //saveFrame("line-######.png");
}

void keyPressed() {
  if (key == 'f') {
    fullcolor = !fullcolor;
  }
}