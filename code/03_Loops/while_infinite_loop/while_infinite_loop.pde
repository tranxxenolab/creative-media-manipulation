// I do not advise running this code
// YOU HAVE BEEN WARNED!!!
// Probably nothing terrible will happen
// But in the worst case you might have to forcibly reboot your computer
// YOU HAVE BEEN WARNED!!!

int i = 600;

void setup() {
  size(600, 600);
  background(0);
  smooth();
}

void draw() {
  fill(255);
  stroke(255);

  // The ending condition here will never evaluate to false
  while(i < height) {
    line(100, i, 500, i);
    i -= 50;
  }
}