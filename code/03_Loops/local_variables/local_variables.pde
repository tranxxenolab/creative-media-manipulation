boolean printvar = false;

int var1 = 0;

void setup() {
  int var1 = 100;
  
  size(600, 600);
  background(0);
  smooth();
  
  println("In setup(), var1 is ", var1);
}

void draw() {
  int var1 = 200;
  fill(255);
  stroke(255);

  //println("In draw(), var1 is ", var1);
  if (printvar) {
    println("In draw(), var1 is ", var1);
    println("Setting var1 to 500");
    var1 = 500;
    printvar = !printvar;
  }
}

void keyPressed() {
  int var1 = 300;
  
  if (key == 'k') {
    println("In keyPressed(), var1 is", var1);  
  } else if (key == 'p') {
    printvar = !printvar;  
  }
}