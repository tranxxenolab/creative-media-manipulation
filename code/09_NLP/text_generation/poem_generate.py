"""
This file will allow us to generate a simple poem based off of randomly
choosing possible words and lines from pre-determined lists.
"""

# This library allows us to randomly choose an item from a list
import random

# Here are a few sentences, fragments, words that we're going to use
# below. It's often good to write out a particular "form" for the poem
# you'd like to generate. Then you can consider what kinds of phrases or fragments
# your options should take.
openings = ["It was morning.",
            "It was afternoon.",
            "It was raining.",
            "We left the barn at daybreak.",
            "We went.",
            "They saw us there."]
doings = ["Running",
          "Walking",
          "Waiting",
          "Loitering",
          "Sleeping",
          "Dozing",
          "Thinking",
          "Dreaming",
          "Loving",
          "Wanting"]
handings = ["touching",
            "feeling",
            "caressing",
            "holding"]
things = ["an idea,",
          "a thought,",
          "a desire,",
          "a dream,",
          "an action,",
          "a possibility,",
          "a memory,"]
closings = ["They were never seen again.",
            "We've never returned.",
            "It transformed us.",
            "We wonder where it went.",
            "Look over there."]

# The function to generate the poem is rather straightforward
def generate_poem():
    # We set our poem to be an empty string at first, since we're going to
    # use string substitution below to build the poem fragment by fragment,
    # line by line.
    poem = ""
    # We use `random.choice()` to randomly choose an item from a list
    poem = "%s\n%s" % (poem, random.choice(openings))
    # And we can build up a line based on the particular wordings that we chose
    # earlier
    poem = "%s\n%s, %s %s" % (poem, random.choice(doings),
                              random.choice(handings),
                              random.choice(things))
    poem = "%s\n%s" % (poem, random.choice(closings))
    return poem

if __name__ == "__main__":
    # And we can easily print out the poem here. Each time this code is run,
    # we get a different poem. Given the options above, there are
    # 8400 different poems that could be generated.
    print(generate_poem())