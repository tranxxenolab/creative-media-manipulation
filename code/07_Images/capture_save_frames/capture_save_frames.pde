import processing.video.*;

// Create an object for working with the video feed
// from the camera
Capture video;

void setup() {
  size(640, 480);
  
  video = new Capture(this, width, height);
  //printArray(Capture.list());
  
  video.start();
}

void captureEvent(Capture video) {
  video.read();  
}

void draw() {
  image(video, 0, 0);
  fill(255, 0, 0, 127);
  stroke(255, 0, 0, 127);
  ellipse(100, 100, 20, 20);
  
  //saveFrame("capture_frames-#####.png");
}
