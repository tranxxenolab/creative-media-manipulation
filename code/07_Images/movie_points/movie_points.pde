// You use the same library for reading movie files as you do
// for reading from cameras
import processing.video.*;

// Instead of `Capture`, we instantiate an object of type `Movie`
Movie movie;

void setup() {
  // This is the size of the movie file
  // You'll probably need to crop and resize a movie file using
  // other programs
  size(500, 500);
  
  // Like `Capture`, you need to use the `this` keyword, along with the
  // name of the movie file you want to load.
  // See the Processing reference for what kinds of files `Movie`
  // can read.
  movie = new Movie(this, "waving_500x500.mov");
  
  // You need to tell the movie object to start playing.
  // `play()` will cause the movie to play through once,
  // while `loop()` will loop it forever.
  //movie.play();
  movie.loop();
}

// Instead of `captureEvent` you have `movieEvent`, which is called
// every time there is a new frame to be read
void movieEvent(Movie movie) {
  movie.read(); 
}

void draw() {
  // Some of the local variables that we'll need below
  int x, y, loc;
  color c;
  float sz;
  int numPixels = 0;
  
  // Load the pixels of the current frame
  movie.loadPixels();
  
  // For some reason, it seems to take a few times through
  // the draw method for the pixels to be loaded. Because
  // of that, the `pixels` array will remain 0 for some times 
  // through the draw loop. As a result, we need to ensure that
  // the length of the `pixels` array is non-zero before
  // we try and index into it.
  // So we get the length of the array.
  numPixels = movie.pixels.length;
  
  // And then ensure that it's non-zero:
  if (numPixels > 0) {
    // Then we can do what we did for the webcam version of the code
    
    // Loop through a certain number of pixels each frame
    for (int i = 0; i < 10; i++) {
      // Get random locations in the window
      x = int(random(0, width));
      y = int(random(0, height));
      
      // Get the 1D location into the `pixels` array
      loc = x + y * width;
      
      // Get the color there
      c = movie.pixels[loc];
      
      // Draw a circle at a random x and y location and based on
      // the color of the underlying pixel at that location.
      fill(c);
      noStroke();
      sz = random(10, 20);    
      ellipse(x, y, sz, sz);
    }
  }
  
  //saveFrame("movie_points-####.png");
}
