import processing.video.*;

Capture video;
int prevx, prevy;

void setup() {
  size(1280, 720);
  
  video = new Capture(this, width, height);
  video.start();
  
  prevx = int(random(0, width));
  prevy = int(random(0, height));
}

void captureEvent(Capture video) {
  video.read();  
}

void draw() {
  video.loadPixels();
  
  for (int i = 0; i < 2; i++) {
    int x = int(random(0, width));
    int y = int(random(0, height));
    int loc = x + y * video.width;
    color c = video.pixels[loc];
   
    stroke(c);
    strokeWeight(5);
    line(prevx, prevy, x, y);
    prevx = x;
    prevy = y;
  }
      
}