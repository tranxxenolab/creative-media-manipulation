// Import the library for reading sound files
import ddf.minim.*;

// Color for quadrant backgrounds
color quadColor;

// Keeping track of which quad we're in
// 0 -> top left
// 1 -> top right
// 2 -> bottom right
// 3 -> bottom left
int quad = 0;

// Have we started moving yet?
boolean moved = false;

// Filenames for each of our sounds
String[] soundFilenames = {"bell.wav", "blonk.wav", "key.wav",
                      "metallic_tap.wav", "plink.wav", "spring.wav",
                      "tink_tonk.wav", "tomp.wav"};
Minim minim;
AudioSample[] samples;

void setup() {
  size(500, 500);
  
  // Initialize the library
  minim = new Minim(this);
  
  // Create a new array of `AudioSample`s
  samples = new AudioSample[soundFilenames.length];
  
  // Load each sample
  for (int i = 0; i < soundFilenames.length; i++) {
    println(soundFilenames[i]);
    // We give the filename to the sample as the first argument,
    // and then the "buffer size". You don't need to worry too
    // much about this, just set it to 512.
    samples[i] = minim.loadSample(soundFilenames[i], 512);  
  }
  
}

void draw() {
  // Clear the background
  background(0);
  
  // Only change the color once we've moved the mouse.
  if (moved) {
    // Set the colors
    fill(quadColor);
    stroke(quadColor);
    rectMode(CORNER);
    
    // Define the rectangles for the various quadrants
    if (quad == 0) {
      rect(0, 0, width/2, height/2);  
    } else if (quad == 1) {
      rect(width/2, 0, width/2, height/2);  
    } else if (quad == 2) {
      rect(width/2, height/2, width/2, height/2);  
    } else if (quad == 3) {
      rect(0, height/2, width/2, height/2);  
    }
  }
}

void mouseMoved() {
  // Which quad are we currently in?
  int currentQuad = 0;
  
  // We have moved, indeed
  moved = true;
  
  // Set the colors and `currentQuad` based on which quadrant we're in
  if ((mouseX < (width/2)) && (mouseY < (height/2))) {
    // top left
    quadColor = color(255, 0 , 0);
    currentQuad = 0;
  } else if ((mouseX >= (width/2)) && (mouseY < (height/2))) {
    // top right 
    quadColor = color(0, 255, 0);
    currentQuad = 1;
  } else if ((mouseX >= (width/2)) && (mouseY >= (height/2))) {
    // bottom right 
    quadColor = color(0, 0, 255);
    currentQuad = 2;  
  } else if ((mouseX < (width/2)) && (mouseY >= (height/2))) {
    // bottom left 
    quadColor = color(255, 255, 255);
    currentQuad = 3;  
  } else {
    currentQuad = 0;  
  }
  
  // Only when we move *out* of one quadrant and into another do we
  // play back ("trigger") the sound
  if (currentQuad != quad) {
    quad = currentQuad;
    samples[quad].trigger();   
  }
  
}