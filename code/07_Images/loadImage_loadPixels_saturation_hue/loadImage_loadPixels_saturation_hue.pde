// Image from https://www.nasa.gov/mission_pages/chandra/westerlund-2.html

PImage img;

float sat = 255;
float newHue = 255;

void setup() {
  size(1000, 700);
  frameRate(30);
  img = loadImage("wd2_1000x700.jpg");
}

void draw() {
  // Some local variables that we're going to use
  int loc = 0;
  color c;
  float h = 0;
  float s = 0;
  float b = 0;
  
  // Load the image's pixels
  img.loadPixels();
  
  // Loop through our image
  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      // Get our location into the 1D array
      loc = i + j * width;
      
      // Get current pixel's color
      c = img.pixels[loc];
      
      // Get the hue, saturation, brightness
      h = hue(c);
      s = saturation(c);
      b = brightness(c);
      
      // Set the colormode
      colorMode(HSB, 255, 255, 255);
      
      // Reset the color,
      // only changing the saturation
      img.pixels[loc] = color(newHue, sat, b);
 
    }
  }
  
  img.updatePixels();
  image(img, 0, 0);
  
}

// This is called each time the mouse is moved
void mouseMoved() {
  sat = map(mouseX, 0, width, 0, 255);
  newHue = map(mouseY, 0, height, 0, 255);
}
