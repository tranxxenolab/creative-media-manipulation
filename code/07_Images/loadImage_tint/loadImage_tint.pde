// Image from https://www.nasa.gov/mission_pages/chandra/galaxy-pictor-a.html

// Processing provides a `PImage` class for working with images
PImage img;

void setup() {
  size(500, 500);
  
  // Load the image from a file
  // This image needs to exist in the "data" folder
  img = loadImage("pictora_500x500.jpg");
}

void draw() {
  // Standard tint
  tint(255);
  // Display the `PImage` starting from the upper left corner
  image(img, 0, 0);
  
  // Change the values below to see what happens
  tint(255, 0, 0);
  //tint(0, 255, 0, 140);
  // Display the tinted image halfway across the screen
  image(img, width/2, 0);
}