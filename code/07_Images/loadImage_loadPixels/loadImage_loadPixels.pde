// Image from https://www.nasa.gov/mission_pages/chandra/westerlund-2.html

PImage img;

float saturation = 127;

void setup() {
  size(1000, 700);
  frameRate(30);
  img = loadImage("wd2_1000x700.jpg");
  smooth();
}

void draw() {
  // Some local variables that we're going to use
  int loc = 0;
  color c;
  float h = 0;
  float s = 0;
  float b = 0;
  
  // Load the image's pixels
  img.loadPixels();
  
  // Loop through our image
  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      // Get our location into the 1D array
      loc = i + j * width;
 
      // Get current pixel's color
      c = img.pixels[loc];
      
      // Get pixel's hue and brightness
      h = hue(c);
      b = brightness(c);
      
      // Set our colorMode
      colorMode(HSB, 255, 255, 255);
      
      // Change the pixel's color to be dependent on the changing saturation value
      img.pixels[loc] = color(h, saturation, b);
    }
  }
  
  img.updatePixels();
  image(img, 0, 0);
}

// This is called each time the mouse is moved
void mouseMoved() {
  // Map the x position of the mouse to between 0 and 255
  // This allows us to take a variable, mouseX,
  // and its bounds, 0 and width, and map that to another
  // set of values, in this case a saturation value between 0 and 255
  saturation = map(mouseX, 0, width, 0, 255);
}