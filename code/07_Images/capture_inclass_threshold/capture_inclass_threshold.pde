import processing.video.*;

// Create an object for working with the video feed
// from the camera
Capture video;
PImage destinationImage;
float threshold = 127;

void setup() {
  size(640, 480);
  
  destinationImage = createImage(width, height, RGB);
  
  video = new Capture(this, width, height);
  //printArray(Capture.list());
  
  video.start();
}

void captureEvent(Capture video) {
  video.read();  
}

void draw() {
  float b;
  color c;
  
  video.loadPixels();
  destinationImage.loadPixels();
  
  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      int loc = i + j*video.width;
      
      c = video.pixels[loc];
      
      b = brightness(c);
      
      if (b > threshold) {
        destinationImage.pixels[loc] = color(255, 255, 255);  
      } else {
        destinationImage.pixels[loc] = color(0, 0, 0);  
      }
    }
  }
  
  destinationImage.updatePixels();
  image(destinationImage, 0, 0);
}

void mouseMoved() {
  threshold = map(mouseX, 0, width, 0, 255);  
}
