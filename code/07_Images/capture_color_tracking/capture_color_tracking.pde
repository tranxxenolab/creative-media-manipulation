import processing.video.*;

Capture video;

// Keep track of current and tracked colors
color trackedColor;
color currentColor;

// Variables to track the best color, as well as current distance
float d = 0;
float bestd = 500;
int bestx = 0;
int besty = 0;

void setup() {
  size(640, 480);

  // Setup our video
  video = new Capture(this, width, height);
  video.start();
  
  // Define a default trackedColor
  trackedColor = color(255, 0, 0);
}

void captureEvent(Capture video) {
  video.read();  
}

void draw() {
  // Setup some local variables
  int loc;
  float r1, g1, b1, r2, g2, b2;
  
  // Set an arbitrarily large best distance
  bestd = 500;
  
  // Load the pixels
  video.loadPixels();
  
  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      // Get the current color
      loc = i + j * width;
      currentColor = video.pixels[loc];
      
      // Start saving the RGB values of the current color
      r1 = red(currentColor);
      g1 = green(currentColor);
      b1 = blue(currentColor);
      
      // And the tracked color
      r2 = red(trackedColor);
      g2 = green(trackedColor);
      b2 = blue(trackedColor);

      // Compute the Euclidian distance between the two RGB values
      d = dist(r1, g1, b1, r2, g2, b2);
      
      // If this new distance is less than the previous best distance,
      // save it
      if (d < bestd) {
        bestd = d;
        
        bestx = i;
        besty = j;
      }
    }
  }
  
  // Display the video frame
  image(video, 0, 0);
  
  // If the best distance is smaller than some arbitrary value,
  // draw a red circle on top of the best tracked pixel
  if (bestd < 10) {
    fill(255, 0, 0);
    noStroke();
    ellipse(bestx, besty, 50, 50);
  }
}

void mousePressed() {
  // Get the current location into the array based on the mouse coordiantes
  int loc = mouseX + mouseY * width;
  
  // Load the pixels
  video.loadPixels();
  
  // Set the `trackedColor` to the pixel underneath the mouse pointer
  trackedColor = video.pixels[loc];
}