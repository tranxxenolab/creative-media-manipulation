void setup() {
  // We can go back to our standard size
  size(600, 600);
  frameRate(30);
  smooth();
}

void draw() {
  int loc = 0;
  background(0);
  
  // This has to be called before any pixel operations
  loadPixels();
  
  // Let's iterate over each pixel in the screen
  
  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      // So this is how we get the index into that array from
      // two dimensions, width and height
      // Think about this as moving from left to right in one row,
      // then moving to the next row down, and so on from top
      // to bottom.
      // This then allows us to index into a 1d `pixels` array
      loc = i + j * width;
      
      // `color` allows us to define, well, a color 
      pixels[loc] = color(random(0, 255), random(0, 255), random(0, 255));
    }
  }
  
  // Once we've modified the pixel array, we need to update it 
  updatePixels();
}