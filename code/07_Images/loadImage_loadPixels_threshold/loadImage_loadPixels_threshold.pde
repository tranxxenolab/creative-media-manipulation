// Image from https://www.nasa.gov/mission_pages/chandra/galaxy-pictor-a.html

// We need two images objects:
// * One for the source image (the downloaded NASA image)
// * One for the destination image (that will hold our thresholded image)
PImage sourceImg;
PImage destinationImg;

// Define a global threshold variable
float threshold = 127.0;

void setup() {
  size(500, 500);
  
  // Load the source image like usual
  sourceImg = loadImage("pictora_500x500.jpg");
  
  // Create a new (blank) image that's the same size as the source image (or the window)
  // We have to tell it what color space to use for the image, in this case, RGB
  destinationImg = createImage(width, height, RGB);
}

void draw() {
  int loc; // location into the 1d arrays
  color c; // pixel color
  float f; // brightness value
  
  // Load the pixels for both images
  sourceImg.loadPixels();
  destinationImg.loadPixels();
    
  // Loop through the 2d pixels
  for (int i = 0; i < sourceImg.width; i++) {
    for (int j = 0; j < sourceImg.height; j++) {
      // Get our 1d location based on 2d values
      loc = i + j * sourceImg.width;
      
      // Get the color of the pixel at this `loc`
      c = sourceImg.pixels[loc];
      
      // Get the brightness of this pixel
      f = brightness(c);
      
      // If the brightness is above the threshold...
      if (f > threshold) {
        // Set the pixel color in the destination image to be white
        destinationImg.pixels[loc] = color(255, 255, 255);  
      } else {
        // Otherwise, set it to be black
        destinationImg.pixels[loc] = color(0, 0, 0);  
      }
    }
  }
  
  // Update the pixels array for the destination image
  destinationImg.updatePixels();
  
  // Finally display the new destination image
  image(destinationImg, 0, 0);
}

// This is called anytime the mouse is moved, but not pressed
void mouseMoved() {
  // Map the x position of the mouse to between 0 and 255
  // This allows us to take a variable, mouseX,
  // and its bounds, 0 and width, and map that to another
  // set of values, in this case a threshold between 0 and 255
  threshold = map(mouseX, 0, width, 0, 255);
}