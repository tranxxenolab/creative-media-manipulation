import processing.video.*;

Capture video;

// Global variables to save the coordinates of the
// brightest pixel, as well as its value
int brightx = 0;
int brighty = 0;
float brightvalue = 0;

void setup() {
  size(640, 480);

  video = new Capture(this, width, height);
  video.start();
}

void captureEvent(Capture video) {
  video.read();  
}

void draw() {
  // Local variables for brightness and color
  float b;
  color c;
  
  // Load the pixels
  video.loadPixels();
  
  // Give a starting value for `brightvalue` based on
  // the first pixel in the array
  brightvalue = brightness(video.pixels[0]);
  
  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      // Get the location
      int loc = i + j * video.width;
      
      // Read the values
      c = video.pixels[loc];
      b = brightness(c);
      
      // If the current pixel's value is brighter than 
      // the previous brightest value, update the values
      if (b > brightvalue) {
        brightx = i;
        brighty = j;
        brightvalue = b;
      }
    }
  }
  
  // Draw the video frame to the screen
  image(video, 0, 0);
  
  // Then draw a red circle at the location of the
  // brightest pixel
  fill(255, 0, 0);
  noStroke();
  ellipse(brightx, brighty, 50, 50);
}