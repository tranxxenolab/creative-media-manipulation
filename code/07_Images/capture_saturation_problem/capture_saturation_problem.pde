import processing.video.*;

Capture video;

void setup() {
  size(640, 480);

  video = new Capture(this, width, height);
  video.start();
}

void captureEvent(Capture video) {
  video.read();  
}

void draw() {

}

void mouseMoved() {
  
}