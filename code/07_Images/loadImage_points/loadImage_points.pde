PImage img;

void setup() {
  size(1000, 700);
  frameRate(60);
  img = loadImage("wd2_1000x700.jpg");
  smooth();
  
  background(0);
}

void draw() {
  // Some local variables
  int loc = 0;
  color c;
  float size = 10;
  
  // Load the image's pixels
  img.loadPixels();
  
  for (int i = 0; i < 10; i++) {
    int x = int(random(0, width));
    int y = int(random(0, height));
    loc = x + y * width;
    
    c = img.pixels[loc];
    fill(c, 127);
    stroke(c, 127);
    
    size = random(10, 25);
    
    ellipse(x, y, size, size);
  }
  
}