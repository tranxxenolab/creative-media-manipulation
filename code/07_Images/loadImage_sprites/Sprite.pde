class Sprite {
  float x = 0;
  float y = 0;
  float angle = 0;
  float rot = 0.1;
  PImage img;
  
  Sprite(String filename) {
    x = width/2;
    y = height/2;
    img = loadImage(filename);
    rot = random(-0.2, 0.2);
  }
  
  Sprite(String filename, float xarg, float yarg, float anglearg, float rotarg) {
    img = loadImage(filename);
    x = xarg;
    y = yarg;
    angle = anglearg;
    rot = rotarg;
  }
  
  void update() {
    x += random(-3, 3);
    y += random(-3, 3);
    angle += rot;
    checkBounds();
  }
  
  void display() {
    pushMatrix();
    translate(x, y);
    rotate(angle);
    imageMode(CENTER);
    image(img, 0, 0);
    popMatrix();
  }
  
  void checkBounds() {
    // Ensure that we don't exceed the bounds of the window
    if (x <= 0) {
      x = 0;
    } else if (x >= width) {
      x = width; 
    }
    
    if (y <= 0) {
      y = 0;
    } else if (y >= height) {
      y = height;
    }    
  }
}
