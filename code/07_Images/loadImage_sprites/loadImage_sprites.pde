Sprite[] spriteArray;
int numSprites = 10;
String spriteFilename = "hand.png";

void setup() {
  size(500, 500);

  spriteArray = new Sprite[numSprites];
  
  for (int i = 0; i < numSprites; i++) {
    spriteArray[i] = new Sprite(spriteFilename);  
  }
}

void draw() {
  // Clear the screen each frame
  background(255);
  
  for (int i = 0; i < numSprites; i++) {
    spriteArray[i].update();
    spriteArray[i].display();
  }
}