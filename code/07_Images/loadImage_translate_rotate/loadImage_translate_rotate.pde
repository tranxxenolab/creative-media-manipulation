// The variable to hold our image
PImage img;

// Variables to hold position and angle
float x, y;
float angle;

// How much to rotate the angle each frame
float rotValue = 0.1;

void setup() {
  size(500, 500);
  
  // Default initial variables
  x = width/2;
  y = height/2;
  angle = 0;
  
  // Load our image
  img = loadImage("hand.png");
}

void draw() {
  // Clear the screen each frame
  background(255);

  // Translate to a new origin, and rotate to a new angle
  translate(x, y);
  rotate(angle);
  
  // Rather than displaying the image using the top, left coords
  // set the coordinate origin to the center of the image
  imageMode(CENTER);
  image(img, 0, 0);
  
  // Update the x and y values
  x += random(-3, 3);
  y += random(-3, 3);
  angle += rotValue;
  
  // Ensure that we don't exceed the bounds of the window
  if (x <= 0) {
    x = 0;
  } else if (x >= width) {
    x = width; 
  }
  
  if (y <= 0) {
    y = 0;
  } else if (y >= height) {
    y = height;
  }
}