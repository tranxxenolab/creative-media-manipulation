import processing.video.*;

int scale = 2;
int cols, rows;
Capture video;

void setup() {
  size(640, 480);
  
  cols = width/scale;
  rows = height/scale;
  printArray(Capture.list());
  video = new Capture(this, cols, rows);
  video.start();
}

void captureEvent(Capture video) {
  video.read();  
}

void draw() {
  background(0);
  
  video.loadPixels();
  
  for (int i = 0; i < cols; i++) {
    for (int j = 0; j < rows; j++) {
      int x = i * scale;
      int y = j * scale;
      
      int loc = (video.width - i - 1) + j * video.width;
      
      color c = video.pixels[loc];
      
      float sz = (brightness(c)/255) * scale;
      
      rectMode(CENTER);
      fill(255);
      noStroke();
      rect(x + scale/2, y + scale/2, sz, sz);
    }
  }
  //video.updatePixels();
}