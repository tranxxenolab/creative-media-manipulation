// Load the video library
import processing.video.*;

// Global variables that we're going to need
Capture video;
PImage destImage;
float threshold;

void setup() {
  size(640, 480);

  // Create and start the capture
  video = new Capture(this, width, height);
  video.start();

  // Create our destination image and set a default threshold
  destImage = createImage(width, height, RGB);
  threshold = 10;
}

// Implement our event handler
void captureEvent(Capture video) {
  video.read();  
}

void draw() {
  // Local variables
  // We'll need to keep track of our current and previous lcoations
  int loc, prevLoc;
  float b;
  color c, prevC;
  
  // Load the pixels
  video.loadPixels();
  destImage.loadPixels();
  
  // Since we're comparing values between *adjacent* locations,
  // we're going to be starting from 1 rather than 0.
  for (int i = 1; i < width; i++) {
    for (int j = 0; j < height; j++) {
      // We need the index into the current location and the previous one
      loc = i + j * width;
      prevLoc = (i - 1) + j * width;
  
      // Load the two colors
      c = video.pixels[loc];
      prevC = video.pixels[prevLoc];

      // Calculate the *absolute value* of the *difference* in brightness
      // between the two colors.
      // This is a simple "edge detector". Edges often occur in images
      // when there is a sharp difference in brightness. This calculation
      // can give us an idea of when things go from light to dark, or from
      // dark to light, indicating an edge.
      b = abs(brightness(c) - brightness(prevC));
      
      // This should look similar to our thresholding example.
      if (b > threshold) {
        destImage.pixels[loc] = color(255, 255, 255);  
      } else {
        destImage.pixels[loc] = color(0, 0, 0);  
      }
    }
  }
  
  // Update the pixels in the destination image
  destImage.updatePixels();
  
  // This is a bit of a trick to get the resulting image to not 
  // be mirrored in the window
  pushMatrix();
  scale(-1, 1);
  image(destImage, -width, 0);
  popMatrix();
  
}

// This is similar to the threshold example, just with some different values
void mouseMoved() {
  threshold = map(mouseX, 0, width, 5, 50);   
}