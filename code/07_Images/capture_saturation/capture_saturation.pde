import processing.video.*;

Capture video;

float saturation = 127;

void setup() {
  size(640, 480);

  //printArray(Capture.list());
  video = new Capture(this, width, height);
  video.start();
}

void captureEvent(Capture video) {
  video.read();  
}

void draw() {
  video.loadPixels();
  float h, s, b;
  color c;
  
  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      int loc = (video.width - i - 1) + j * width;
      
      c = video.pixels[loc];
      h = hue(c);
      b = brightness(c);
      
      colorMode(HSB, 255, 255, 255);
      video.pixels[loc] = color(h, saturation, b);
    }
  }
  video.updatePixels();
  image(video, 0, 0);
}

void mouseMoved() {
  saturation = map(mouseX, 0, width, 0, 255);  
}